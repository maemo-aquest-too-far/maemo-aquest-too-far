--- A Quest Too Far version 1.3 (09/10/10) ---
Copyright (C) 2010 Geoffrey White

- INSTRUCTIONS -

Instructions can be brought up in game by pressing '?'.


- LICENSE -

'A Quest Too Far' is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be enjoyed,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

- VERSION HISTORY -

version 1.3	09/10/10	added skeleton, mummy
				added variable wall colouring
				visibility system improved in diagonal tunnels
				witch rebalanced, manticore AI improved
				minor bugs, interface improvements, code tidy up
version 1.2	28/03/10	implemented alternative controls (for laptops)
				added to the in game help screen and readme
				strength of poisons reduced by about 50%
				witch rebalanced (slower spells but uses close combat attacks)
				minor bugs and interface improvements
version 1.1	14/03/10	fixed source to compile on Linux/g++
version 1.0	13/03/10	original 7DRL release

- CONTACT -

The official web site of 'A Quest Too Far' is:
	http://www.randomstuff.org.uk/~geoffrey/roguelikes/aquesttoofar.html

Contact me at:
	[my first name]-aquesttoofar@randomstuff.org.uk

- END -
