# Microsoft Developer Studio Project File - Name="AQuestTooFar" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=AQuestTooFar - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "AQuestTooFar.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "AQuestTooFar.mak" CFG="AQuestTooFar - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "AQuestTooFar - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "AQuestTooFar - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "AQuestTooFar - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /FR /YX /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x809 /d "NDEBUG"
# ADD RSC /l 0x809 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /machine:I386

!ELSEIF  "$(CFG)" == "AQuestTooFar - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /YX /FD /GZ /c
# ADD CPP /nologo /MD /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /FR /YX /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x809 /d "_DEBUG"
# ADD RSC /l 0x809 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "AQuestTooFar - Win32 Release"
# Name "AQuestTooFar - Win32 Debug"
# Begin Group "Source"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat;h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\Source\brain.cpp
# End Source File
# Begin Source File

SOURCE=.\Source\brain.h
# End Source File
# Begin Source File

SOURCE=.\Source\charlib.cpp
# End Source File
# Begin Source File

SOURCE=.\Source\charlib.h
# End Source File
# Begin Source File

SOURCE=.\Source\creature.cpp
# End Source File
# Begin Source File

SOURCE=.\Source\creature.h
# End Source File
# Begin Source File

SOURCE=.\Source\debug.h
# End Source File
# Begin Source File

SOURCE=.\Source\defines.h
# End Source File
# Begin Source File

SOURCE=.\Source\describe.cpp
# End Source File
# Begin Source File

SOURCE=.\Source\describe.h
# End Source File
# Begin Source File

SOURCE=.\Source\fx.cpp
# End Source File
# Begin Source File

SOURCE=.\Source\fx.h
# End Source File
# Begin Source File

SOURCE=.\Source\game.cpp
# End Source File
# Begin Source File

SOURCE=.\Source\game.h
# End Source File
# Begin Source File

SOURCE=.\Source\inventory.cpp
# End Source File
# Begin Source File

SOURCE=.\Source\inventory.h
# End Source File
# Begin Source File

SOURCE=.\Source\item.cpp
# End Source File
# Begin Source File

SOURCE=.\Source\item.h
# End Source File
# Begin Source File

SOURCE=.\Source\main.cpp
# End Source File
# Begin Source File

SOURCE=.\Source\map.cpp
# End Source File
# Begin Source File

SOURCE=.\Source\map.h
# End Source File
# Begin Source File

SOURCE=.\Source\message.cpp
# End Source File
# Begin Source File

SOURCE=.\Source\message.h
# End Source File
# Begin Source File

SOURCE=.\Source\powers.cpp
# End Source File
# Begin Source File

SOURCE=.\Source\powers.h
# End Source File
# Begin Source File

SOURCE=.\Source\rng.cpp
# End Source File
# Begin Source File

SOURCE=.\Source\rng.h
# End Source File
# Begin Source File

SOURCE=.\Source\screens.cpp
# End Source File
# Begin Source File

SOURCE=.\Source\screens.h
# End Source File
# Begin Source File

SOURCE=.\Source\stack.cpp
# End Source File
# Begin Source File

SOURCE=.\Source\stack.h
# End Source File
# Begin Source File

SOURCE=.\Source\statusbar.cpp
# End Source File
# Begin Source File

SOURCE=.\Source\statusbar.h
# End Source File
# Begin Source File

SOURCE=.\Source\util.cpp
# End Source File
# Begin Source File

SOURCE=.\Source\util.h
# End Source File
# Begin Source File

SOURCE=.\Source\vis.cpp
# End Source File
# Begin Source File

SOURCE=.\Source\vis.h
# End Source File
# End Group
# Begin Group "Resources"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\Source\resourcescript.rc
# End Source File
# End Group
# Begin Group "Library Files"

# PROP Default_Filter ""
# Begin Source File

SOURCE="C:\Program Files\SDL-1.2.13\lib\SDLmain.lib"
# End Source File
# Begin Source File

SOURCE="C:\Program Files\SDL-1.2.13\lib\SDL.lib"
# End Source File
# End Group
# End Target
# End Project
