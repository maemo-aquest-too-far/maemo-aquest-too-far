// screens.cpp - 'A Quest Too Far'
//
// Copyright 2010 Geoffrey White
//
// see readme.txt for more details.

#include "defines.h"
#include "screens.h"
#include "message.h"
#include "creature.h"
#include "item.h"

#define TEXT_ALT_STYLE mk_char_style(128, 128, 255, 0, 0, 0)

void draw_screen_title(std::string title, std::string help)
{
	std::string str = title;
	int x;

	// draw title
	x = str.size() + 1;
	while (str.size() < GRID_WIDTH) {str += ' ';}
	char_draw(0, 0, str, MESSAGE_DEFAULT_STYLE);

	// draw help text
	char_draw(x, 0, help, MESSAGE_HIGHLIGHT_STLYE);
}

// --- intro_screen ---

intro_screen :: intro_screen()
{
	needs_key = true;
}

void intro_screen :: carry_out(const char_key &key)
{
	if (key.ascii == ' ')
	{
		// done with screen
	} else if ((key.ascii == '?') || (key.sdl == SDLK_F1)) {
		stack::push_action(*this);
		{			
			help_screen _action = help_screen();
			stack::push_action(_action);
		}
	} else {
		// keep waiting
		stack::push_action(*this);
	}
}

void intro_screen :: draw()
{
	int y;

	char_clear();
	draw_screen_title("A Quest Too Far 1.3", "");

	y = 2;
					// ########################################################
	char_draw(0, y++, " @ <-- You have beaten the Dungeons of Doom.  You have", char_default_style);
	char_draw(1, y - 1, "@", mk_char_style(255, 255, 128, 0, 0, 0));
	char_draw(0, y++, "       saved kingdoms, slain dragons and fought evil -", char_default_style);
	char_draw(0, y++, "       a true hero.  But that was all a long time ago.", char_default_style);
	y++;
	char_draw(0, y++, "Now you're an old man with a family.  Your adventuring", char_default_style);
	char_draw(0, y++, "days are over ... or so you thought.  One day you wake", char_default_style);
	char_draw(0, y++, "up to find your family distraught as your grandchildren", char_default_style);
	char_draw(0, y++, "are missing.", char_default_style);
	y++;
	char_draw(0, y++, "Taking some of your old equipment, you follow their", char_default_style);
	char_draw(0, y++, "tracks into the forest.  After many miles you reach the", char_default_style);
	char_draw(0, y++, "entrance to a cave.", char_default_style);
	y++;
	char_draw(15, y + 0, "     ^      ^     ", mk_char_style(64, 192, 64, 0, 0, 0));
	char_draw(15, y + 1, " ^  /|\\  ^ /|\\    ", mk_char_style(64, 192, 64, 0, 0, 0));
	char_draw(15, y + 2, "/|\\ /|\\ /|\\/|\\  ^ ", mk_char_style(64, 192, 64, 0, 0, 0));
	char_draw(15, y + 3, "/|\\ /|\\ /|\\/|\\ /|\\", mk_char_style(64, 192, 64, 0, 0, 0));
	char_draw(35, y + 0, "            ####  ", mk_char_style(95, 95, 127, 0, 0, 0));
	char_draw(35, y + 1, "           ##  ## ", mk_char_style(95, 95, 127, 0, 0, 0));
	char_draw(35, y + 2, "    #   ## ##  ## ", mk_char_style(95, 95, 127, 0, 0, 0));
	char_draw(35, y + 3, "#############..###", mk_char_style(95, 95, 127, 0, 0, 0));
	y += 5;
	char_draw(0, y++, "Welcome to 'A Quest Too Far'.  Navigate the dungeon with", TEXT_ALT_STYLE);
	char_draw(0, y++, "the numeric keypad (arrows and Home, End, Page Up and", TEXT_ALT_STYLE);
	char_draw(0, y++, "Page Down to move diagonally).  Walk into monsters to", TEXT_ALT_STYLE);
	char_draw(0, y++, "attack them.  For more help and controls, press ?", TEXT_ALT_STYLE);
	char_draw(48, y - 1, "?", MESSAGE_HIGHLIGHT_STLYE);
	char_draw(0, y++, "now or when you're playing.", TEXT_ALT_STYLE);
	y++;
	char_draw(0, y++, "You won't earn 'xp' for slaying monsters like you do in", TEXT_ALT_STYLE);
	char_draw(0, y++, "many other games.  You have plenty of experience", TEXT_ALT_STYLE);
	char_draw(0, y++, "already!  Instead you will accumulate 'decline' for", TEXT_ALT_STYLE);
	char_draw(0, y++, "just about everything you do - so don't hang around!", TEXT_ALT_STYLE);
					// ########################################################
	y++;
	char_draw(0, y++, "Press SPACE to continue.", char_default_style);
	char_draw(6, y - 1, "SPACE", MESSAGE_HIGHLIGHT_STLYE);

	char_draw(0, GRID_HEIGHT - 2, "                           Copyright 2010 Geoffrey White", char_default_style);
	char_draw(0, GRID_HEIGHT - 1, "                         see readme.txt for more details", char_default_style);
}

// --- help_screen ---

help_screen :: help_screen()
{
	needs_key = true;
}

void help_screen :: carry_out(const char_key &key)
{
	if ((key.ascii == ' ') || (key.sdl == SDLK_BACKSPACE))
	{
		// done with screen
	} else {
		// keep waiting
		stack::push_action(*this);
	}
}

void help_screen :: draw()
{
	int y;

	char_clear();
	draw_screen_title("Help and Keys", "SPACE");

	y = 2;
					// ########################################################
	char_draw(0, y++, "Navigate the dungeon with Nethack style hjkl+yubn, ", char_default_style);
	char_draw(0, y++, "boosted with arrows.", char_default_style);
	char_draw(0, y++, "Walk into monsters to attack them.", char_default_style);
	char_draw(0, y++, "", char_default_style);
	y++;
	char_draw(0, y++, "q  w  e        y  k  u ", char_default_style);
	char_draw(0, y++, " \\ | /          \\ | / ", char_default_style);
	char_draw(0, y++, "a- @ -d   or   h- @ -l", char_default_style);
	char_draw(0, y++, " / | \\          / | \\ ", char_default_style);
	char_draw(0, y++, "z  x  c        b  j  n", char_default_style);
	y++;
	char_draw(0, y++, " .        - rest for a turn", char_default_style);
	char_draw(0, y++, " i        - inventory / use", char_default_style);
	char_draw(0, y++, " f        - drop item", char_default_style);
	char_draw(0, y++, " , / g    - pick up item", char_default_style);
	char_draw(0, y++, "s /return - descend stairs", char_default_style);
	y++;
	char_draw(0, y++, " ?        - help screen", char_default_style);
	char_draw(0, y++, " m        - message history", char_default_style);
	char_draw(0, y++, " v        - Quit current game", char_default_style);
	y++;
	char_draw(0, y++, "Your decline counter normally increases by one point per", TEXT_ALT_STYLE);
	char_draw(0, y++, "turn, but being poisoned (without poison resistance)", TEXT_ALT_STYLE);
	char_draw(0, y++, "temporarily accelerates this.", TEXT_ALT_STYLE);
	y++;
	char_draw(0, y++, "Healing potions restore hit points, cure poison and", TEXT_ALT_STYLE);
	char_draw(0, y++, "reverse a small amount of decline.", TEXT_ALT_STYLE);
					// ########################################################
}

// --- history_screen ---

history_screen :: history_screen()
{
	needs_key = true;
}

void history_screen :: carry_out(const char_key &key)
{
	if ((key.ascii == ' ') || (key.sdl == SDLK_BACKSPACE))
	{
		// done with screen
	} else {
		// keep waiting
		stack::push_action(*this);
	}
}

void history_screen :: draw()
{
	std::list<std::string>::iterator it;
	bool use_alt_style;
	int y;

	char_clear();
	draw_screen_title("Message History", "SPACE");

	// work backwards to decide how many messages we can display
	y = 2;
	it = recent_messages.end();
	while (it != recent_messages.begin())
	{
		it--;

		y += format_message(*it, MESSAGE_DEFAULT_STYLE, GRID_WIDTH, false, 0, false);
		if (y >= GRID_HEIGHT)
		{
			// doesn't fit
			it++;
			break;
		}
	}

	// draw message history
	y = 2;
	use_alt_style = false;
	while (it != recent_messages.end())
	{
		y += format_message(*it, (use_alt_style) ? (TEXT_ALT_STYLE) : (char_default_style),
							GRID_WIDTH, false, y, true);
		use_alt_style = !use_alt_style;
		it++;
	}
}


// --- suicide_dialog ---

suicide_dialog :: suicide_dialog(creature *_hero)
{
	needs_key = true;
	my_hero = _hero;
}

void suicide_dialog :: carry_out(const char_key &key)
{
	if ((toupper(key.ascii) == 'Y') )
	{
		my_hero->hp=0;
	} else {
		//printf("Suicide canceled\n");
	}
}

void suicide_dialog :: draw()
{
	std::string str = "Commit suicide? Press 'y' to confirm";
	while (str.size() < GRID_WIDTH) {str += ' ';}
	char_draw(0, 0, str, mk_char_style(255, 255, 192, 128, 0, 0));
}
