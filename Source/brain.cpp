// brain.cpp - 'A Quest Too Far'
//
// Copyright 2010 Geoffrey White
//
// see readme.txt for more details.

#include "brain.h"
#include "defines.h"
#include "creature.h"
#include "game.h"
#include "rng.h"
#include "map.h"
#include "powers.h"
#include "describe.h"
#include "message.h"
#include "stack.h"
#include "util.h"
#include <assert.h>
#include <queue>

// --- movement costs ---
//
// we use:
//	distance allowing diagonals + distance without diagonals
//  (this discourages monsters from needless zigzagging)

static int move_cost[8] = {2, 3, 2, 3, 2, 3, 2, 3};

static inline int move_cost_minimum(int x1, int y1, int x2, int y2)
{
	int dx = abs(x2 - x1);
	int dy = abs(y2 - y1);

	return MAX(dx, dy) + dx + dy;
}

// --- path finding ---

struct point
{
	int x, y;
};

struct open_point
{
	int x, y;
	int dir_from;
	int cost_to_reach;
	int minimum_total_cost;
};

bool operator<(const open_point &a, const open_point &b)
{
	return (a.minimum_total_cost > b.minimum_total_cost);
		// inverted, because we want the lowest cost at the front of the priority_queue
}

static bool path_find(creature *_creature, map *_map, point start, std::vector<point> dests, std::list<int> &out_list)
{
	// A* path finding; constructs a list of move directions from start to dest in 'out_list' if successful
	// path finding is actually carried out BACKWARDS, since that allows multiple destinations to be supported easily.
	int best_path_cost[MAP_WIDTH][MAP_HEIGHT]; // walls are assigned best path cost 0, which is unbeatable
	int best_dir_from[MAP_WIDTH][MAP_HEIGHT]; // for reconstruction
	std::priority_queue<open_point> open_list;
	std::vector<point>::iterator it;
	map_tile *tile;
	open_point p, p2;
	int x, y, dir, cost;

	if (_map == NULL)
	{
		return false;
	}

	// initialize solid map tiles to closed
	for (y = 0; y < MAP_HEIGHT; y++)
	{
		for (x = 0; x < MAP_WIDTH; x++)
		{
			tile = _map->get_tile(x, y);

			best_path_cost[x][y] = (tile->is_solid) ? (0) : (9999);
			if ((abs(x - start.x) <= 1) && (abs(y - start.y) <= 1) &&
				(tile->my_creature != NULL) && (tile->my_creature != _creature) &&
				(_creature->is_friends(tile->my_creature)))
			{
				// nearby allies count as walls, so we path find around them
				// (further allies do not, as they're likely to have moved by the time we reach them anyway)
				//
				// note that here and in several other places, we allow monsters to perceive other
				// invisible monsters.  This isn't ideal, but the reverse (running into them, attacking
				// them etc) is probably much worse.
				best_path_cost[x][y] = 0;
			}
			best_dir_from[x][y] = -1;
		}
	}

	// put each end point on the open list
	for (it = dests.begin(); it != dests.end(); it++)
	{
		p.x = it->x;
		p.y = it->y;
		p.dir_from = -1; // marks an end point
		p.cost_to_reach = 0;
		p.minimum_total_cost = move_cost_minimum(start.x, start.y, it->x, it->y);
		open_list.push(p);
	}

	// process
	while (true)
	{
		// check for failure
		if (open_list.empty())
		{
			return false;
		}

		// get the element with the best optimistic estimate
		p = open_list.top();
		open_list.pop();

		// check for a useless path (this is usually detected sooner, but not always)
		if (p.cost_to_reach >= best_path_cost[p.x][p.y])
		{
			continue;
		}

		// mark this square with our path
		best_path_cost[p.x][p.y] = p.cost_to_reach;
		best_dir_from[p.x][p.y] = p.dir_from;

		// check for success
		if (p.cost_to_reach == p.minimum_total_cost) // i.e. remaining estimate = 0, we're there
		{
			// reconstruct the winning path back from start -> dest
			assert(p.x == start.x);
			assert(p.y == start.y);
			x = start.x;
			y = start.y;
			while (best_dir_from[x][y] != -1)
			{
				dir = best_dir_from[x][y];

				// step back
				out_list.push_back(dir);
				x += dir_xs[dir];
				y += dir_ys[dir];
			}
			return true;
		}

		// expand surrounding squares
		for (dir = 0; dir < 8; dir++)
		{
			x = p.x + dir_xs[dir];
			y = p.y + dir_ys[dir];
			cost = p.cost_to_reach + move_cost[dir];

			if ((x >= 0) && (x < MAP_WIDTH) && (y >= 0) && (y < MAP_HEIGHT) &&
				(cost < best_path_cost[x][y]))
			{
				p2.x = x;
				p2.y = y;
				p2.dir_from = (dir + 4) % 8; // reverse dir
				p2.cost_to_reach = cost;
				p2.minimum_total_cost = cost + move_cost_minimum(start.x, start.y, x, y);
				open_list.push(p2);
			}
		}
	}

	return false; // never reached
}

// --- brain ---

brain :: brain(game &_game) : my_game(_game), mode(BM_NOTINITIALIZED)
{
}

brain :: ~brain()
{
}

void brain :: init(class creature *_creature, brain_type _type, warning_type _warning)
{
	my_creature = _creature;
	type = _type;
	warning = _warning;
	home_x = _creature->get_x();
	home_y = _creature->get_y();
	has_spotted = false;
	has_warned = false;
}

bool brain :: try_to_step(int dx, int dy, bool &out_progressed)
{
	map *new_map;
	map_tile *new_tile;
	int new_x, new_y;

	out_progressed = false;

	new_map = my_creature->get_map();
	new_x = my_creature->get_x() + dx;
	new_y = my_creature->get_y() + dy;
	new_tile = map::get_tile(new_map, new_x, new_y);
	if (new_tile == NULL)
	{
		return false;
	}

	// attack?
	if ((new_tile->my_creature != NULL) && (!my_creature->is_friends(new_tile->my_creature)))
	{
		if (new_tile->my_creature->is_hero())
		{
			// spot the hero if we walk into him!
			assert(my_creature->get_map() == my_game.the_hero->get_map());
			spot_x = my_game.the_hero->get_x();
			spot_y = my_game.the_hero->get_y();
			has_spotted = true;
		}

		my_creature->attack(new_tile->my_creature);
		return true;
	}

	// move?
	if ((my_creature->can_move(new_tile)) && (my_creature->carrying <= my_creature->get_carry_max()))
	{
		my_creature->move_to(new_map, new_x, new_y);
		out_progressed = true;
		return true;
	}

	return false;
}

bool brain :: follow_path()
{
	int dir;
	bool progressed;

	if (!planned_path.empty())
	{
		dir = planned_path.front();

		if (try_to_step(dir_xs[dir], dir_ys[dir], progressed))
		{
			if (progressed)
			{
				planned_path.pop_front();
			}
			return true;
		}
	}

	// fail
	return false;
}

bool brain :: ready_to_range()
{
	u32 attack_flags = my_creature->get_attack_flags();

	if (((attack_flags & AF_RFIRE) != 0) && (my_creature->special_recharge == 0))
	{
		// can breathe fire
		return true;
	}

	if ((attack_flags & AF_RSPINE) != 0)
	{
		// can fire spines
		return true;
	}

	return false;
}

bool brain :: check_range(int from_x, int from_y, int target_x, int target_y, int *out_dx, int *out_dy)
{
	u32 attack_flags = my_creature->get_attack_flags();
	int dx, dy, i, dist;
	map_tile *tile;

	// measure direction and distance to target
	dx = 0;
	if (target_x > from_x) {dx = 1;}
	if (target_x < from_x) {dx = -1;}
	dy = 0;
	if (target_y > from_y) {dy = 1;}
	if (target_y < from_y) {dy = -1;}
	dist = MAX(abs(target_x - from_x), abs(target_y - from_y));

	// are we lined up?
	if ((from_x + (dist * dx) != target_x) || (from_y + (dist * dy) != target_y))
	{
		return false;
	}

	// is range acceptable?
	if (((attack_flags & AF_RSPINE) != 0) && (dist < 2))
	{
		return false;
	}

	// do we have a clear path?
	for (i = 1; i < dist; i++)
	{
		tile = map::get_tile(my_creature->get_map(), from_x + (i * dx), from_y + (i * dy));
		if ((tile == NULL) || (tile->is_solid))
		{
			// blocked by wall
			return false;
		}
		if ((tile->my_creature != NULL) && (my_creature->is_friends(tile->my_creature)))
		{
			// blocked by friendly creature
			return false;
		}
	}

	if (out_dx != NULL)
	{
		*out_dx = dx;
	}
	if (out_dy != NULL)
	{
		*out_dy = dy;
	}
	return true;
}

bool brain :: try_to_range(int target_x, int target_y)
{
	u32 attack_flags = my_creature->get_attack_flags();
	int dx, dy;

	// can we make a range attack?
	if ((ready_to_range()) && (check_range(my_creature->get_x(), my_creature->get_y(), target_x, target_y, &dx, &dy)))
	{
		// breathe fire?
		if ((attack_flags & AF_RFIRE) != 0)
		{
			zap_fire(my_creature, dx, dy);
			my_creature->special_recharge = random(5, 10);
			return true;
		}

		// launch spikes
		if ((attack_flags & AF_RSPINE) != 0)
		{
			// fire!
			zap_spines(my_creature, dx, dy);
			return true;
		}
	}

	// can we summon?
	if (((my_creature->get_attack_flags() & AF_SUMMONS) != 0) &&
		(my_creature->special_recharge == 0))
	{
		if (my_creature->special_counter == 0)
		{
			// summon lots of monsters the first time
			do_summons(my_game, my_creature, 5);
		} else {
			// summon more monsters every few turns
			do_summons(my_game, my_creature, 3);
		}
		my_creature->special_counter++;
		my_creature->special_recharge = random(7, 9);
		return true;
	}

	return false;
}

void brain :: behave_normal()
{
	map_tile *tile;
	int i, dest_x, dest_y, dist, t_size;;
	bool do_path_find = false;

	switch (type)
	{
	case BT_TERRITORIAL:
	case BT_TERRITORIALAGGRO:
	case BT_BOSS:
		{
			t_size = TERRITORY_SIZE;
			if (type == BT_BOSS) {t_size = 1;}

			dist = MAX(abs(my_creature->get_x() - home_x), abs(my_creature->get_y() - home_y));
			if ((dist <= t_size) && (random(2) == 0))
			{
				// wait for a while (inside our territory)
				mode = BM_WAITING;
				counter = random(1, 50);
			} else {
				// pick a random destination inside our territory
				for (i = 0; i < 10; i++)
				{
					// try to pick somewhere that isn't solid
					dest_x = random(MAX(home_x - t_size, 0), MIN(home_x + t_size, MAP_WIDTH - 1));
					dest_y = random(MAX(home_y - t_size, 0), MIN(home_y + t_size, MAP_HEIGHT - 1));
					tile = my_creature->get_map()->get_tile(dest_x, dest_y);
					if (!tile->is_solid) break;
				}

				// path to it
				do_path_find = true;
				mode = BM_PATHING;
			}
		} break;

	case BT_WANDERING:
		{
			if (random(5) == 0)
			{
				// wait for a while
				mode = BM_WAITING;
				counter = random(1, 50);
			} else {
				// pick a random destination on the level
				for (i = 0; i < 10; i++)
				{
					// try to pick somewhere that isn't solid
					dest_x = random(0, MAP_WIDTH - 1);
					dest_y = random(0, MAP_HEIGHT - 1);
					tile = my_creature->get_map()->get_tile(dest_x, dest_y);
					if (!tile->is_solid) break;
				}

				do_path_find = true;
				mode = BM_PATHING;
			}
		} break;

	default:
		{
			assert(false);
		} break;
	}

	if (do_path_find)
	{
		// path to location
		point from, dest;
		std::vector<point> dests;

		from.x = my_creature->get_x();
		from.y = my_creature->get_y();
		dest.x = dest_x;
		dest.y = dest_y;
		dests.push_back(dest);

		planned_path.clear();
		path_find(my_creature, my_creature->get_map(), from, dests, planned_path);
	}
}

void brain :: behave_attacking(bool use_range)
{
	point from, dest;
	std::vector<point> dests;

	// path to player (repath each turn)
	from.x = my_creature->get_x();
	from.y = my_creature->get_y();
	dest.x = spot_x;
	dest.y = spot_y;
	dests.push_back(dest);

	// or path to anywhere we can make a ranged attack on the player from
	if ((use_range) && (ready_to_range()))
	{
		int i, dist;
		bool ok;

		for (i = 0; i < 8; i++)
		{
			dist = 0;
			ok = true;
			while (ok)
			{
				dest.x = spot_x + (dist * dir_xs[i]);
				dest.y = spot_y + (dist * dir_ys[i]);
				if (check_range(dest.x, dest.y, spot_x, spot_y, NULL, NULL))
				{
					// we could shoot the player from here
					dests.push_back(dest);
				} else if (dist >= 2) {
					// give up on this direction
					ok = false;
				}
				dist++;
			}
		}
	}

	planned_path.clear();
	path_find(my_creature, my_creature->get_map(), from, dests, planned_path);
}

void brain :: behave_spot(bool is_final_move)
{
	int dist;

	switch (type)
	{
	case BT_WANDERING:
	case BT_TERRITORIALAGGRO:
	case BT_BOSS:
		{
			// mindlessly attack
			if (mode != BM_ANGRY)
			{
				mode = BM_ATTACKING;
			}
		} break;

	case BT_TERRITORIAL:
		{
			// cautiously defend territory
			dist = MAX(abs(spot_x - home_x), abs(spot_y - home_y));
			if (mode == BM_ANGRY)
			{
				// stay angry!
			} else if (dist <= TERRITORY_SIZE + 1) {
				if ((warning != WT_NONE) && (!has_warned))
				{
					// warn them (at hero -> always visible)
					mode = BM_WARNING;
				} else {
					// attack them
					mode = BM_ATTACKING;
				}
			} else {
				// call off attack (unless angry)
				if (mode == BM_ATTACKING)
				{
					behave_normal();
				}
			}
		} break;

	default:
		{
			assert(false);
		} break;
	}
}

void brain :: behave_attacked(creature *attacker)
{
	map_tile *tile;
	int i, chance;

	if (attacker->is_hero())
	{
		// spot player
		// note:
		//  - we don't get the attackers position, we have to guess (they may be invisible)
		//  - if they are visible, behave_spot will set the position exactly.
		//  - if we hit them, we also become aware of their location
		if (!has_spotted)
		{
			chance = 0;
			for (i = 0; i < 8; i++)
			{
				tile = map::get_tile(my_creature->get_map(), my_creature->get_x() + dir_xs[i], my_creature->get_y() + dir_ys[i]);
				if ((tile != NULL) && (!tile->is_solid) &&
					((tile->my_creature == NULL) || (!my_creature->is_friends(tile->my_creature))))
				{
					chance++;
					if (random(chance) == 0)
					{
						// guess that the attacker is here
						spot_x = my_creature->get_x() + dir_xs[i];
						spot_y = my_creature->get_y() + dir_ys[i];
						has_spotted = true;
					}
				}
			}
		}

		// if we weren't already fighting we didn't start it, so become angry
		if (mode != BM_ATTACKING)
		{
			mode = BM_ANGRY;
		}
	}
}

void brain :: move(bool is_final_move)
{
	map_tile *tile;
	bool did_spot, done;

	// check surroundings
	tile = my_creature->get_map_tile();
	if (my_game.the_hero != NULL)
	{
		if ((tile->vis == V_VISIBLE) && (my_game.the_hero->is_visible()))
		{
			// our tile is visible + player is visible -> we can see the player
			// (this occurs every turn as long as we can see them)
			assert(my_creature->get_map() == my_game.the_hero->get_map());
			spot_x = my_game.the_hero->get_x();
			spot_y = my_game.the_hero->get_y();
			has_spotted = true;
		}
		if ((my_creature->has_detection) &&
			(abs(my_creature->get_x() - my_game.the_hero->get_x()) <= DETECTION_RANGE) &&
			(abs(my_creature->get_y() - my_game.the_hero->get_y()) <= DETECTION_RANGE))
		{
			// hehehe, we have detection!
			assert(my_creature->get_map() == my_game.the_hero->get_map());
			spot_x = my_game.the_hero->get_x();
			spot_y = my_game.the_hero->get_y();
			has_spotted = true;
		}
	}
	did_spot = has_spotted;
	if (has_spotted)
	{
		behave_spot(is_final_move);
		has_spotted = false;
	}

	done = false;
	while (!done)
	{
		done = true;

		// run mode
		switch (mode)
		{
		case BM_NOTINITIALIZED:
			{
				// set normal behaviour
				behave_normal();
				done = false; // continue immediately
			} break;

		case BM_WAITING:
			{
				if (!is_final_move)
				{
					// no rush
					return;
				}

				counter--;
				if (counter <= 0)
				{
					// finished waiting
					behave_normal();
				}
			} break;

		case BM_WARNING:
			{
				if (is_final_move)
				{
					display_message _action = display_message(describe_warning(my_creature, my_game.the_hero, warning));
					stack::push_action(_action);

					has_warned = true;
					behave_normal();
				}
			} break;

		case BM_PATHING:
			{
				if (!is_final_move)
				{
					// no rush
					return;
				}

				if (follow_path())
				{
					// progress made
				} else {
					// arrived / blocked
					behave_normal();
				}
			} break;

		case BM_ATTACKING:
		case BM_ANGRY:
			{
				// make a range attack?
				if ((did_spot) && (is_final_move) && (try_to_range(spot_x, spot_y)))
				{
					break;
				}

				if ((type == BT_BOSS) && (did_spot) &&
					((abs(my_creature->get_x() - spot_x) > 1) || (abs(my_creature->get_y() - spot_y) > 1)))
				{
					// harass type - don't approach further once in vision
					break;
				}

				// path to player or somewhere to make range attack from (repath each turn)
				behave_attacking(true);
				if (follow_path())
				{
					// progress made
					break;
				}
				// failed to path - arrived or blocked

				// path to players location only?
				behave_attacking(false);
				if (follow_path())
				{
					// progress made
					break;
				}

				// we've lost them (arrived at where we thought they were / pathing blocked)
				behave_normal();
				done = false; // continue immediately
			} break;

		default:
			{
				assert(false);
			} break;
		};
	}
}