// inventory.cpp - 'A Quest Too Far'
//
// Copyright 2010 Geoffrey White
//
// see readme.txt for more details.

#include "defines.h"
#include "inventory.h"
#include "map.h"
#include "message.h"
#include "describe.h"
#include "util.h"
#include <assert.h>

#define INVENTORY_NORMAL_STYLE (char_default_style)
#define INVENTORY_ALT_STYLE mk_char_style(128, 128, 255, 0, 0, 0)
#define INVENTORY_GREYED_STYLE (mk_char_style(128, 128, 128, 0, 0, 0))

// --- display_inventory ---

display_inventory :: display_inventory(class creature *_creature, inventory_mode _mode, action &_cancel_action) :
	my_creature(_creature), mode(_mode), my_cancel_action(NULL)
{
	map_tile *tile;

	assert(_creature != NULL);
	my_cancel_action = _cancel_action.clone();
	needs_key = true;

	offset = 0;
	if (mode == IM_PICKUP)
	{
		// inventory the ground
		tile = my_creature->get_map_tile();
		items = &(tile->my_items);
	} else {
		// inventory the creature
		items = &(my_creature->my_items);
	}
}

display_inventory :: ~display_inventory()
{
	if (my_cancel_action != NULL)
	{
		delete my_cancel_action;
		my_cancel_action = NULL;
	}
}

bool display_inventory :: can_pick_item(item *_item)
{
	bool can_pick = true;
	bool is_exhausted;

	switch (mode)
	{
	case IM_INVENTORY:
		{
			assert(_item->can_equip(ES_LEFTRING) == _item->can_equip(ES_RIGHTRING));
			return (_item->can_equip(ES_LEFTRING)) || (_item->can_activate(is_exhausted));
		} break;

	case IM_WIELD:
		{
			return (_item->can_equip(ES_WEAPON));
		} break;

	default:
		{
			// IM_PICKUP, IM_DROP - can pick anything
		} break;
	}

	return can_pick;
}

void display_inventory :: carry_out(const char_key &key)
{
	std::list<item *>::iterator it;
	std::string message;
	bool is_exhausted, continue_inventory = true;
	int i;

	if ((toupper(key.ascii) >= 'A') && (toupper(key.ascii) <= 'Z'))
	{
		// find the item
		i = toupper(key.ascii) - 'A';
		it = items->begin();
		while ((it != items->end()) && (i > 0))
		{
			it++;
			i--;
		}
		if (it != items->end())
		{
			// found item
			switch (mode)
			{
			case IM_INVENTORY:
				{
					assert((*it)->can_equip(ES_LEFTRING) == (*it)->can_equip(ES_RIGHTRING));

					//Is it ring? (can it be equipped to ring-slot?)
					if ((*it)->can_equip(ES_LEFTRING))
					{
						equip_slot slot = ES_NUM;

						if (my_creature->equipped[ES_RIGHTRING] == *it)
						{
							// take off right ring
							slot = ES_RIGHTRING;
						} else if (my_creature->equipped[ES_LEFTRING] == *it) {
							// take off left ring
							slot = ES_LEFTRING;
						} else if (my_creature->equipped[ES_RIGHTRING] == NULL) {
							// put on right ring
							slot = ES_RIGHTRING;
						} else if (my_creature->equipped[ES_LEFTRING] == NULL) {
							// put on left ring
							slot = ES_LEFTRING;
						} else {
							slot = ES_NUM;
						}

						if (slot < ES_NUM)
						{
							// put on / take off ring
							equip_item _action = equip_item(my_creature, *it, slot);
							stack::push_action(_action);
							continue_inventory = false;
						} else {
							stack::push_action(*my_cancel_action);
							{
								display_message _action = display_message("You must take off a ring before putting another one on!");
								stack::push_action(_action);
							}
							continue_inventory = false;
						}
					} 
					
					//Is it weapon? (can it be equipped to weapon-slot?)
					else if ((*it)->can_equip(ES_WEAPON)){
						//If it directional, it must be thrown
						if (((*it))->is_directional() ) 
						{
							//activating=throwing
							activate_item _action = activate_item(my_creature, *it, *my_cancel_action);
							stack::push_action(_action);
						}
						else 
						{
							equip_item _action = equip_item(my_creature, *it, ES_WEAPON);
							stack::push_action(_action);
						}
						continue_inventory = false;
					}

					//It is activatable
					else {
						//Is is empty? (exhausted)
						if ((*it)->can_activate(is_exhausted)) {
							//Can activated
							activate_item _action = activate_item(my_creature, *it, *my_cancel_action);
							stack::push_action(_action);
							continue_inventory = false;
						} else { 
							//exhausted.
							stack::push_action(*my_cancel_action);
							display_message _action = display_message("The " + (*it)->short_name + " is exhausted!");
							stack::push_action(_action);
						}

						continue_inventory = false;
					}
				} break;



			case IM_PICKUP:
				{
					// pick up the item
					pickup_item _action = pickup_item(my_creature, *it);
					stack::push_action(_action);
					continue_inventory = false;
				} break;

			case IM_DROP:
				{
					// drop the item
					drop_item _action = drop_item(my_creature, *it);
					stack::push_action(_action);
					continue_inventory = false;
				} break;

			default:
				{
					assert(false);
				} break;
			}
		}
	}

	if (key.sdl == SDLK_BACKSPACE || key.sdl == SDLK_SPACE )
	{
		// cancel
		stack::push_action(*my_cancel_action);
		continue_inventory = false;
	}

	if ((key.sdl == SDLK_PAGEUP) || (key.sdl == SDLK_KP9))
	{
		if (offset > 0)
		{
			offset -= 26;
		}
	}

	if ((key.sdl == SDLK_PAGEDOWN) || (key.sdl == SDLK_KP3))
	{
		if (offset + 25 < (int)(my_creature->my_items.size()) - 1)
		{
			offset += 26;
		}
	}

	if (continue_inventory)
	{
		// stay in inventory
		stack::push_action(*this);
	}
}

void display_inventory :: draw()
{
	std::list<item *>::iterator it;
	char letter[2];
	std::string str;
	bool page_up, page_down;
	int x, y, i, j, num_items;

	char_clear();

	page_up = false;
	page_down = false;
	num_items = 0;
	y = 2;

	// inventory items
	if (items != NULL)
	{
		i = 0;
		for (it = items->begin(); it != items->end(); it++)
		{
			if ((i >= offset) && (i < offset + 26))
			{
				// item description
				letter[0] = 'a' + i - offset;
				letter[1] = 0;
				str = std::string("(") + std::string(letter) + std::string(") ");
				str += (*it)->full_name;
				if ((*it)->weight > 1)
				{
					str += "*";
				}
				char_draw(0, y, str, (can_pick_item(*it)) ? (INVENTORY_NORMAL_STYLE) : (INVENTORY_GREYED_STYLE));
				x = str.size() + 1;

				// in use?
				for (j = 0; j < ES_NUM; j++)
				{
					if (my_creature->equipped[j] == *it)
					{
						str = std::string("(") + equip_slot_descs[j].slot_name + std::string(")");
						char_draw(x, y, str, INVENTORY_ALT_STYLE);
						x += str.size() + 1;
					}
				}
				// ext text provided by item?
				if ((*it)->inventory_ext != std::string(""))
				{
					char_draw(x, y, (*it)->inventory_ext, INVENTORY_ALT_STYLE);
					x += (*it)->inventory_ext.size() + 1;
				}

				y++;
				num_items++;
			} else if (i == offset - 1) {
				char_draw(0, y++, "... (page up)", INVENTORY_ALT_STYLE);
				y++;

				page_up = true;
			} else if (i == offset + 26) {
				y++;
				char_draw(0, y++, "... (page down)", INVENTORY_ALT_STYLE);

				page_down = true;
			}
			i++;
		}
	}

	if (num_items == 0)
	{
		char_draw(0, y++, "nothing", INVENTORY_NORMAL_STYLE);
	}

	y++;
	char_draw(0, y++, "[] = combat damage", INVENTORY_ALT_STYLE);
	char_draw(0, y++, " * = double weight items", INVENTORY_ALT_STYLE);

	// inventory instructions
	switch (mode)
	{
	case IM_INVENTORY:
		{
			str = "Choose an item to put on / activate:";
		} break;

	case IM_WIELD:
		{
			str = "Choose an item to wield:";
		} break;

	case IM_PICKUP:
		{
			str = "Choose an item to pick up:";
		} break;

	case IM_DROP:
		{
			str = "Choose an item to drop:";
		} break;

	default:
		{
			assert(false);
		} break;
	}
	x = str.size() + 1;
	while (str.size() < GRID_WIDTH) {str += ' ';}
	char_draw(0, 0, str, MESSAGE_DEFAULT_STYLE);

	// inventory help text
	if (num_items > 1)
	{
		letter[0] = 'A' + (num_items - 1);
		letter[1] = 0;
		str = std::string("A-") + std::string(letter);
		
		char_draw(x, 0, str, MESSAGE_HIGHLIGHT_STLYE);
		x += str.size() + 1;
	} else if (num_items == 1) {
		char_draw(x, 0, "A", MESSAGE_HIGHLIGHT_STLYE);
		x += 2;
	}
	if (page_up)
	{
		char_draw(x, 0, "PGUP", MESSAGE_HIGHLIGHT_STLYE);
		x += 5;
	}
	if (page_down)
	{
		char_draw(x, 0, "PGDN", MESSAGE_HIGHLIGHT_STLYE);
		x += 5;
	}
	char_draw(x, 0, "SPACE", MESSAGE_HIGHLIGHT_STLYE);
}

// --- pickup_item ---

pickup_item :: pickup_item(creature *_creature, item *_item) :
	my_creature(_creature), my_item(_item)
{
	assert(my_creature != NULL);
	assert(my_item != NULL);
}

void pickup_item :: carry_out(const char_key &key)
{
	std::list<item *>::iterator it;
	map_tile *tile;

	tile = my_creature->get_map_tile();
	for (it = tile->my_items.begin(); it != tile->my_items.end(); it++)
	{
		if (*it == my_item)
		{
			// pick up the item
			my_creature->my_items.push_back(my_item);
			tile->my_items.erase(it);

			// report the event
			if ((my_creature->is_visible()) || (my_creature->is_hero()))
			{
				// hero - describe what's still here?
				if (my_creature->is_hero())
				{
					std::string _message = describe_things_here(tile);
					if (_message.size() > 0)
					{
						display_message _action = display_message(_message);
						stack::push_action(_action);
					}
				}

				// describe the pickup
				{
					display_message _action = display_message(describe_pickup(my_creature, my_item));
					stack::push_action(_action);
				}
			}

			return;
		}
	}
}

// --- drop_item ---

drop_item :: drop_item(creature *_creature, item *_item) :
	my_creature(_creature), my_item(_item)
{
	assert(my_creature != NULL);
	assert(my_item != NULL);
}

void _drop_really(creature *my_creature, map_tile *tile, item *my_item){
	std::list<item *>::iterator it;
	int i;

	for (it = my_creature->my_items.begin(); it != my_creature->my_items.end(); it++)
	{
		if (*it == my_item)
		{
			tile->my_items.push_back(my_item);
			for (i = 0; i < ES_NUM; i++)
			{
				if (my_creature->equipped[i] == *it)
				{
					my_creature->equipped[i] = NULL;
				}
			}
			my_item->dropped();
			my_creature->my_items.erase(it);
			return;
		}
	}
}

void drop_item :: carry_out(const char_key &key)
{
	map_tile *tile;
	tile = my_creature->get_map_tile();

	_drop_really(my_creature,tile,my_item);

	// report the event
	if ((my_creature->is_visible()) || (my_creature->is_hero()))
	{
		display_message _action = display_message(describe_drop(my_creature, my_item));
		stack::push_action(_action);
	}
}

// --- drop_item_to_tile ---

drop_item_to_tile :: drop_item_to_tile(creature *_creature, map_tile *_tile, item *_item) :
	my_creature(_creature), my_tile(_tile), my_item(_item)
{
	assert(my_creature != NULL);
	assert(my_tile != NULL);
	assert(my_item != NULL);
}

void drop_item_to_tile :: carry_out(const char_key &key)
{
	_drop_really(my_creature,my_tile,my_item);
}

// --- equip_item ---

equip_item :: equip_item(creature *_creature, item *_item, equip_slot _slot) :
	my_creature(_creature), my_item(_item), slot(_slot)
{
	assert(my_creature != NULL);
	assert(my_item != NULL);
	assert(slot >= 0);
	assert(slot < ES_NUM);
	assert(my_item->can_equip(slot));
}

void equip_item :: carry_out(const char_key &key)
{
	bool unequip = false;

	if (my_creature->equipped[slot] == my_item)
	{
		// unequip
		my_creature->equipped[slot] = NULL;
		unequip = true;
	} else {
		// equip
		my_creature->equipped[slot] = my_item;
	}

	if ((my_creature->is_visible()) || (my_creature->is_hero()))
	{
		display_message _action = display_message(describe_equip(my_creature, my_item, slot, unequip));
		stack::push_action(_action);
	}
}

// --- activate_item ---

activate_item :: activate_item(creature *_creature, item *_item, action &_cancel_action) :
	my_creature(_creature), my_item(_item), my_cancel_action(NULL)
{
	assert(my_creature != NULL);
	assert(my_item != NULL);

	my_cancel_action = _cancel_action.clone();
	needs_key = my_item->is_directional();
}

activate_item :: ~activate_item()
{
	if (my_cancel_action != NULL)
	{
		delete my_cancel_action;
		my_cancel_action = NULL;
	}
}

void activate_item :: carry_out(const char_key &key)
{
	int dx, dy;

	if (my_item->is_directional())
	{
		if (translate_direction_key(key, dx, dy))
		{
			my_item->activate(my_creature, dx, dy);
		} else if (key.sdl == SDLK_BACKSPACE || key.sdl == SDLK_SPACE ) {
			// cancel
			stack::push_action(*my_cancel_action);
		} else {
			// keep waiting
			stack::push_action(*this);
		}
	} else {
		my_item->activate(my_creature, 0, 0);
	}
}

void activate_item :: draw()
{
	std::string str;
	int x;

	if (my_item->is_directional())
	{
		// query direction
		str = my_item->get_direction_query();
		x = str.size() + 1;
		while (str.size() < GRID_WIDTH) {str += ' ';}
		char_draw(0, 0, str, MESSAGE_DEFAULT_STYLE);

		// direction help text
		char_draw(x, 0, "ARROW", MESSAGE_HIGHLIGHT_STLYE);
		x += 6;
		char_draw(x, 0, "SPACE", MESSAGE_HIGHLIGHT_STLYE);
	}
}

// --- consume_item ---

consume_item :: consume_item(creature *_creature, item *_item) : my_creature(_creature), my_item(_item)
{
	assert(my_item != NULL);
}

void consume_item :: carry_out(const char_key &key)
{
	// remove the item from the creature
	if (my_creature != NULL)
	{
		std::list<item *>::iterator it;
		int i;

		for (it = my_creature->my_items.begin(); it != my_creature->my_items.end(); it++)
		{
			if (*it == my_item)
			{
				for (i = 0; i < ES_NUM; i++)
				{
					if (my_creature->equipped[i] == *it)
					{
						my_creature->equipped[i] = NULL;
					}
				}
				// remove the item
				my_creature->my_items.erase(it);
				break;
			}
		}
	}

	// delete the item
	delete my_item;
	my_item = NULL;
}