// map.h - 'A Quest Too Far'
//
// Copyright 2010 Geoffrey White
//
// see readme.txt for more details.

#ifndef INCLUDED_MAP
#define INCLUDED_MAP

	#include "defines.h"
	#include "stack.h"
	#include "charlib.h"
	#include <string>
	#include <list>

	#define MAP_WIDTH (GRID_WIDTH)
	#define MAP_HEIGHT (GRID_HEIGHT - 1)

	enum level_type
	{
		LT_ENTRY,
		LT_TUNNELS,
		LT_CRYPT,
		LT_GOBLINHALLS,
		LT_LAIR,
		LT_NUM
	};

	// --- map_tile ---

	enum visibility
	{
		V_HIDDEN,
		V_EXPLORED,
		V_VISIBLE
	};

	struct map_tile
	{
		// appearence
		char display_ch;
		char_style display_style;

		// temp fx
		bool is_fx;
		char fx_ch;
		char_style fx_style;

		// flags
		bool is_solid;
		bool is_stairs;

		// visibility
		visibility vis;

		// contents
		class creature *my_creature;
		std::list<class item *> my_items;
	};

	// --- map ---

	class map
	{
	public:
		map();
		~map();
		void clear_items();
		void clear_creatures();
		
		void gen_level(class game &the_game, level_type _level, creature *the_hero);
		void draw();
		void draw_fx();

		map_tile *get_tile(int x, int y);
			// asserts if co-ordinates are off map
		static map_tile *get_tile(map *_map, int x, int y);
			// returns NULL if _map is NULL, or co-ordinates are off map

		std::string get_level_introduction();

	private:
		map_tile tiles[MAP_WIDTH][MAP_HEIGHT];
		level_type level;
		int start_x, start_y; // player start position

		bool add_room(level_type level);
		bool add_tunnel(level_type level);
		bool add_player_start(game &the_game);
		bool add_stairs(game &the_game, level_type level);
		bool add_monster(game &the_game, level_type level);
		int make_connected(game &the_game);
		bool post_process(game &the_game, level_type level);

		bool try_gen_level(game &the_game, level_type level, creature *the_hero);
	};

#endif // INCLUDED_MAP