// util.cpp - 'A Quest Too Far'
//
// Copyright 2010 Geoffrey White
//
// see readme.txt for more details.

#include "util.h"
#include <assert.h>

// --- directional stuff ---

int dir_xs[8] = {0, 1, 1, 1, 0, -1, -1, -1};
int dir_ys[8] = {-1, -1, 0, 1, 1, 1, 0, -1};
int dirs_from[3][3] =
{{	7,	6,	5},
 {	0,	-1,	4},
 {	1,	2,	3}
};

int dir_from(int dx, int dy)
{
	assert(dx >= -1);
	assert(dx <= 1);
	assert(dy >= -1);
	assert(dy <= 1);

	return dirs_from[dx + 1][dy + 1];
}

// --- directional keys ---

struct key_move
{
	SDLKey key;
	int delta_x, delta_y;
};

/*
Movement:
q  w  e     y  k  u            
 \ | /       \ | /             
a- . -d     h- . -l            
 / | \       / | \             
z  x  c     b  j  n

And arrow keys for south, north, west and east
*/
key_move key_moves[] =
{{SDLK_UP,		0,	-1},	// arrows
 {SDLK_k,		0,	-1},		// vi / nethack
 {SDLK_w,		0,	-1},		// 'wasd'
 
 {SDLK_DOWN,	0,	1},
 {SDLK_j,		0,	1},
 {SDLK_x,		0,	1},
 
 {SDLK_LEFT,	-1,	0},
 {SDLK_h,		-1,	0},
 {SDLK_a,		-1,	0},
 
 {SDLK_RIGHT,	1,	0},
 {SDLK_l,		1,	0},
 {SDLK_d,		1,	0},
 
 {SDLK_y,		-1,	-1},
 {SDLK_u,		1,	-1},
 {SDLK_n,		1,	1},
 {SDLK_b,		-1,	1},
 
 {SDLK_q,		-1,	-1},
 {SDLK_e,		1,	-1},
 {SDLK_c,		1,	1},
 {SDLK_z,		-1,	1}
};

bool translate_direction_key(const char_key &key, int &out_dx, int &out_dy)
{
	int i;

	for (i = 0; i < (int)(sizeof(key_moves) / sizeof(key_move)); i++)
	{
		if (key.sdl == key_moves[i].key)
		{
			out_dx = key_moves[i].delta_x;
			out_dy = key_moves[i].delta_y;
			return true;
		}
	}

	return false;
}