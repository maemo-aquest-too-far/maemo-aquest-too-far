// stack.cpp - 'A Quest Too Far'
//
// Copyright 2010 Geoffrey White
//
// see readme.txt for more details.

#include "stack.h"
#include <assert.h>

// --- stack ---

std::vector<action *> stack :: actions;

void stack :: push_action(action &_action)
{
	action *new_action = NULL;

	try
	{
		// push our own copy
		new_action = _action.clone();
		actions.push_back(new_action);
		new_action = NULL;
	} catch (...) {
		if (new_action != NULL)
		{
			delete new_action;
			new_action = NULL;
		}
		throw;
	}
}

int stack :: get_num_actions()
{
	return actions.size();
}

bool stack :: get_needs_key()
{
	assert(!actions.empty());
	
	return actions.back()->get_needs_key();
}

void stack :: carry_out(const char_key &key)
{
	action *do_action = NULL;

	assert(!actions.empty());

	try
	{
		// pop an action
		do_action = actions.back();
		actions.pop_back();

		// do it
		do_action->carry_out(key);

		// delete it
		delete do_action;
		do_action = NULL;
	} catch (...) {
		if (do_action != NULL)
		{
			delete do_action;
			do_action = NULL;
		}
		throw;
	}
}

void stack :: draw()
{
	if (!actions.empty())
	{
		actions.back()->draw();
	}
}
