// fx.cpp - 'A Quest Too Far'
//
// Copyright 2010 Geoffrey White
//
// see readme.txt for more details.

#include "fx.h"
#include <assert.h>

// --- show_fx ---

show_fx :: show_fx(map &_map, fx_tile *_fx) : my_map(_map)
{
	int x, y;

	assert(_fx != NULL);

	for (y = 0; y < MAP_HEIGHT; y++)
	{
		for (x = 0; x < MAP_WIDTH; x++)
		{
			my_fx[x][y] = _fx[(x * MAP_HEIGHT) + y];
		}
	}
}

void show_fx :: carry_out(const char_key &key)
{
	int x, y;

	for (y = 0; y < MAP_HEIGHT; y++)
	{
		for (x = 0; x < MAP_WIDTH; x++)
		{
			my_map.get_tile(x, y)->is_fx = my_fx[x][y].is_fx;
			my_map.get_tile(x, y)->fx_ch = my_fx[x][y].ch;
			my_map.get_tile(x, y)->fx_style = my_fx[x][y].style;
		}
	}
}

// --- clear_fx ---

void clear_fx :: carry_out(const char_key &key)
{
	int x, y;

	for (y = 0; y < MAP_HEIGHT; y++)
	{
		for (x = 0; x < MAP_WIDTH; x++)
		{
			my_map.get_tile(x, y)->is_fx = false;
		}
	}
}