// describe.cpp - 'A Quest Too Far'
//
// Copyright 2010 Geoffrey White
//
// see readme.txt for more details.

#include "describe.h"
#include "creature.h"
#include "item.h"
#include "map.h"
#include "charlib.h"
#include <assert.h>

std::string describe_attack(creature *attacker, creature *target, bool attack_hit, int damage_amount)
{
	std::string message;

	assert(attacker != NULL);
	assert(target != NULL);

	// basic description
	if (attack_hit)
	{
		if (attacker->get_use_plural_verb())
		{
			message = attacker->get_name_the() + " hit " + target->get_name_the();
		} else {
			message = attacker->get_name_the() + " hits " + target->get_name_the();
		}

		// append damage
		message += " [" + char_int_str(damage_amount) + "].";
	} else {
		if (attacker->get_use_plural_verb())
		{
			message = attacker->get_name_the() + " miss " + target->get_name_the();
		} else {
			message = attacker->get_name_the() + " misses " + target->get_name_the();
		}
		message += ".";
	}

	// capitalize and return
	assert(message.size() > 0);
	message[0] = toupper(message[0]);
	return message;
}

std::string describe_poison(creature *attacker, creature *target, int turns)
{
	std::string message;

	// ('attacker' may be NULL)
	assert(target != NULL);

	if (target->get_use_plural_verb())
	{
		message = target->get_name_the() + " are poisoned!";
	} else {
		message = target->get_name_the() + " is poisoned!";
	}

	// capitalize and return
	assert(message.size() > 0);
	message[0] = toupper(message[0]);
	return message;
}

std::string describe_burn(creature *attacker, creature *target, int damage_amount)
{
	std::string message;

	// ('attacker' may be NULL)
	assert(target != NULL);

	if (target->get_use_plural_verb())
	{
		message = target->get_name_the() + " are burned";
	} else {
		message = target->get_name_the() + " is burned";
	}
	
	// append damage
	message += " [" + char_int_str(damage_amount) + "]!";

	// capitalize and return
	assert(message.size() > 0);
	message[0] = toupper(message[0]);
	return message;
}

std::string describe_spine(creature *attacker, creature *target, int damage_amount)
{
	std::string message;

	// ('attacker' may be NULL)
	assert(target != NULL);

	if (target->get_use_plural_verb())
	{
		message = target->get_name_the() + " are hit by a spine";
	} else {
		message = target->get_name_the() + " is hit by a spine";
	}
	
	// append damage
	message += " [" + char_int_str(damage_amount) + "]!";

	// capitalize and return
	assert(message.size() > 0);
	message[0] = toupper(message[0]);
	return message;
}

std::string describe_hurt(creature *attacker, creature *target, damage_type type)
{
	std::string message;

	// ('attacker' may be NULL)
	assert(target != NULL);

	if (target->get_use_plural_verb())
	{
		message = target->get_name_the() + " are badly wounded.";
	} else {
		message = target->get_name_the() + " is badly wounded.";
	}

	// capitalize and return
	assert(message.size() > 0);
	message[0] = toupper(message[0]);
	return message;
}

std::string describe_killed(creature *attacker, creature *target, damage_type type)
{
	std::string message;

	// ('attacker' may be NULL)
	assert(target != NULL);

	switch (type)
	{
	case DT_FIRE:
		{
			if (target->get_use_plural_verb())
			{
				message = target->get_name_the() + " are toast!";
			} else {
				message = target->get_name_the() + " is toast!";
			}
		} break;

	case DT_DISINTEGRATE:
		{
			if (target->get_use_plural_verb())
			{
				message = target->get_name_the() + " are disintegrated!";
			} else {
				message = target->get_name_the() + " is disintegrated!";
			}
		} break;

	default:
		{
			if (target->get_use_plural_verb())
			{
				message = target->get_name_the() + " die.";
			} else {
				message = target->get_name_the() + " dies.";
			}
		} break;
	}

	// capitalize and return
	assert(message.size() > 0);
	message[0] = toupper(message[0]);
	return message;
}

std::string describe_warning(creature *who, creature *target, warning_type type)
{
	std::string message;

	switch (type)
	{
	case WT_GROWL:
		{
			if (who->get_use_plural_verb())
			{
				message = who->get_name_the() + " growl at " + target->get_name_the() + ".";
			} else {
				message = who->get_name_the() + " growls at " + target->get_name_the() + ".";
			}
		} break;

	case WT_ROAR:
		{
			if (who->get_use_plural_verb())
			{
				message = who->get_name_the() + " roar at " + target->get_name_the() + ".";
			} else {
				message = who->get_name_the() + " roars at " + target->get_name_the() + ".";
			}
		} break;

	default:
		{
			assert(false);
		} break;
	}

	// capitalize and return
	assert(message.size() > 0);
	message[0] = toupper(message[0]);
	return message;
}

std::string describe_things_here(struct map_tile *tile)
{
	std::string message;

	assert(tile != NULL);

	// describe items
	if (tile->my_items.empty())
	{
		message = "";
	} else if (tile->my_items.size() == 1) {
		message += "There is a " + tile->my_items.front()->short_name + " here.";
	} else {
		message += "There are several items here.";
	}

	// describe room features
	if (tile->display_ch == '=')
	{
		if (message.size() == 0)
		{
			message = "There is a dusty coffin here.";
		} else {
			message += " There is a dusty coffin here.";
		}
	}

	return message;
}

std::string describe_pickup(creature *who, class item *what)
{
	std::string message;

	assert(who != NULL);
	assert(what != NULL);

	if (who->get_use_plural_verb())
	{
		message = who->get_name_the() + " pick up a " + what->short_name + ".";
	} else {
		message = who->get_name_the() + " picks up a " + what->short_name + ".";
	}

	// capitalize and return
	assert(message.size() > 0);
	message[0] = toupper(message[0]);
	return message;
}

std::string describe_drop(creature *who, class item *what)
{
	std::string message;

	assert(who != NULL);
	assert(what != NULL);

	if (who->get_use_plural_verb())
	{
		message = who->get_name_the() + " drop a " + what->short_name + ".";
	} else {
		message = who->get_name_the() + " drops a " + what->short_name + ".";
	}

	// capitalize and return
	assert(message.size() > 0);
	message[0] = toupper(message[0]);
	return message;
}

std::string describe_equip(creature *who, item *what, equip_slot slot, bool unequip)
{
	std::string message;

	assert(who != NULL);
	assert(what != NULL);
	assert(slot >= 0);
	assert(slot < ES_NUM);

	if (unequip)
	{
		if (who->get_use_plural_verb())
		{
			message = who->get_name_the() + " " + equip_slot_descs[slot].unequip_plural_text + " " + what->short_name + ".";
		} else {
			message = who->get_name_the() + " " + equip_slot_descs[slot].unequip_text + " " + what->short_name + ".";
		}
	} else {
		if (who->get_use_plural_verb())
		{
			message = who->get_name_the() + " " + equip_slot_descs[slot].equip_plural_text + " " + what->short_name + ".";
		} else {
			message = who->get_name_the() + " " + equip_slot_descs[slot].equip_text + " " + what->short_name + ".";
		}
	}

	// capitalize and return
	assert(message.size() > 0);
	message[0] = toupper(message[0]);
	return message;
}

std::string describe_candle(creature *who, item *what, candle_action _action)
{
	std::string message;

	assert(who != NULL);
	assert(what != NULL);

	switch (_action)
	{
	case CA_LIGHT:
		{
			if (who->get_use_plural_verb())
			{
				message = who->get_name_the() + " light a " + what->short_name + ".";
			} else {
				message = who->get_name_the() + " lights a " + what->short_name + ".";
			}
		} break;

	case CA_UNLIGHT:
		{
			if (who->get_use_plural_verb())
			{
				message = who->get_name_the() + " put out a " + what->short_name + ".";
			} else {
				message = who->get_name_the() + " puts out a " + what->short_name + ".";
			}
		} break;

	case CA_EXPIRE:
		{
			if (who->get_use_plural_verb())
			{
				message = who->get_name_the_object() + " " + what->short_name + " runs out.";
			} else {
				message = who->get_name_the_object() + " " + what->short_name + " runs out.";
			}
		} break;

	default:
		{
			assert(false);
		} break;
	}

	// capitalize and return
	assert(message.size() > 0);
	message[0] = toupper(message[0]);
	return message;
}

std::string describe_healed(creature *who)
{
	std::string message;

	assert(who != NULL);

	if (who->get_use_plural_verb())
	{
		message = who->get_name_the() + " feel much better.";
	} else {
		message = who->get_name_the() + " looks much better.";
	}

	// capitalize and return
	assert(message.size() > 0);
	message[0] = toupper(message[0]);
	return message;
}

std::string describe_poison_off(creature *who)
{
	std::string message;

	assert(who != NULL);

	if (who->get_use_plural_verb())
	{
		message = who->get_name_the() + " recover from the poison.";
	} else {
		message = who->get_name_the() + " recovers from the poison.";
	}

	// capitalize and return
	assert(message.size() > 0);
	message[0] = toupper(message[0]);
	return message;
}

const char *decline_messages[8] =
{
//	 ########################################################
	// worst
	"You despair at your weakness.", // severe
	"You feel weak and frail.  You wish you never betted away that 'Amulet of Yendor' thingy.",
	"Your back is worsening.  Think you'll have to skip the after-adventure party!",
	"More aches and pains develop.  You used to be able to carry far more loot than this!",
	"All this exercise is exhausting - but you worry about the fate of your grandchildren.",
	"You feel a shooting pain in your back again.  It wasn't like this back in the day!",
	"You feel stiff due to arthritis.  Too many years burning the candle at both ends.",
	"You feel a sudden twinge of pain in your back.  You aren't sure you can carry on with all this stuff!",
	// max
//	 ########################################################
};

std::string describe_decline(creature *who)
{
	std::string message;
	int carry_limit, msg_ix;
	
	assert(who != NULL);
	assert(who->is_hero());
	
	carry_limit = who->get_carry_max();
	msg_ix = carry_limit - 8;
	if (msg_ix < 0) {msg_ix = 0;}
	if (msg_ix > 7) {msg_ix = 7;}

	message = decline_messages[msg_ix];
	message += " [weight limit " + char_int_str(carry_limit) + "]";

	return message;
}

std::string describe_spines(creature *who)
{
	std::string message;

	assert(who != NULL);

	if (who->get_use_plural_verb())
	{
		message = who->get_name_the() + " launch tail spines.";
	} else {
		message = who->get_name_the() + " launches tail spines.";
	}

	// capitalize and return
	assert(message.size() > 0);
	message[0] = toupper(message[0]);
	return message;
}