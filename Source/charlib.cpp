// charlib.cpp
//
// by Geoffrey White

#include "charlib.h"
#include <sstream>
#include <iostream>
#include <stdexcept>
#include <assert.h>

// --- defines ---

#define DIV_ROUND_NEAR(x, y) (((x) + ((y) / 2)) / (y))
#define DIV_ROUND_UP(x, y) (((x) + ((y) - 1)) / (y))
#define DIV_ROUND_EQUAL_DOWN(x, y) (((x * 2) - 1) / (y * 2))
#define MIN(a, b) ((a) < (b) ? (a) : (b))
#define MAX(a, b) ((a) > (b) ? (a) : (b))

// --- state ---

struct char_state
{
	// config
	char_config config;

	// window
	SDL_Surface *video_surface; // (shortcut, SDL is responsible for freeing this, etc)

	// other state
	bool has_quit;
};
char_state char_state;

// --- error handling ---

#ifdef WIN32
	#include <windows.h>
	#include <new.h>

	static int main_new_handler(size_t sz)
	{
		throw std::bad_alloc();
		return 0;
	}
#endif // WIN32

void char_debug_out(const std::string &message)
{
#ifdef _DEBUG
	std::string msg = char_state.config.window_title + std::string(": ") + message + std::string("\n");

#ifdef WIN32
	OutputDebugString(msg.c_str());
#else
	std::cout << msg;
#endif // WIN32
#endif // _DEBUG
}

std::string char_int_str(int i)
{
	std::stringstream ss;

	ss << i;
	return ss.str();
}

void char_display_error(const std::string &message)
{
	std::string msg = char_state.config.window_title + std::string(": ") + message + std::string("\n");

#ifdef WIN32
	OutputDebugString(msg.c_str());

	// windows message box
	MessageBeep(0);
	MessageBox(GetFocus(), message.c_str(), char_state.config.window_title.c_str(), MB_OK | MB_ICONEXCLAMATION);
#else
	std::cerr << msg;
#endif // WIN32
}

static void char_do_error(const std::string &message, bool is_sdl)
{
	std::string msg;

	if (is_sdl)
	{
		msg = message + std::string(" [") + std::string(SDL_GetError()) + std::string("]");
	} else {
		msg = message;
	}

	throw std::runtime_error(message.c_str()); // abort with error
}

// --- low level graphics ---

static void char_style_rect(int x, int y, int w, int h, const char_style &style)
{
	// note: video_surface must be locked
	int x2, y2; // current position
	u8 *dest;
	u32 pixel=0; // current pixel
	u8 r, g, b;
	u32 cache_s=0, cache_r; // cache
	SDL_PixelFormat *format; // format
	int bpp;
	int r_diff, g_diff, b_diff; // precompute

	format = char_state.video_surface->format;
	bpp = format->BytesPerPixel;
	assert(bpp <= 4); // SDL uses u32 to represent a pixel

	assert(x >= 0);
	assert(y >= 0);
	assert(x + w <= char_state.video_surface->w);
	assert(y + h <= char_state.video_surface->h);

	r_diff = DIV_ROUND_UP((style.fore_r - style.back_r) << 8, 255);
	g_diff = DIV_ROUND_UP((style.fore_g - style.back_g) << 8, 255);
	b_diff = DIV_ROUND_UP((style.fore_b - style.back_b) << 8, 255);
		// error when remultiplied by 255 is +0 to +254, which is zero after >> 8

	dest = (u8 *)char_state.video_surface->pixels + (y * char_state.video_surface->pitch) + (x * bpp);
	
	// ensure cache is invalid for first pixel
	memcpy(&cache_s, dest, bpp);
	cache_s ^= 0xFFFFFFFF;

	switch (bpp)
	{
	case 4:
		{
			// 32 bit case
			for (y2 = 0; y2 < h; y2++)
			{
				for (x2 = 0; x2 < w; x2++)
				{
					// get the pixel
					pixel = *((u32 *)dest);

					if (pixel != cache_s)
					{
						cache_s = pixel;

						// convert to RGB
						SDL_GetRGB(pixel, format, &r, &g, &b);

						// remap colour
						r = style.back_r + ((r * r_diff) >> 8);
						g = style.back_g + ((g * g_diff) >> 8);
						b = style.back_b + ((b * b_diff) >> 8);

						// convert back to pixel
						cache_r = SDL_MapRGB(format, r, g, b);
					}

					// write the pixel
					*((u32 *)dest) = cache_r;

					dest += 4;
				}
				dest += char_state.video_surface->pitch - (w * 4);
			}
		} break;

	case 2:
		{
			// 16 bit case
			for (y2 = 0; y2 < h; y2++)
			{
				for (x2 = 0; x2 < w; x2++)
				{
					// get the pixel
					pixel = *((u16 *)dest);

					if (pixel != cache_s)
					{
						cache_s = pixel;

						// convert to RGB
						SDL_GetRGB(pixel, format, &r, &g, &b);

						// remap colour
						r = style.back_r + ((r * r_diff) >> 8);
						g = style.back_g + ((g * g_diff) >> 8);
						b = style.back_b + ((b * b_diff) >> 8);

						// convert back to pixel
						cache_r = SDL_MapRGB(format, r, g, b);
					}

					// write the pixel
					*((u16 *)dest) = cache_r;

					dest += 2;
				}
				dest += char_state.video_surface->pitch - (w * 2);
			}
		} break;

	case 1:
		{
			// 8 bit case
			for (y2 = 0; y2 < h; y2++)
			{
				for (x2 = 0; x2 < w; x2++)
				{
					// get the pixel
					pixel = *((u8 *)dest);

					if (pixel != cache_s)
					{
						cache_s = pixel;

						// convert to RGB
						SDL_GetRGB(pixel, format, &r, &g, &b);

						// remap colour
						r = style.back_r + ((r * r_diff) >> 8);
						g = style.back_g + ((g * g_diff) >> 8);
						b = style.back_b + ((b * b_diff) >> 8);

						// convert back to pixel
						cache_r = SDL_MapRGB(format, r, g, b);
					}

					// write the pixel
					*((u8 *)dest) = cache_r;

					dest += 1;
				}
				dest += char_state.video_surface->pitch - w;
			}
		} break;

	default:
		{
			// general case (memcpy's can be slow)
			for (y2 = 0; y2 < h; y2++)
			{
				for (x2 = 0; x2 < w; x2++)
				{
					// get the pixel
					memcpy(&pixel, dest, bpp);

					if (pixel != cache_s)
					{
						cache_s = pixel;

						// convert to RGB
						SDL_GetRGB(pixel, format, &r, &g, &b);

						// remap colour
						r = style.back_r + ((r * r_diff) >> 8);
						g = style.back_g + ((g * g_diff) >> 8);
						b = style.back_b + ((b * b_diff) >> 8);

						// convert back to pixel
						cache_r = SDL_MapRGB(format, r, g, b);
					}

					// write the pixel
					memcpy(dest, &cache_r, bpp);

					dest += bpp;
				}
				dest += char_state.video_surface->pitch - (w * bpp);
			}
		} break;
	}
}

static void char_scale_region(SDL_Surface *source_surface, int source_x, int source_y, int source_width, int source_height,
							  SDL_Surface *dest_surface, int dest_x, int dest_y, int dest_width, int dest_height)
{
	// high quality anti-aliased software image scaling;
	// source and dest surfaces must be locked and in internal format;
	// border pixels in source shouldn't be important.
	int x, y;							// dest co-ordinates relative to source_x, source_y
	int x2, y2;							// source co-ordinates relative to dest_x, dest_y
	int x2_min, y2_min, x2_max, y2_max;	// source region affecting this dest pixel
	int min_num, max_num;				// see below
	int x_blend_num, y_blend_num, x_blend_denom, y_blend_denom;
	u32 weight, y_weight, sum_weight;
	u32 r, g, b, r_sum, g_sum, b_sum;	// colour and accumulated colour
	bool source_locked = false, dest_locked = false;
	u32 *dest;

	assert(source_x >= 0);
	assert(source_y >= 0);
	assert(source_x + source_width <= source_surface->w);
	assert(source_y + source_height <= source_surface->h);
	assert(dest_x >= 0);
	assert(dest_y >= 0);
	assert(dest_x + dest_width <= dest_surface->w);
	assert(dest_y + dest_height <= dest_surface->h);

	try
	{
		// lock the surfaces
		if (SDL_LockSurface(dest_surface) != 0)
		{
			char_do_error("Couldn't lock surface for scaling (1)", true);
		}
		dest_locked = true;
		if (SDL_LockSurface(source_surface) != 0)
		{
			char_do_error("Couldn't lock surface for scaling (2)", true);
		}
		source_locked = true;

		// --- scale image ---
		//
		// we map the source image to 0 .. 1, and linearly interpolate between each pixel to get
		// what we will call the 'perfect image':
		//
		// source			0    1    2    3		(4 segments in this example)
		//					-----+++++-----+++++
		//					-----+++++     +++++
		//					     +++++
		//
		// perfect image	0                  1	(continuous)
		//					*    *   -*--  *	*
		//					*--  * --    --*----*
		//					   --*-
		//
		//  - linear interpolation makes magnification look much nicer
		//  - notice that data from 1 sample to far right 'leaks' into our perfect image;
		//    let's fix that:
		source_width -= 1;
		source_height -= 1;
		//  - since border pixels are only represented half as much as internal ones, they
		//    shouldn't contain important data.
		//
		// the destination image is constructed by cutting the perfect image into segments of equal length,
		// and measuring their area:
		//
		// destination		0   1   2   3   4		(5 segments in this example)
		//					----++++----++++----
		//					----++++-    +++----
		//					   -+++
		//					9	11	5	7	8
		//
		//  - this area sampling process makes minification look good
		//  - in practice this algorithm is performed in two dimensions. :)
		//
		for (y = 0; y < dest_height; y++)
		{
			// calculate which source segments are at least partially under this dest segment
			y2_min = (source_height * y) / dest_height;
			y2_max = DIV_ROUND_EQUAL_DOWN(source_height * (y + 1), dest_height);
			assert(y2_min >= 0);
			assert(y2_max < source_height);

			dest = (u32 *)(((u8 *)dest_surface->pixels) + ((dest_y + y) * dest_surface->pitch));
			dest += dest_x;
			for (x = 0; x < dest_width; x++)
			{
				// calculate which source segments are at least partially under this dest segment
				x2_min = (source_width * x) / dest_width;
				x2_max = DIV_ROUND_EQUAL_DOWN(source_width * (x + 1), dest_width);
				assert(x2_min >= 0);
				assert(x2_max < source_width);

				// sample over the dest area
				r_sum = g_sum = b_sum = sum_weight = 0;
				for (y2 = y2_min; y2 <= y2_max; y2++)
				{
					// calculate vertical source / dest segment overlap region
					// (converting all fractions to common denominator: dest_height * source_height)
					//
					// find minimum of intersection
					min_num = MAX(y * source_height,					// y / dest_height
								  y2 * dest_height);					// y2 / source_height

					// find maximum of intersection
					max_num = MIN((y + 1) * source_height,				// y + 1 / dest_height
								  (y2 + 1) * dest_height);				// y2 + 1 / source_height
					assert(max_num >= min_num);

					// calculate weight by intersect segment length
					y_weight = max_num - min_num;

					// calculate blend position (midpoint of the intersection)
					// (same method as for x, below)
					y_blend_num = (min_num + max_num) - (y2 * 2 * dest_height);
					y_blend_denom = 2 * dest_height;

					for (x2 = x2_min; x2 <= x2_max; x2++)
					{
						// calculate vertical source / dest segment overlap region
						// (converting all fractions to common denominator: dest_width * source_width)
						//
						// find minimum of intersection
						min_num = MAX(x * source_width,					// x / dest_width
									  x2 * dest_width);					// x2 / source_width

						// find maximum of intersection
						max_num = MIN((x + 1) * source_width,			// x + 1 / dest_height
									  (x2 + 1) * dest_width);			// x2 + 1 / source_width
						assert(max_num >= min_num);

						// calculate weight by intersect segment length
						weight = y_weight * (max_num - min_num);

						// calculate blend position (midpoint of the intersection)
						//
						// mid point of intersection:
						//  (min_num + max_num) / (2 * dest_width * source_width)
						// left edge of source segment (to subtract):
						//  x2 / source_width
						// width of source segment:
						//  1 / source_width
						// source_width's all cancel; denom is thus 2 * dest_width
						x_blend_num = (min_num + max_num) - (x2 * 2 * dest_width);
						x_blend_denom = 2 * dest_width;

						{
							// perform bilinear sample on the segment
							u32 r_sum, g_sum, b_sum, r2_sum, g2_sum, b2_sum;
							int rest = x_blend_denom - x_blend_num;
							u32 pixel1, pixel2;
							u8 *source;

							// sample and interpolate top two pixels
							source = ((u8 *)source_surface->pixels) +
									 ((source_y + y2) * source_surface->pitch) +
									 ((source_x + x2) * 4);
							pixel1 = *((u32 *)source);
							pixel2 = *((u32 *)(source + 4));
							r_sum = (((pixel1 & 0xFF000000) >> 24) * rest) + (((pixel2 & 0xFF000000) >> 24) * x_blend_num);
							g_sum = (((pixel1 & 0x00FF0000) >> 16) * rest) + (((pixel2 & 0x00FF0000) >> 16) * x_blend_num);
							b_sum = (((pixel1 & 0x0000FF00) >> 8) * rest) + (((pixel2 & 0x0000FF00) >> 8) * x_blend_num);

							// sample and interpolate bottom two pixels
							source += source_surface->pitch;
							pixel1 = *((u32 *)source);
							pixel2 = *((u32 *)(source + 4));
							r2_sum = (((pixel1 & 0xFF000000) >> 24) * rest) + (((pixel2 & 0xFF000000) >> 24) * x_blend_num);
							g2_sum = (((pixel1 & 0x00FF0000) >> 16) * rest) + (((pixel2 & 0x00FF0000) >> 16) * x_blend_num);
							b2_sum = (((pixel1 & 0x0000FF00) >> 8) * rest) + (((pixel2 & 0x0000FF00) >> 8) * x_blend_num);

							// combine samples
							rest = y_blend_denom - y_blend_num;
							r = ((r_sum * rest) + (r2_sum * y_blend_num)) / (x_blend_denom * y_blend_denom);
							g = ((g_sum * rest) + (g2_sum * y_blend_num)) / (x_blend_denom * y_blend_denom);
							b = ((b_sum * rest) + (b2_sum * y_blend_num)) / (x_blend_denom * y_blend_denom);
						}

						// accumulate weighted sample
						r_sum += r * weight;
						g_sum += g * weight;
						b_sum += b * weight;
						sum_weight += weight;
					}
				}
				assert(sum_weight < 256 * 256 * 256); // range sanity check
				r_sum /= sum_weight;
				g_sum /= sum_weight;
				b_sum /= sum_weight;

				// write the pixel
				*(dest++) = (((u8)r_sum) << 24) | (((u8)g_sum) << 16) | (((u8)b_sum) << 8);
			}
		}

		// unlock the surfaces
		SDL_UnlockSurface(source_surface);
		source_locked = false;
		SDL_UnlockSurface(dest_surface);
		dest_locked = false;
	} catch (...) {
		if (source_locked)
		{
			SDL_UnlockSurface(source_surface);
			source_locked = false;
		}
		if (dest_locked)
		{
			SDL_UnlockSurface(dest_surface);
			dest_locked = false;
		}
		throw;
	}
}

// --- surfaces ---

struct char_surface_impl
{
	// decl
	char_surface_decl decl;

	// surfaces
	SDL_Surface *source;	// source scale, internal pixel format
	SDL_Surface *display;	// display scale, display pixel format
};
SDL_PixelFormat char_internal_pixel_format;

static void char_load_surface(char_surface_impl &impl, const char_surface_decl &decl)
{
	SDL_Surface *intermediate = NULL;

	// create the surface record
	impl.decl = decl;
	impl.source = NULL;
	impl.display = NULL;

	try
	{
		// load the image-file (intermediate)
		SDL_RWops *rwop;
		rwop=SDL_RWFromFile(decl.file_name.c_str(), "r");
		intermediate = IMG_LoadPNG_RW(rwop);
		if (intermediate == NULL)
		{
			char_do_error("Couldn't load tile-image '" + decl.file_name + "' (1)", true);
		}

		// convert to internal format
		impl.source = SDL_ConvertSurface(intermediate, &(char_internal_pixel_format), SDL_SWSURFACE);
		if (impl.source == NULL)
		{
			char_do_error("Couldn't load tile-image '" + decl.file_name + "' (2)", true);
		}

		// free intermediate
		SDL_FreeSurface(intermediate);
		intermediate = NULL;
	} catch (...) {
		if (intermediate != NULL)
		{
			SDL_FreeSurface(intermediate);
			intermediate = NULL;
		}
		throw;
	}
}

static void char_scale_surface(char_surface_impl &surface, int cells_across, int cells_down,
														   int cell_scaled_width, int cell_scaled_height)
{
	SDL_Surface *surface_intermediate = NULL;
	int x, y, source_tile_w, source_tile_h;

	try
	{
		// clear any existing display surface
		if (surface.display != NULL)
		{
			SDL_FreeSurface(surface.display);
			surface.display = NULL;
		}

		// create the intermediate (scaled, but still internal format) surface
		surface_intermediate = SDL_CreateRGBSurface(SDL_SWSURFACE,
													cells_across * cell_scaled_width,
													cells_down * cell_scaled_height,
													32, 0xFF000000, 0x00FF0000, 0x0000FF00, 0x000000FF);
		if (surface_intermediate == NULL)
		{
			char_do_error("Couldn't scale surface '" + surface.decl.file_name+ "' (1)", true);
		}

		source_tile_w = surface.source->w / cells_across;
		source_tile_h = surface.source->h / cells_down;
		for (y = 0; y < surface.decl.grid_height; y++)
		{
			for (x = 0; x < surface.decl.grid_width; x++)
			{
				char_scale_region(surface.source,
								  x * source_tile_w, y * source_tile_w,
								  source_tile_w, source_tile_h,
								  surface_intermediate,
								  x * cell_scaled_width, y * cell_scaled_height,
								  cell_scaled_width, cell_scaled_height);
			}
		}

		// create the final (display format) surface
		surface.display = SDL_DisplayFormat(surface_intermediate);
		if (surface.display == NULL)
		{
			char_do_error("Couldn't scale surface '" + surface.decl.file_name+ "' (4)", true);
		}

		// free intermediate
		SDL_FreeSurface(surface_intermediate);
		surface_intermediate = NULL;
	} catch (...) {
		if (surface_intermediate != NULL)
		{
			SDL_FreeSurface(surface_intermediate);
			surface_intermediate = NULL;
		}
		throw;
	}
}

static void char_free_surface(char_surface_impl &surface)
{
	if (surface.display != NULL)
	{
		SDL_FreeSurface(surface.display);
		surface.display = NULL;
	}

	if (surface.source != NULL)
	{
		SDL_FreeSurface(surface.source);
		surface.source = NULL;
	}
}

// --- graphics ---

struct char_tile_state
{
	char ch;
	char_style style;
	bool dirty;
};
char_tile_state *char_tile_map;

struct char_gfx_state
{
	// scale
	int scale_num, scale_denom; // exact scale (aspect tile -> size for display)
	int display_tile_w, display_tile_h; // tiles are scaled to this size (rounded up)
	int display_offs_x, display_offs_y; // grid display offset

	// surfaces
	char_surface_impl char_ascii_surface;

	// reset state
	bool post_reset;
};
char_gfx_state char_gfx_state;
char_style char_default_style;

#define SCALE_X(x) (((x) * char_state.config.aspect_width * char_gfx_state.scale_num) / char_gfx_state.scale_denom)
#define SCALE_Y(y) (((y) * char_state.config.aspect_height * char_gfx_state.scale_num) / char_gfx_state.scale_denom)

static void char_gfx_reset(int width, int height)
{
	SDL_Surface *screen;
	int window_width, window_height;
	char_tile_state *tile;
	int x, y;
	Uint32 flags = SDL_SWSURFACE;
#ifdef MAEMO
	flags |= SDL_FULLSCREEN;
#endif
	screen = SDL_SetVideoMode(width, height, 24, flags);
	if (!screen)
	{
		// have another go...
		screen = SDL_SetVideoMode(width, height, 16, flags);
		if (!screen)
		{
			char_do_error("SDL_SetVideoMode failed", true);
		}
	}

	//No need for mouse cursor
	SDL_ShowCursor(0);

	//Enable repeat
	SDL_EnableKeyRepeat(SDL_DEFAULT_REPEAT_DELAY,SDL_DEFAULT_REPEAT_INTERVAL);

	// get video details
	char_state.video_surface = SDL_GetVideoSurface();
	window_width = MAX(char_state.video_surface->w, 2);
	window_height = MAX(char_state.video_surface->h, 2);

	// clear the surface to black, just in case it isn't already
	SDL_FillRect(char_state.video_surface, NULL, SDL_MapRGBA(char_state.video_surface->format, 0, 0, 0, 255));

	// determine exact scale
	if (window_width * char_state.config.grid_height * char_state.config.aspect_height <
		window_height * char_state.config.grid_width * char_state.config.aspect_width)
	{
		char_gfx_state.scale_num = window_width;
		char_gfx_state.scale_denom = char_state.config.grid_width * char_state.config.aspect_width;
	} else {
		char_gfx_state.scale_num = window_height;
		char_gfx_state.scale_denom = char_state.config.grid_height * char_state.config.aspect_height;
	}

	// determine tile size (rounded)
	char_gfx_state.display_tile_w = DIV_ROUND_UP(char_state.config.aspect_width * char_gfx_state.scale_num, char_gfx_state.scale_denom);
	char_gfx_state.display_tile_h = DIV_ROUND_UP(char_state.config.aspect_height * char_gfx_state.scale_num, char_gfx_state.scale_denom);

	// determine display layout (rounded pixels)
	char_gfx_state.display_offs_x = (window_width - SCALE_X(char_state.config.grid_width)) / 2;
	char_gfx_state.display_offs_y = (window_height - SCALE_Y(char_state.config.grid_height)) / 2;

	// force redraw
	tile = &(char_tile_map[0]);
	for (y = 0; y < char_state.config.grid_height; y++)
	{
		for (x = 0; x < char_state.config.grid_width; x++)
		{
			tile->dirty = true;
			tile++;
		}
	}
}

static void char_gfx_init()
{
	int w, h, num, denom;

	// create our internal pixel format
	memset(&char_internal_pixel_format, 0, sizeof(char_internal_pixel_format));
	char_internal_pixel_format.BitsPerPixel = 32;
	char_internal_pixel_format.BytesPerPixel = 4;
	char_internal_pixel_format.Rmask = 0xFF000000;
	char_internal_pixel_format.Rshift = 24;
	char_internal_pixel_format.Rloss = 0;
	char_internal_pixel_format.Gmask = 0x00FF0000;
	char_internal_pixel_format.Gshift = 16;
	char_internal_pixel_format.Gloss = 0;
	char_internal_pixel_format.Bmask = 0x0000FF00;
	char_internal_pixel_format.Bshift = 8;
	char_internal_pixel_format.Bloss = 0;
	char_internal_pixel_format.Amask = 0x000000FF;
	char_internal_pixel_format.Ashift = 0;
	char_internal_pixel_format.Aloss = 0;

	// create the default tile style
	char_default_style = mk_char_style(255, 255, 255, 0, 0, 0);

	// init tile map
	assert(char_state.config.grid_width > 0);
	assert(char_state.config.grid_height > 0);
	char_tile_map = new char_tile_state[char_state.config.grid_width * char_state.config.grid_height];
	char_clear();

	// init ascii surface
	char_load_surface(char_gfx_state.char_ascii_surface, char_state.config.ascii_decl);

	// init window size (up to 90% of desktop size)
	w = 800;//(SDL_GetVideoInfo()->current_w * 10) / 10;
	h = 480;//(SDL_GetVideoInfo()->current_h * 10) / 10;
	if ((w * char_state.config.grid_height * char_state.config.aspect_height) <
		(h * char_state.config.grid_width * char_state.config.aspect_width))
	{
		num = w;
		denom = char_state.config.grid_width * char_state.config.aspect_width;
	} else {
		num = h;
		denom = char_state.config.grid_height * char_state.config.aspect_height;
	}	
	char_gfx_reset((char_state.config.grid_width * char_state.config.aspect_width * num) / denom,
				   (char_state.config.grid_height * char_state.config.aspect_height * num) / denom);
	char_gfx_state.post_reset = true;
}

static void char_gfx_shutdown()
{
	// free ascii surface
	char_free_surface(char_gfx_state.char_ascii_surface);

	// free tile map
	delete[] char_tile_map;
	char_tile_map = NULL;
}

//#define CHAR_PROFILE

static void char_gfx_update()
{
	char_tile_state *tile;
	SDL_Rect src, dest;
	int x, y, tile_x, tile_y, ret;
#ifdef CHAR_PROFILE
	int t1, t2;
#endif // CHAR_PROFILE
	char ch;

	// has device been reset?
	if (char_gfx_state.post_reset)
	{
#ifdef CHAR_PROFILE
		t1 = SDL_GetTicks();
#endif // CHAR_PROFILE

		// rescale graphics
		char_scale_surface(char_gfx_state.char_ascii_surface,
						   char_gfx_state.char_ascii_surface.decl.grid_width,
						   char_gfx_state.char_ascii_surface.decl.grid_height,
						   char_gfx_state.display_tile_w,
						   char_gfx_state.display_tile_h);

#ifdef CHAR_PROFILE
		t2 = SDL_GetTicks();
		char_debug_out(std::string("rescale took ") + char_int_str(t2 - t1) + std::string(" ms"));
#endif // CHAR_PROFILE

		char_gfx_state.post_reset = false;

#ifndef MAEMO /*We get only black screen if not redraw on maemo*/
		// don't redraw in the first frame after a reset; on some platforms this causes
		// flicker whilst resizing the window.
		return;
#endif
	}

	// redraw dirty tiles
#ifdef CHAR_PROFILE
	t1 = SDL_GetTicks();
#endif // CHAR_PROFILE
	tile = &(char_tile_map[0]);
	for (y = 0; y < char_state.config.grid_height; y++)
	{
		for (x = 0; x < char_state.config.grid_width; x++)
		{
			if (tile->dirty)
			{
				ch = tile->ch;
				if ((ch < 32) || (ch >= 128))
				{
					// not supported
					ch = 127; // 'missing' character
				}

				// find the source position
				ch -= 32;
				tile_y = ch / char_gfx_state.char_ascii_surface.decl.grid_width;
				tile_x = ch - (tile_y * char_gfx_state.char_ascii_surface.decl.grid_width);
				src.x = tile_x * char_gfx_state.display_tile_w;
				src.y = tile_y * char_gfx_state.display_tile_h;
				//src.w = char_gfx_state.display_tile_w;
				//src.h = char_gfx_state.display_tile_h;

				// find the dest position
				dest.x = SCALE_X(x);
				dest.y = SCALE_Y(y);
				src.w = dest.w = SCALE_X(x + 1) - dest.x;
				src.h = dest.h = SCALE_Y(y + 1) - dest.y;
				dest.x += char_gfx_state.display_offs_x;
				dest.y += char_gfx_state.display_offs_y;

				// blit character
				ret = SDL_BlitSurface(char_gfx_state.char_ascii_surface.display, &src, char_state.video_surface, &dest);
				assert(ret == 0); // -2 means device lost; shouldn't happen in a windowed software app
			}

			tile++;
		}
	}
#ifdef CHAR_PROFILE
	t2 = SDL_GetTicks();
	char_debug_out(std::string("redraw took ") + char_int_str(t2 - t1) + std::string(" ms"));
#endif // CHAR_PROFILE

#ifdef CHAR_PROFILE
	t1 = SDL_GetTicks();
#endif // CHAR_PROFILE

	// lock surface
	if (SDL_LockSurface(char_state.video_surface) != 0)
	{
		// fail
		assert(false);
		return;
	}
	try
	{
		// restyle dirty tiles
		tile = &(char_tile_map[0]);
		for (y = 0; y < char_state.config.grid_height; y++)
		{
			for (x = 0; x < char_state.config.grid_width; x++)
			{
				if (tile->dirty)
				{
					// post-style
					if ((tile->style.fore_r != 255) ||
						(tile->style.fore_g != 255) ||
						(tile->style.fore_b != 255) ||
						(tile->style.back_r != 0) ||
						(tile->style.back_g != 0) ||
						(tile->style.back_b != 0))
					{
						// find the dest position
						dest.x = SCALE_X(x);
						dest.y = SCALE_Y(y);
						dest.w = SCALE_X(x + 1) - dest.x;
						dest.h = SCALE_Y(y + 1) - dest.y;
						dest.x += char_gfx_state.display_offs_x;
						dest.y += char_gfx_state.display_offs_y;

						char_style_rect(dest.x, dest.y, dest.w, dest.h, tile->style);
					}

					tile->dirty = false;
				}

				tile++;
			}
		}

		// unlock surface
		SDL_UnlockSurface(char_state.video_surface);
	} catch (...) {
		SDL_UnlockSurface(char_state.video_surface);
		throw;
	}

#ifdef CHAR_PROFILE
	t2 = SDL_GetTicks();
	char_debug_out(std::string("restyle took ") + char_int_str(t2 - t1) + std::string(" ms"));
#endif // CHAR_PROFILE
}

void char_clear()
{
	char_tile_state *tile;
	int x, y;

	tile = &(char_tile_map[0]);
	for (y = 0; y < char_state.config.grid_height; y++)
	{
		for (x = 0; x < char_state.config.grid_width; x++)
		{
			tile->ch = ' ';
			tile->style = char_default_style;
			tile->dirty = true;
			tile++;
		}
	}
}

void char_draw(int x, int y, char ch, const char_style &style)
{
	char_tile_state *tile;

	if ((x >= 0) && (x < char_state.config.grid_width) &&
		(y >= 0) && (y < char_state.config.grid_height))
	{
		tile = &(char_tile_map[(y * char_state.config.grid_width) + x]);
		if ((ch != tile->ch) ||
			(style.fore_r != tile->style.fore_r) ||
			(style.fore_g != tile->style.fore_g) ||
			(style.fore_b != tile->style.fore_b) ||
			(style.back_r != tile->style.back_r) ||
			(style.back_g != tile->style.back_g) ||
			(style.back_b != tile->style.back_b))
		{
			tile->ch = ch;
			tile->style = style;
			tile->dirty = true;
		}
	}
}

void char_draw(int x, int y, const std::string &str, const char_style &style)
{
	unsigned int i;

	for (i = 0; i < str.length(); i++)
	{
		char_draw(x + i, y, str[i], style);
	}
}

// --- input ---

struct char_input
{
	char_key key;
	bool key_full;
};
char_input char_input_state;

static void char_input_init()
{
	SDL_EnableUNICODE(1);
	char_input_state.key_full = false;
}

static void char_input_shutdown()
{
}



// --- update ---

static int char_event_filter(const SDL_Event *event)
{
	switch (event->type)
	{
	case SDL_VIDEORESIZE:
		{
			// reset device
			char_gfx_reset(event->resize.w, event->resize.h);
			char_gfx_state.post_reset = true;
		} break;

	case SDL_KEYDOWN:
		{
			// record the key press (overwrite any existing record)
			if ((event->key.keysym.unicode & 0x7F) == event->key.keysym.unicode)
			{
				// ascii
				char_input_state.key.ascii = event->key.keysym.unicode;
			} else {
				char_input_state.key.ascii = 0;
			}
			char_input_state.key.sdl = event->key.keysym.sym;
			char_input_state.key_full = true;
		} break;

	case SDL_QUIT:
		{
			char_state.has_quit = true;
		} break;

	default:
		{
			// ignore other events
		} break;
	}

    return 1; // add the event to the queue
}

//Set char_key. Return false when quit or error.
bool char_get_key(char_key &p_key)
{
	SDL_Event event;
	int status;
	char *key;

	//some sane, but not used default values
	p_key.ascii='\0';
	p_key.sdl=SDLK_F10;

	// graphics
	char_gfx_update();
	SDL_Flip(char_state.video_surface);
	
	//wait indefinitely for an event to occur
	status = SDL_WaitEvent(&event);

	//event will be removed from event queue
	if ( !status ) //Error has occurred while waiting
		{
		printf("SDL_WaitEvent error: %s\n", SDL_GetError());
		return false;
		}
		switch (event.type) //check the event type
			{
			case SDL_KEYDOWN:
				key = SDL_GetKeyName(event.key.keysym.sym);
				//printf("The %s key was pressed!\n", key );
				if (char_input_state.key_full)
					{
					p_key = char_input_state.key;
					char_input_state.key_full = false;
					return true;
					} else {
					return false;
					}
			break;

			case SDL_QUIT:      //'x' of Window clicked
				return false;
			break;
		}
  
  return true;
}

// --- init / shutdown ---

void char_init(const char_config &config)
{
#ifndef MAEMO
	SDL_Surface *surface_for_icon;
#endif
	u32 flags;

	char_state.config = config;
	char_state.has_quit = false;

	// --- init platform ---

#ifdef WIN32
	// Windows/VC6.0 doesn't throw on new failure by default.
	_set_new_handler(main_new_handler);
#endif // WIN32

	// --- init SDL ---

	// allow screensaver
	putenv((char *)"SDL_VIDEO_ALLOW_SCREENSAVER=1");
		// (ignore error)

	flags = SDL_INIT_VIDEO;
#ifdef _DEBUG
	flags |= SDL_INIT_NOPARACHUTE; // better for debugging
#endif // _DEBUG
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER) < 0)
	{
		char_do_error("SDL_Init failed", true);
	}
	
	/* Ignore some events */
	SDL_EventState(SDL_MOUSEMOTION, SDL_IGNORE);
	SDL_EventState(SDL_MOUSEBUTTONDOWN, SDL_IGNORE);
	SDL_EventState(SDL_MOUSEBUTTONUP , SDL_IGNORE);
	SDL_EventState(SDL_KEYUP, SDL_IGNORE);  //yes, we need only SDL_KEYDOWN

	SDL_SetEventFilter(&char_event_filter);

	// --- init app ---

	// caption
	SDL_WM_SetCaption(char_state.config.window_title.c_str(), char_state.config.window_title.c_str());
#ifndef MAEMO
	// load icon
	surface_for_icon = SDL_LoadBMP(char_state.config.icon_file_name.c_str());
	if (surface_for_icon != NULL)
	{
		SDL_SetColorKey(surface_for_icon, SDL_SRCCOLORKEY, SDL_MapRGB(surface_for_icon->format, 0xFF, 0x00, 0xFF));
		SDL_WM_SetIcon(surface_for_icon, NULL);
		SDL_FreeSurface(surface_for_icon);
	} else {
		// non-fatal
	}
#endif

	char_gfx_init();
	char_input_init();
}

void char_shutdown()
{
	char_input_shutdown();
	char_gfx_shutdown();
	SDL_Quit();
}