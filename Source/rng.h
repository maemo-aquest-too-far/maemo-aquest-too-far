// rng.h - 'A Quest Too Far'
//
// Copyright 2010 Geoffrey White
//
// see readme.txt for more details.

#ifndef INCLUDED_RNG
#define INCLUDED_RNG

	// --- random numbers ---

	int random(int less_than);
	inline int random(int min, int max)
	{
		return (random(max + 1 - min) + min);
	}

#endif // INCLUDED_RNG