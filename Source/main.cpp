// main.cpp - 'A Quest Too Far'
//
// Copyright (C) 2010 Geoffrey White
//
// see readme.txt for more details.
//
// 'A Quest Too Far' is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be enjoyed,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "defines.h"
#include "game.h"
#include "charlib.h"
#include "debug.h"

static bool new_round();

int main(int argc, char *argv[])
{
	char_config config;

	// prepare CharLib config
	config.window_title = "A Quest Too Far";
	config.icon_file_name = "Data/icon32.bmp";
	config.ascii_decl.file_name = "tiles_ascii.png"; // ascii characters 32 .. 127
	config.ascii_decl.grid_width = 8;
	config.ascii_decl.grid_height = 12;
	config.grid_width = GRID_WIDTH;
	config.grid_height = GRID_HEIGHT;
	config.aspect_width = 6;
	config.aspect_height = 5;

	bool another_round=true;
	while(another_round) 
	{
		// --- init ---
		char_init(config);

#ifndef _DEBUG
		try
		{
#endif // _DEBUG

		// --- New 'round' ---
		another_round=new_round(); //return false, when game should be closed

#ifndef _DEBUG
		} catch (const std::exception &e) {
		// whoops, an exception occurred!
		// lets display it to the user clearly (unless we're in a debug build)
		char_display_error(std::string("An error has occurred:\n") +
						   std::string(e.what()) +
						   std::string("\n\n'") +
						   config.window_title +
						   std::string("' is shutting down."));
	}
#endif // _DEBUG

		// --- shutdown ---
		char_shutdown();
	}//while

	return 0;
}

static bool new_round() {
	game the_game;
	char_key key;
	key.ascii='\0'; //this is not used, but safer if it something
	the_game.update(false, key);

	// handle input
	while (char_get_key(key)) //will return false when quitted (or errors encountered)
	{
#ifdef DEBUG_PROFILE
		int t1, t2;
		t1 = SDL_GetTicks();
#endif // DEBUG_PROFILE

		if (the_game.update(true, key)) //return true, when game ends.
		{
			// draw game over notice
			std::string str = "GAME OVER, press 'Y' to play again";
			while (str.size() < GRID_WIDTH) {str += ' ';}
			char_draw(0, 0, str, mk_char_style(255, 255, 192, 128, 0, 0));

#ifdef DEBUG_PROFILE
			t2 = SDL_GetTicks();
			char_debug_out("whole update took " + char_int_str(t2 - t1) + "ms");
#endif // DEBUG_PROFILE

			//Wait for key-press
			char_get_key(key);
			if (toupper(key.ascii) == 'Y')
				return true; //Means, start next round
			break; //break while-loop with any other keypress
		}

	}
	return false; //Close application
}
