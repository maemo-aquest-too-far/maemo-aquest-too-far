// item.cpp - 'A Quest Too Far'
//
// Copyright 2010 Geoffrey White
//
// see readme.txt for more details.

#include "item.h"
#include "defines.h"
#include "game.h"
#include "creature.h"
#include "powers.h"
#include "message.h"
#include "describe.h"
#include "inventory.h"
#include "charlib.h"
#include <assert.h>

// --- item ---

item :: item(game &_game) :
	short_name("???"), full_name("???"), inventory_ext(""), display_ch('?'), weight(1), my_game(_game)
{
	// add to game
	my_game.add_item(this);
}

item :: ~item()
{
	// remove from game
	my_game.remove_item(this);
}

void item :: draw(int x, int y)
{
	char_draw(x, y, display_ch, ITEM_DEFAULT_STYLE);
}

// defaults

bool item :: can_equip(equip_slot slot)
{
	return false;
}

void item :: carry_update(creature *_creature)
{
}

bool item :: can_activate(bool &out_is_exhausted)
{
	out_is_exhausted = false;
	return false;
}

bool item :: is_directional()
{
	return false;
}

std::string item :: get_direction_query()
{
	return "Which direction?";
}

void item :: activate(creature *_creature, int dx, int dy)
{
}

void item :: dropped()
{
}

int item :: get_min_damage() {
	return 0;
}
int item :: get_max_damage() {
	return 0;
}


// --- weapon ---
struct weapon_info
{
	const char *name;
	int damage_min, damage_max;
	int weight;
};
weapon_info weapon_infos[WT_NUM] =
{{"dagger",		1,	6,		1},	// WT_DAGGER
 {"spear",		1,	8,		2},	// WT_SPEAR
 {"mace",		1,	8,		2},	// WT_MACE
 {"hammer",		1,	8,		2},	// WT_HAMMER
 {"sword",		1,	10,		2},	// WT_SWORD
 {"axe",		1,	10,		2},	// WT_AXE
};

weapon :: weapon(game &_game, weapon_type _type, int _plus) :
	item(_game), type(_type), plus(_plus)
{
	assert(type >= 0);
	assert(type < WT_NUM);
	assert(plus >= 0); // -ve modifiers not currently supported

	display_ch = '/';
	weight = weapon_infos[type].weight;
	short_name = std::string("+") + char_int_str(plus) + std::string(" ") + weapon_infos[type].name;
	full_name = short_name +
		std::string(" [") + char_int_str(weapon_infos[type].damage_min + plus) + std::string("-") +
							char_int_str(weapon_infos[type].damage_max + plus) + std::string("]");
}

weapon :: ~weapon()
{
}

int weapon :: get_min_damage() {
	return weapon_infos[type].damage_min + plus;
}
int weapon :: get_max_damage() {
	return weapon_infos[type].damage_max + plus;
}

bool weapon :: can_equip(equip_slot slot)
{
	if (slot == ES_WEAPON)
	{
		return true;
	} else {
		return false;
	}
}

bool weapon :: can_activate(bool &out_is_exhausted)
{
	out_is_exhausted = true;
	return true;
}

void weapon :: carry_update(creature *_creature)
{
	if (_creature->equipped[ES_WEAPON] == this)
	{
		_creature->damage_min += weapon_infos[type].damage_min + plus;
		_creature->damage_max += weapon_infos[type].damage_max + plus;
		_creature->damage_unarmed = false;
	}
}

std::string weapon :: get_direction_query()
{
	return "Throw in which direction?";
}

bool weapon :: is_directional()
{
	if (this->type==WT_DAGGER)
		return true;
	else
		return false;
}

void weapon :: activate(creature *_creature, int dx, int dy) {
	powers_throw_item(_creature, dx, dy, this);
}

// --- stick ---

struct stick_info
{
	const char *name;
	bool is_staff;
	int damage_min, damage_max;
	int weight;
};
stick_info stick_infos[WT_NUM] =
{{"staff of disintegration",	false,		1,	6,		2},		// ST_DISINTEGRATION
 {"wand of displacement",		false,		0,	0,		1},		// ST_DISPLACEMENT
 {"wand of fire",				false,		0,	0,		1},		// ST_FIRE
};

stick :: stick(game &_game, stick_type _type, int _plus, int _charges) :
	item(_game), type(_type), plus(_plus), charges(_charges)
{
	assert(type >= 0);
	assert(type < ST_NUM);
	assert(plus >= 0); // -ve modifiers not currently supported
	assert(charges >= 0);

	display_ch = '/';
	weight = stick_infos[type].weight;
	update_name();
}

stick :: ~stick()
{
}

void stick :: update_name()
{
	short_name = stick_infos[type].name;
	if (stick_infos[type].is_staff)
	{
		short_name = std::string("+") + char_int_str(plus) + std::string(" ") + short_name;
		full_name = short_name +
			std::string(" [") + char_int_str(weapon_infos[type].damage_min + plus) + std::string("-") +
								char_int_str(weapon_infos[type].damage_max + plus) + std::string("]");
		// (don't display damage range, it's confusing, and often there isn't space)
	} else {
		full_name = short_name + std::string(" ");
	}

	full_name = full_name + std::string("{") + char_int_str(charges) + std::string(" charges}");
}

bool stick :: can_equip(equip_slot slot)
{
	return false;
	/*
	if (slot == ES_WEAPON)
	{
		return stick_infos[type].is_staff;
	} else {
		return false;
	}
	*/
}

void stick :: carry_update(creature *_creature)
{
	if (_creature->equipped[ES_WEAPON] == this)
	{
		_creature->damage_min += stick_infos[type].damage_min + plus;
		_creature->damage_max += stick_infos[type].damage_max + plus;
		_creature->damage_unarmed = false;
	}
}

bool stick :: can_activate(bool &out_is_exhausted)
{
	if (charges > 0)
	{
		out_is_exhausted = false;
		return true;
	} else {
		out_is_exhausted = true;
		return false;
	}
}

bool stick :: is_directional()
{
	return true;
}

std::string stick :: get_direction_query()
{
	switch (type)
	{
	case ST_DISINTEGRATION:
		{
			return "Zap disintegration in which direction?";
		} break;

	case ST_DISPLACEMENT:
		{
			return "Displace in which direction?";
		} break;

	case ST_FIRE:
		{
			return "Zap fire in which direction?";
		} break;

	default:
		{
			return item::get_direction_query();
			assert(false);
		} break;
	}
}

void stick :: activate(creature *_creature, int dx, int dy)
{
	assert(charges > 0);

	// use up a charge
	charges--;
	update_name();

	if (charges==0)
	{
		//drop empty stick automatically
		drop_item _action2 = drop_item(_creature, this);
		stack::push_action(_action2);

		//describe_candle(CA_EXPIRE) is good for this too
		display_message _action = display_message(describe_candle(_creature, this, CA_EXPIRE));
		stack::push_action(_action);
	}

	switch (type)
	{
	case ST_DISINTEGRATION:
		{
			zap_disintegration(_creature, dx, dy);
		} break;

	case ST_DISPLACEMENT:
		{
			zap_displacement(_creature, dx, dy);
		} break;

	case ST_FIRE:
		{
			zap_fire(_creature, dx, dy);
		} break;

	default:
		{
			assert(false);
		} break;
	}

}

// --- ring ---

struct ring_info
{
	const char *name, *ext;
};
ring_info ring_infos[WT_NUM] =
{{"ring of might",				" [+1-6]"},		// RT_MIGHT
 {"ring of monster detection",	""},			// RT_DETECTION
 {"ring of fire resistance",	""},			// RT_FRESIST
 {"ring of poison resistance",	""},			// RT_PRESIST
};

ring :: ring(game &_game, ring_type _type) : item(_game), type(_type)
{
	assert(type >= 0);
	assert(type < RT_NUM);

	display_ch = 'o';
	short_name = ring_infos[type].name;
	full_name = short_name + ring_infos[type].ext;
}

ring :: ~ring()
{
}

bool ring :: can_equip(equip_slot slot)
{
	if ((slot == ES_LEFTRING) || (slot == ES_RIGHTRING))
	{
		return true;
	}

	return false;
}

void ring :: carry_update(creature *_creature)
{
	if ((_creature->equipped[ES_LEFTRING] == this) ||
		(_creature->equipped[ES_RIGHTRING] == this))
	{
		switch (type)
		{
		case RT_MIGHT:
			{
				_creature->damage_min += 1;
				_creature->damage_max += 6;
			} break;

		case RT_DETECTION:
			{
				_creature->has_detection = true;
			} break;

		case RT_FRESIST:
			{
				_creature->resists_fire = true;
			} break;

		case RT_PRESIST:
			{
				_creature->resists_poison = true;
			} break;

		default:
			{
				assert(false);
			} break;
		}
	}
}

// --- candle ---

candle :: candle(game &_game, int _turns_left) : item(_game), is_lit(false), turns_left(_turns_left)
{
	display_ch = '!';
	update_name();
}

candle :: ~candle()
{
}

void candle :: update_name()
{
	short_name = "candle of invisibility";
	full_name = short_name + " {" + char_int_str(turns_left) + " turns}";
}

void candle :: carry_update(creature *_creature)
{
	if (is_lit)
	{
		if (turns_left == 0)
		{
			// run out?
			if ((_creature->is_visible()) || (_creature->is_hero()))
			{
				//drop it automatically
				drop_item _action2 = drop_item(_creature, this);
				stack::push_action(_action2);
				_creature->carrying -= 1; //we can't call _creature->update bacause of circularity

				display_message _action = display_message(describe_candle(_creature, this, CA_EXPIRE));
				stack::push_action(_action);
			}
			is_lit = false;
			inventory_ext = "";
		} else {
			// provide invisibility
			_creature->has_invisibility = true;

			// consume a turn
			turns_left--;
			update_name();
		}
	}
}

bool candle :: can_activate(bool &out_is_exhausted)
{
	return ((turns_left > 0) || (is_lit));
}

void candle :: activate(creature *_creature, int dx, int dy)
{	
	if (is_lit)
	{
		// put out candle
		if ((_creature->is_visible()) || (_creature->is_hero()))
		{
			display_message _action = display_message(describe_candle(_creature, this, CA_UNLIGHT));
			stack::push_action(_action);
		}
		is_lit = false;
		inventory_ext = "";
	} else {
		assert(turns_left > 0);

		// light candle
		if ((_creature->is_visible()) || (_creature->is_hero()))
		{
			display_message _action = display_message(describe_candle(_creature, this, CA_LIGHT));
			stack::push_action(_action);
		}
		is_lit = true;
		inventory_ext = "[lit]";
	}
}

void candle :: dropped()
{
	// go out	
	is_lit = false;
	inventory_ext = "";
}

// --- consumable ---

struct consumable_info
{
	const char *name;
	bool is_potion;
};
consumable_info consumable_infos[WT_NUM] =
{{"potion of extra healing",	true},		// CT_EXTRAHEALING
 {"scroll of teleportation",	false},		// CT_TELEPORT
 {"scroll of magic mapping",	false},		// CT_MAPPING
};

consumable :: consumable(game &_game, consumable_type _type) : item(_game), type(_type)
{
	assert(type >= 0);
	assert(type < SPT_NUM);

	if (consumable_infos[type].is_potion)
	{
		display_ch = '!'; // potion
	} else {
		display_ch = '~'; // scroll
	}
	full_name = short_name = consumable_infos[type].name;
}

consumable :: ~consumable()
{
}

bool consumable :: can_activate(bool &out_is_exhausted)
{
	return true;
}

void consumable :: activate(creature *_creature, int dx, int dy)
{
	map *_map;
	int x, y;

	switch (type)
	{
	case SPT_EXTRAHEALING:
		{
			if ((_creature->poisoned_turns > 0) && (_creature->is_hero()))
			{
				display_message _action = display_message(describe_poison_off(_creature));
				stack::push_action(_action);
			}
			if ((_creature->is_visible()) || (_creature->is_hero()))
			{
				display_message _action = display_message(describe_healed(_creature));
				stack::push_action(_action);
			}
			_creature->hp = MIN(_creature->hp + 40, _creature->hp_max); // heal 40 hit points
			_creature->decline = MAX(_creature->decline - 40, 0); // also heal 40 decline
			_creature->decline--; // HACK: to counter the inevitable +1 increment
			_creature->poisoned_turns = 0; // cure all poisons
		} break;

	case SPT_TELEPORT:
		{
			do_teleport(_creature);
		} break;

	case SPT_MAPPING:
		{
			_map = _creature->get_map();
			assert(_map != NULL);

			if (_creature->is_hero()) // (every other creature knows the whole map anyway)
			{
				display_message _action = display_message("The scroll has a map of this level on it.");
				stack::push_action(_action);

				for (y = 0; y < MAP_HEIGHT; y++)
				{
					for (x = 0; x < MAP_WIDTH; x++)
					{
						if ((_map->get_tile(x, y)->is_solid) ||
							(_map->get_tile(x, y)->is_stairs))
						{
							// reveal the tile
							if (_map->get_tile(x, y)->vis == V_HIDDEN)
							{
								_map->get_tile(x, y)->vis = V_EXPLORED;
							}
						}
					}
				}
			}
		} break;

	default:
		{
			assert(false);
		} break;
	}

	// consume it
	{
		consume_item _action = consume_item(_creature, this);
		stack::push_action(_action);
	}
}