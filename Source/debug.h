// debug.h - 'A Quest Too Far'
//
// Copyright 2010 Geoffrey White
//
// see readme.txt for more details.

#ifndef INCLUDED_DEBUG
#define INCLUDED_DEBUG

	#ifdef _DEBUG

		// --- debugging defines ----

		//#define DEBUG_SEE_EVERYTHING
		// - always see map and monsters

		//#define DEBUG_EASY_DESCENT
		// - allow descent from any tile, not just stairs

		//#define DEBUG_PROFILE
		// - extra debug output about CPU

		//#define DEBUG_LEVELGEN
		// - extra debug output about level generation

	#endif

#endif