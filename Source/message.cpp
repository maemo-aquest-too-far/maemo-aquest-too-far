// message.cpp - 'A Quest Too Far'
//
// Copyright 2010 Geoffrey White
//
// see readme.txt for more details.

#include "defines.h"
#include "message.h"
#include <assert.h>

// --- draw message ---

struct char_pair
{
	char ch;
	char_style style;
};

void format_part(const std::vector<char_pair> &source_text, std::vector<char_pair> &dest_text, int max_width,
				 const char_style &default_style, int pos_begin, int pos_end)
{
	char_pair space_char;
	int i, pos;

	space_char.ch = ' ';
	space_char.style = default_style;

	for (i = 0; i < max_width; i++)
	{
		pos = pos_begin + i;
		dest_text.push_back((pos <= pos_end) ? (source_text[pos]) : (space_char));
	}
}

int format_message(const std::string &text, const char_style &style, int max_width, bool more, int at_y, bool and_draw)
{
	std::vector<char_pair> intermediate_text, formatted_text;
	int lines = 0;

	assert(max_width > 0);

	// generate intermediate text
	{
		std::string::const_iterator it;
		std::string more_text = "SPACE";
		char_pair ch;

		// add message
		ch.style = style;
		for (it = text.begin(); it != text.end(); it++)
		{
			ch.ch = *it;
			intermediate_text.push_back(ch);
		}

		// add more indicator
		if (more)
		{
			ch.ch = ' ';
			intermediate_text.push_back(ch);

			ch.style = MESSAGE_HIGHLIGHT_STLYE;
			for (it = more_text.begin(); it != more_text.end(); it++)
			{
				ch.ch = *it;
				intermediate_text.push_back(ch);
			}
		}
	}

	// generate formatted text
	{
		int pos, line_start, white_start, white_end;
		char c;

		pos = 0;
		line_start = 0;
		white_start = -1;
		white_end = -1;
		formatted_text.clear();
		while (pos < (int)intermediate_text.size())
		{
			c = intermediate_text[pos].ch;
			if (c == '\n')
			{
				if (white_end == pos - 1)
				{
					// end line before the chunk of whitespace
					format_part(intermediate_text, formatted_text, max_width, style, line_start, white_start - 1);
				} else {
					// end line right here
					format_part(intermediate_text, formatted_text, max_width, style, line_start, pos - 1);
				}
				// start a new line after the linebreak
				line_start = pos = pos + 1;
			} else if (c == ' ') {
				if (white_end == pos - 1)
				{
					// extend whitespace
					white_end = pos;
				} else {
					// start whitespace
					white_start = white_end = pos;
				}
				pos++;
			} else {
				if (pos + 1 - line_start <= max_width)
				{
					// character fits
					pos++;
				} else  {
					// character doesn't fit on the current line
					if (white_start > line_start)
					{
						// there was a whitespace block, end line before it
						format_part(intermediate_text, formatted_text, max_width, style, line_start, white_start - 1);

						// start a new line after that whitespace
						pos = line_start = white_end + 1;
					} else {
						// no whitespace, end line right before this char
						format_part(intermediate_text, formatted_text, max_width, style, line_start, pos - 1);

						// start a new line with this char
						line_start = pos;
					}
				}
			}
		}

		// display anything we have left
		format_part(intermediate_text, formatted_text, max_width, style, line_start, pos - 1);
	}

	// display formatted text
	{
		int i, x, y;

		x = 0;
		y = at_y;
		for (i = 0; i < (int)formatted_text.size(); i++)
		{
			if (and_draw)
			{
				char_draw(x, y, formatted_text[i].ch, formatted_text[i].style);
			}
			x++;
			if (x >= max_width)
			{
				x = 0;
				y++;
				lines++;
			}
		}
	}

	return lines;
}

// --- message_history ---

std::list<std::string> recent_messages;

// --- message state ---

static bool is_cur_message = false;
static std::string cur_message_text;
static bool cur_message_more;

// --- wait_message ---

class wait_message : public action
{
public:
	wait_message() {needs_key = true;}
	virtual ~wait_message() {}
	virtual action *clone() {return new wait_message();}

	virtual void carry_out(const char_key &key);
};

void wait_message :: carry_out(const char_key &key)
{
	if (key.ascii == ' ')
	{
		// done with message
		is_cur_message = false;
	} else {
		// keep waiting
		stack::push_action(*this);
	}
}

// --- display_message ---

void display_message :: carry_out(const char_key &key)
{
	// is a message already up?
	if (is_cur_message)
	{
		// add 'more' and continue after waiting for the current message
		cur_message_more = true;
		stack::push_action(*this);
		{
			wait_message _action = wait_message();
			stack::push_action(_action);
		}
		return;
	}

	// set our message
	is_cur_message = true;
	cur_message_text = text;
	cur_message_more = false;

	// update recent_messages
	recent_messages.push_back(text);
	if (recent_messages.size() > GRID_HEIGHT - 1)
	{
		recent_messages.pop_front();
	}
}

bool display_message :: draw_current()
{
	if (is_cur_message)
	{
		// display the current message
		format_message(cur_message_text, MESSAGE_DEFAULT_STYLE, GRID_WIDTH, cur_message_more, 0, true);

		return true;
	} else {
		return false;
	}
}

void display_message :: dismiss_current()
{
	if (is_cur_message)
	{
		// auto-dismiss
		is_cur_message = false;
	}
}

// --- wait_if_message ---

void wait_if_message :: carry_out(const char_key &key)
{
	if (is_cur_message)
	{
		if ((including_one_line) ||
			(format_message(cur_message_text, MESSAGE_DEFAULT_STYLE, GRID_WIDTH, false, 0, false) > 1))
		{
			// wait for this message
			cur_message_more = true;
			{
				wait_message _action = wait_message();
				stack::push_action(_action);
			}
		}
	}
}
