// game.cpp - 'A Quest Too Far'
//
// Copyright 2010 Geoffrey White
//
// see readme.txt for more details.

#include "defines.h"
#include "game.h"
#include "item.h"
#include "statusbar.h"
#include "stack.h"
#include "vis.h"
#include "inventory.h"
#include "describe.h"
#include "message.h"
#include "util.h"
#include "screens.h"
#include "rng.h"
#include "debug.h"
#include <assert.h>

// --- game_remove_dead ---

class game_remove_dead : public action
{
public:
	game_remove_dead(std::list<creature *> &_creatures) : creatures(_creatures) {}
	virtual ~game_remove_dead() {}
	virtual action *clone() {return new game_remove_dead(creatures);}

	virtual void carry_out(const char_key &key);

private:
	std::list<creature *> &creatures;
};

void game_remove_dead :: carry_out(const char_key &key)
{
	std::list<creature *>::iterator it, it_here;

	it = creatures.begin();
	while (it != creatures.end())
	{
		it_here = it;
		it++;

		if ((*it_here)->hp <= 0)
		{
			delete (*it_here);
		}
	}
}

// --- game_move_hero ---

class game_move_hero : public action
{
public:
	game_move_hero(game &_game, creature *_hero);
	virtual ~game_move_hero() {}
	virtual action *clone() {return new game_move_hero(my_game, hero);}

	virtual void carry_out(const char_key &key);

private:
	game &my_game;
	creature *hero;
};

game_move_hero :: game_move_hero(game &_game, creature *_hero) : my_game(_game), hero(_hero)
{
	needs_key = true;
}

SDLKey game_ignore_keys[] =
{SDLK_PAUSE, SDLK_BREAK, SDLK_PRINT, SDLK_SYSREQ,
 SDLK_NUMLOCK, SDLK_CAPSLOCK, SDLK_SCROLLOCK,
 SDLK_LSHIFT, SDLK_RSHIFT, SDLK_LCTRL, SDLK_RCTRL, SDLK_LALT, SDLK_RALT,
 SDLK_LMETA, SDLK_RMETA, SDLK_LSUPER, SDLK_RSUPER, SDLK_MENU};

void game_move_hero :: carry_out(const char_key &key)
{
	int dx, dy, new_x, new_y;
	map *new_map;
	map_tile *tile;
	int i;

	// ignore some buttons entirely
	for (i = 0; i < (int)(sizeof(game_ignore_keys) / sizeof(game_ignore_keys[0])); i++)
	{
		if (key.sdl == game_ignore_keys[i])
		{
			// keep waiting
			stack::push_action(*this);
			return;
		}
	}

	// auto-dismiss any message that's still up
	display_message::dismiss_current();

	if (hero != NULL)
	{
		// check for movement keys
		if (translate_direction_key(key, dx, dy))
		{
			new_map = hero->get_map();
			new_x = hero->get_x() + dx;
			new_y = hero->get_y() + dy;
			tile = map::get_tile(new_map, new_x, new_y);
			if (tile != NULL)
			{
				// attack?
				if (tile->my_creature != NULL)
				{
					hero->attack(tile->my_creature);
					return;
				}

				// move?
				if (hero->can_move(tile))
				{
					if (hero->carrying <= hero->get_carry_max())
					{
						hero->move_to(new_map, new_x, new_y);
						return;
					} else {
						stack::push_action(*this);
						{
							display_message _action = display_message("You are carrying too much to move. ('f' to drop)");
							stack::push_action(_action);
						}
						return;
					}
				}
			}
		}

		// rest?
		if ((key.ascii == '.') || (key.sdl == SDLK_KP5) || (key.sdl == SDLK_5))
		{
			return;
		}

		// view inventory / use item / wield item?
		if ((toupper(key.ascii) == 'I') )
		{
			// use interface (cancellable)
			display_inventory _action = display_inventory(hero, IM_INVENTORY, *this);
			stack::push_action(_action);
			return;
		}

		// drop?
		if (toupper(key.ascii) == 'F')
		{
			// drop interface (cancellable)
			display_inventory _action = display_inventory(hero, IM_DROP, *this);
			stack::push_action(_action);
			return;
		}

		// pick up?
		if ( (toupper(key.ascii) == 'P') || (toupper(key.ascii) == ',') || (toupper(key.ascii) == 'G'))
		{
			tile = hero->get_map_tile();
			if (!tile->my_items.empty())
			{
				if (tile->my_items.size() == 1)
				{
					// pick up the item
					pickup_item _action = pickup_item(hero, &(*(tile->my_items.front())));
					stack::push_action(_action);
					return;
				} else {
					// pickup interface (cancellable)
					display_inventory _action = display_inventory(hero, IM_PICKUP, *this);
					stack::push_action(_action);
					return;
				}
			} else {
				stack::push_action(*this);
				{
					display_message _action = display_message("There is nothing here to pick up.");
					stack::push_action(_action);
				}
				return;
			}
		}

		// descend stairs?
		if ( (toupper(key.ascii) == 's') ||  (key.ascii == '>') || (key.sdl == SDLK_RETURN) || (key.sdl == SDLK_KP_ENTER))
		{
			tile = hero->get_map_tile();
#ifdef DEBUG_EASY_DESCENT
			if (true)
#else
			if (tile->is_stairs)
#endif
			{
				if (hero->carrying > hero->get_carry_max())
				{
					stack::push_action(*this);
					{
						display_message _action = display_message("You are carrying too much to move. ('f' to drop)");
						stack::push_action(_action);
					}
					return;
				}

				my_game.level++;
				if (my_game.level < LT_NUM)
				{
					// start the next level
					go_new_level _action = go_new_level(my_game);
					stack::push_action(_action);
					return;
				} else {
					// you win!
					game_won _action = game_won(my_game);
					stack::push_action(_action);
					{
						wait_if_message _action = wait_if_message(true);
						stack::push_action(_action);
					}
					{
						//   ########################################################
						display_message action1 = display_message(
							"You descend into a small chamber.  There you find your "
							"grandchildren, Benjamin and Jessica, shaken but little "
							"worse for wear.  You struggle to untie them, and then "
							"you collapse.");
						display_message action2 = display_message(
							"'My body is broken and I can't go on.  Take these things "
							"and return home without me.  Remember everything I "
							"taught you!'");
						stack::push_action(action2);
						stack::push_action(action1);
					}
					return;
				}
			} else {
				stack::push_action(*this);
				{
					display_message _action = display_message("The are no stairs here.");
					stack::push_action(_action);
				}
				return;
			}
		}

		// message history?
		if ((toupper(key.ascii) == 'M') || (key.sdl == SDLK_F2))
		{
			stack::push_action(*this);
			{
				history_screen _action = history_screen();
				stack::push_action(_action);
			}
			return;
		}

		// help?
		if ((key.ascii == '?') || (key.sdl == SDLK_F1))
		{
			stack::push_action(*this);
			{
				help_screen _action = help_screen();
				stack::push_action(_action);
			}
			return;
		}

		// Quitting?
		if (key.ascii == 'v')
		{
			stack::push_action(*this);
			{
				suicide_dialog _action = suicide_dialog(hero);
				stack::push_action(_action);
			}
			return;
		}
	}

	// keep waiting
	stack::push_action(*this);
}

// --- game_move_monsters ---

class game_move_monsters : public action
{
public:
	game_move_monsters(std::list<creature *> &_all_creatures) : all_creatures(_all_creatures) {}
	virtual ~game_move_monsters() {}
	virtual action *clone() {return new game_move_monsters(all_creatures);}

	virtual void carry_out(const char_key &key);

private:
	std::list<creature *> &all_creatures;
};

void game_move_monsters :: carry_out(const char_key &key)
{
	std::list<creature *>::iterator it;

	// find the first unprocessed creature
	// note: this approach is O(N^2), but it's safe to add and remove creatures during the process,
	//		 which is important to us.
	it = all_creatures.begin();
	while ((it != all_creatures.end()) && ((*it)->had_turn))
	{
		it++;
	}
	if (it == all_creatures.end())
	{
		// we've done them all
		return;
	}

	// queue up an action to continue this process
	{
		game_move_monsters _action = game_move_monsters(all_creatures);
		stack::push_action(_action);
	}

	// queue up an action to remove dead
	{
		game_remove_dead _action = game_remove_dead(all_creatures);
		stack::push_action(_action);
	}

	// move and update this creature
	assert((*it)->hp > 0);
	{
		creature_update _action = creature_update(*it);
		stack::push_action(_action); // amongst other things sets 'had_turn'
	}
	(*it)->move();
}

// --- game ---

game :: game() : the_hero(NULL)
{
	bool level_taken[LT_NUM];
	int i, j, pick, num;

	try
	{
		// plan the levels
		for (i = 0; i < LT_NUM; i++)
		{
			level_taken[i] = false;
		}
		// fix first and last levels
		level_order[0] = LT_ENTRY;
		level_taken[0] = true;
		level_order[LT_NUM - 1] = LT_LAIR;
		level_taken[LT_NUM - 1] = true;
		// determine the rest at random
		for (i = 1; i < LT_NUM - 1; i++)
		{
			num = 0;
			pick = -1;
			for (j = 0; j < LT_NUM; j++)
			{
				if (!level_taken[j])
				{
					num++;
					if (random(num) == 0)
					{
						pick = j;
					}
				}
			}
			assert(pick != -1);
			level_order[i] = (level_type)pick;
			level_taken[pick] = true;
		}

		// nothing has been introduced yet
		for (i = 0; i < CT_NUM; i++)
		{
			creatures_introduced[i] = false;
		}
		stairs_introduced = false;

		// create the hero
		the_hero = new creature(*this, NULL, 0, 0, CT_MAN, true);

		// create the level
		level = 0;
		{
			go_new_level _action = go_new_level(*this);
			stack::push_action(_action);
		}

		// intro screen
		{
			intro_screen _action = intro_screen();
			stack::push_action(_action);
		}
	} catch (...) {
		cleanup();
		throw;
	}
}

game :: ~game()
{
	cleanup();
}

void game :: cleanup()
{
	the_map.clear_creatures();
	the_map.clear_items();
	if (the_hero != NULL)
	{
		delete the_hero;
		the_hero = NULL;
	}
	assert(all_creatures.empty());
	assert(all_items.empty());
}

//Return true, when game ends.
bool game :: update(bool key_ready, const char_key &key)
{
	std::list<creature *>::iterator it;

	while (true)
	{
		if (stack::get_num_actions() > 0)
		{
			// do an action from the stack
			if (stack::get_needs_key())
			{
				if (key_ready)
				{
					// use this key
					stack::carry_out(key);
					key_ready = false;
				} else {
					// redraw, wait for another key
					draw();
					return false;
				}
			} else {
				// no key required
				stack::carry_out(key);
			}
		} else {
			// stack is empty -
			if ((the_hero != NULL) && (the_hero->hp > 0))
			{
				// clear turn flags
				for (it = all_creatures.begin(); it != all_creatures.end(); it++)
				{
					(*it)->had_turn = false;
				}

				// push a turn
				{
					vis_update action1 = vis_update(*this, the_hero);
					wait_if_message action2 = wait_if_message(false);
					game_move_hero action3 = game_move_hero(*this, the_hero);
					vis_update action4 = vis_update(*this, the_hero);
					creature_update action5 = creature_update(the_hero);
					game_remove_dead action6 = game_remove_dead(all_creatures);
					game_move_monsters action7 = game_move_monsters(all_creatures); // (hero has already 'had_turn' by now)

					stack::push_action(action7);
					stack::push_action(action6);
					stack::push_action(action5);
					stack::push_action(action4);
					stack::push_action(action3);
					stack::push_action(action2);
					stack::push_action(action1);
				}
			} else {
				// game over

				// redraw
				draw();

				// wait for another key
				return  true;
			}
		}
	}
}

void game :: draw()
{
	std::list<creature *>::iterator it;

	// draw map
	the_map.draw();

	// draw creatures
	for (it = all_creatures.begin(); it != all_creatures.end(); it++)
	{
		if ((*it)->get_map() == (&the_map))
		{
			(*it)->draw();
		}
	}

	// draw fx over the top
	the_map.draw_fx();

	// draw status bar
	draw_status(GRID_HEIGHT - 1, this, the_hero);

	// draw message?  (messages stay up beyond when they're on the stack)
	display_message::draw_current();

	// draw whatever's top of the stack
	stack::draw();
}

void game :: add_creature(class creature *_creature)
{
	assert(_creature != NULL);
	all_creatures.push_back(_creature);
}

void game :: remove_creature(class creature *_creature)
{
	std::list<creature *>::iterator it;

	for (it = all_creatures.begin(); it != all_creatures.end(); it++)
	{
		if ((*it) == _creature)
		{
			all_creatures.erase(it);
			return;
		}
	}

	assert(false);
}

void game :: add_item(item *_item)
{
	assert(_item != NULL);
	all_items.push_back(_item);
}

void game :: remove_item(item *_item)
{
	std::list<item *>::iterator it;

	for (it = all_items.begin(); it != all_items.end(); it++)
	{
		if ((*it) == _item)
		{
			all_items.erase(it);
			return;
		}
	}

	assert(false);
}

// --- arrive_new_level ---

class arrive_new_level : public action
{
public:
	arrive_new_level(game &_game) : my_game(_game) {}
	virtual ~arrive_new_level() {}
	virtual action *clone() {return new arrive_new_level(my_game);}

	virtual void carry_out(const char_key &key);

private:
	game &my_game;
};

void arrive_new_level :: carry_out(const char_key &key)
{
	// describe the feeling of the level
	display_message _action = display_message(my_game.the_map.get_level_introduction());
	stack::push_action(_action);
}

// --- go_new_level ---

void go_new_level :: carry_out(const char_key &key)
{
	assert(my_game.level >= 0);
	assert(my_game.level < LT_NUM);

	my_game.the_map.gen_level(my_game, my_game.level_order[my_game.level], my_game.the_hero);
	{
		arrive_new_level _action = arrive_new_level(my_game);
		stack::push_action(_action);
	}
}

// --- game_won ---

game_won :: game_won(game &_game) : my_game(_game)
{
	needs_key = true;
}

void game_won :: carry_out(const char_key &key)
{
	// stay in this state forever
	stack::push_action(*this);
}

void game_won :: draw()
{
	// draw game won notice
	{
		std::string str = "THE END";
		while (str.size() < GRID_WIDTH) {str += ' ';}
		char_draw(0, 0, str, MESSAGE_GREEN_STLYE);
	}
}