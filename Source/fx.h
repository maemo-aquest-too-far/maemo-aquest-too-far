// fx.h - 'A Quest Too Far'
//
// Copyright 2010 Geoffrey White
//
// see readme.txt for more details.

#ifndef INCLUDED_FX
#define INCLUDED_FX

	#include "map.h"
	#include "stack.h"
	#include "charlib.h"

	// --- show_fx ---

	struct fx_tile
	{
		bool is_fx;
		char ch;
		char_style style;
	};

	class show_fx : public action
	{
	public:
		show_fx(map &_map, fx_tile *_fx);
		virtual ~show_fx() {}
		virtual action *clone() {return new show_fx(my_map, &(my_fx[0][0]));}

		virtual void carry_out(const char_key &key);

	private:
		map &my_map;
		fx_tile my_fx[MAP_WIDTH][MAP_HEIGHT];
	};

	// --- clear_fx ---

	class clear_fx : public action
	{
	public:
		clear_fx(map &_map) : my_map(_map) {}
		virtual ~clear_fx() {}
		virtual action *clone() {return new clear_fx(my_map);}

		virtual void carry_out(const char_key &key);

	private:
		map &my_map;
	};

#endif // INCLUDED_FX