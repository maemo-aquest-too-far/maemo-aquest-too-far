// message.h - 'A Quest Too Far'
//
// Copyright 2010 Geoffrey White
//
// see readme.txt for more details.

#ifndef INCLUDED_MESSAGE
#define INCLUDED_MESSAGE

	#include "stack.h"
	#include "charlib.h"
	#include <list>

	#define MESSAGE_DEFAULT_STYLE	(mk_char_style(255, 255, 192, 64, 64, 128))
	#define MESSAGE_HIGHLIGHT_STLYE	(mk_char_style(255, 255, 192, 128, 128, 255))
	#define MESSAGE_RED_STLYE		(mk_char_style(255, 255, 192, 128, 0, 0))
	#define MESSAGE_GREEN_STLYE		(mk_char_style(255, 255, 192, 0, 128, 0))

	// --- format_message ---

	int format_message(const std::string &text, const char_style &style, int max_width, bool more, int at_y, bool and_draw);
		// returns number of lines

	// --- display_message ---

	class display_message : public action
	{
	public:
		display_message(const std::string &_text) : text(_text) {}
		virtual ~display_message() {}
		virtual action *clone() {return new display_message(text);}

		virtual void carry_out(const char_key &key);

		static bool draw_current();
		static void dismiss_current();

	private:
		std::string text;
	};

	// --- wait_if_message ---
	//
	// waits for a message if there is one

	class wait_if_message : public action
	{
	public:
		wait_if_message(bool _including_one_line) : including_one_line(_including_one_line) {}
		virtual ~wait_if_message() {}
		virtual action *clone() {return new wait_if_message(including_one_line);}

		virtual void carry_out(const char_key &key);

	private:
		bool including_one_line;
	};

	// --- message_history ---

	extern std::list<std::string> recent_messages;

#endif // INCLUDED_MESSAGE