// powers.cpp - 'A Quest Too Far'
//
// Copyright 2010 Geoffrey White
//
// see readme.txt for more details.

#include "powers.h"
#include "defines.h"
#include "creature.h"
#include "map.h"
#include "fx.h"
#include "message.h"
#include "describe.h"
#include "game.h"
#include "item.h"
#include "rng.h"
#include "inventory.h"
#include <assert.h>

// --- defines ---

char ray_chars[3][3] =
{{	'\\',	'-',	'/'},
 {	'|',	'*',	'|'},
 {	'/',	'-',	'\\'}
};

// --- do_teleport ---

void do_teleport(creature *_creature)
{
	map_tile *tile;
	map *_map;
	int x, y;

	assert(_creature != NULL);
	_map = _creature->get_map();
	assert(_map != NULL);

	// find a position to teleport to
	while (true)
	{
		x = random(0, MAP_WIDTH - 1);
		y = random(0, MAP_HEIGHT - 1);
		tile = map::get_tile(_map, x, y);
		if ((_creature->can_move(tile)) ||
			((x == _creature->get_x()) && (y == _creature->get_y()))) // (so we can't get stuck)
		{
			// teleport!
			do_teleport_to(_creature, _map, x, y);
			return;
		}
	}
}

void do_teleport_to(creature *_creature, map *_map, int x, int y)
{
	bool visible;

	assert(_creature != NULL);
	assert(_map != NULL);

	visible = (_creature->is_visible()) || (_creature->is_hero());

	// teleport
	_creature->move_to(_map, x, y);

	visible |= (_creature->is_visible());

	// report
	if (visible)
	{
		display_message _action = display_message("Pop!");
		stack::push_action(_action);
	}
}

// --- zap_disintegration ---

void zap_disintegration(creature *_creature, int dx, int dy)
{
	fx_tile my_fx[MAP_WIDTH][MAP_HEIGHT];
	map_tile *tile;
	map *_map;
	item *_item;
	int x, y, dist;
	bool visible;

	assert(_creature != NULL);
	_map = _creature->get_map();
	assert(_map != NULL);

	// determine visibility
	visible = (_creature->is_visible()) || (_creature->is_hero());
	for (dist = 10; dist >= 1; dist--)
	{
		x = _creature->get_x() + (dist * dx);
		y = _creature->get_y() + (dist * dy);
		tile = map::get_tile(_map, x, y);
		if ((tile != NULL) && (tile->vis == V_VISIBLE))
		{
			visible = true;
		}
	}

	// post zap: wait for message, clear FX
	{
		clear_fx _action = clear_fx(*_map);
		stack::push_action(_action);
	}
	if (visible)
	{
		wait_if_message _action = wait_if_message(true);
		stack::push_action(_action);
	}

	// prep fx
	for (y = 0; y < MAP_HEIGHT; y++)
	{
		for (x = 0; x < MAP_WIDTH; x++)
		{
			my_fx[x][y].is_fx = false;
		}
	}

	// disintegrate tiles
	for (dist = 10; dist >= 1; dist--)
	{
		x = _creature->get_x() + (dist * dx);
		y = _creature->get_y() + (dist * dy);
		tile = map::get_tile(_map, x, y);
		if (tile != NULL)
		{
			if (tile->my_creature != NULL)
			{
				// disintegrate creature
				creature_damage _action = creature_damage(_creature, tile->my_creature, DT_DISINTEGRATE, 999);
				stack::push_action(_action);
			}
			while (!tile->my_items.empty())
			{
				// disintegrate item
				_item = tile->my_items.front();
				if (tile->vis == V_VISIBLE)
				{
					display_message _action = display_message("A " + _item->short_name + " is disintegrated!");
					stack::push_action(_action);
				}
				delete _item;
				tile->my_items.pop_front();
			}
			if (tile->is_solid)
			{
				// disintegrate wall
				tile->display_ch = ':';
				tile->is_solid = false;
			}

			// prep fx
			my_fx[x][y].is_fx = true;
			my_fx[x][y].ch = '*';
			my_fx[x][y].style = mk_char_style(255, 255, 0, 0, 0, 0);
		}
	}

	// show / describe fx
	if (visible)
	{
		{
			show_fx _action = show_fx(*_map, &(my_fx[0][0]));
			stack::push_action(_action);
		}
		{
			display_message _action = display_message("Zap!");
			stack::push_action(_action);
		}
	}
}

// --- zap_displacement ---

void zap_displacement(creature *_creature, int dx, int dy)
{
	map_tile *tile;
	map *_map;
	int x, y, xd, yd, dist;
	bool do_displace;

	assert(_creature != NULL);
	_map = _creature->get_map();
	assert(_map != NULL);

	// find a position to displace to
	do_displace = false;
	dist = 0;
	while (true)
	{
		dist++;

		x = _creature->get_x() + (dist * dx);
		y = _creature->get_y() + (dist * dy);
		tile = map::get_tile(_map, x, y);
		if ((tile == NULL) || (tile->is_solid))
		{
			// stop at walls
			break;
		}
		if (_creature->can_move(tile))
		{
			// we could move here
			xd = x;
			yd = y;
			do_displace = true;
		}
	}

	// displace
	if (do_displace)
	{
		do_teleport_to(_creature, _map, xd, yd);
	} else {
		if (_creature->is_hero())
		{
			display_message _action = display_message("Nothing happens.");
			stack::push_action(_action);
		}
	}
}

// --- zap_fire ---

char_style fire_colour(int dist)
{
	int r, g, b, dist2;

	// adjust distance
	dist2 = MAX(dist - 1, 0);

	// determine colour
	r = 320;
	g = 320 - (dist2 * 64);
	b = 192 - (dist2 * 128);

	// clamp and return
	r = MAX(MIN(r, 255), 0);
	g = MAX(MIN(g, 255), 0);
	b = MAX(MIN(b, 255), 0);
	return mk_char_style(r, g, b, 0, 0, 0);
}

void zap_fire(creature *_creature, int dx, int dy)
{
	fx_tile my_fx[MAP_WIDTH][MAP_HEIGHT];
	map_tile *tile;
	map *_map;
	int x, y, i, dist, damage_amount;
	bool visible;

	assert(_creature != NULL);
	_map = _creature->get_map();
	assert(_map != NULL);

	// find the target tile and determine visibility
	dist = 0;
	visible = (_creature->is_visible()) || (_creature->is_hero());
	while (true)
	{
		dist++;

		x = _creature->get_x() + (dist * dx);
		y = _creature->get_y() + (dist * dy);
		tile = map::get_tile(_map, x, y);
		if ((tile == NULL) || (tile->is_solid))
		{
			// stop before walls
			dist--;
			break;
		}
		if (tile->vis == V_VISIBLE)
		{
			visible = true;
		}
		if (tile->my_creature != NULL)
		{
			// stop at creatures
			break;
		}
	}

	// post zap: wait for message, clear FX
	{
		clear_fx _action = clear_fx(*_map);
		stack::push_action(_action);
	}
	if (visible)
	{
		wait_if_message _action = wait_if_message(true);
		stack::push_action(_action);
	}

	// prep fx
	for (y = 0; y < MAP_HEIGHT; y++)
	{
		for (x = 0; x < MAP_WIDTH; x++)
		{
			my_fx[x][y].is_fx = false;
		}
	}

	// do flame
	for (i = (dist == 0) ? (0) : (1); i <= dist; i++)
	{
		x = _creature->get_x() + (i * dx);
		y = _creature->get_y() + (i * dy);
		tile = map::get_tile(_map, x, y);
		if (tile != NULL)
		{
			if (tile->my_creature != NULL)
			{
				// check resistances
				if (tile->my_creature->resists_fire)
				{
					damage_amount = random(5, 10);
				} else {
					damage_amount = random(25, 30);
				}
 
				// burn creature
				{
					creature_damage _action = creature_damage(_creature, tile->my_creature, DT_FIRE, damage_amount);
					stack::push_action(_action);
				}

				// report
				if ((tile->vis == V_VISIBLE) || (tile->my_creature->is_hero()))
				{
					display_message _action = display_message(describe_burn(_creature, tile->my_creature, damage_amount));
					stack::push_action(_action);
				}
			}

			// prep fx
			my_fx[x][y].is_fx = true;
			if (i == dist)
			{
				my_fx[x][y].ch = '*';
			} else {
				my_fx[x][y].ch = ray_chars[dx + 1][dy + 1];
			}
			my_fx[x][y].style = fire_colour(i);
		}
	}

	// show / describe fx
	if (visible)
	{
		{
			show_fx _action = show_fx(*_map, &(my_fx[0][0]));
			stack::push_action(_action);
		}
		{
			display_message _action = display_message("Whoosh!");
			stack::push_action(_action);
		}
	}
}


char_style throw_colour(int dist)
{
	int r, g, b, dist2;

	// adjust distance
	dist2 = MAX(dist - 1, 0);

	// determine colour
	r = 320- (dist2 * 64);
	g = 320 ;
	b = 192 - (dist2 * 128);

	// clamp and return
	r = MAX(MIN(r, 255), 0);
	g = MAX(MIN(g, 255), 0);
	b = MAX(MIN(b, 255), 0);
	return mk_char_style(r, g, b, 0, 0, 0);
}

// --throw_item --
void powers_throw_item(creature *_creature, int dx, int dy, item* _item)
{
	fx_tile my_fx[MAP_WIDTH][MAP_HEIGHT];
	map_tile *tile;
	map *_map;
	int x, y, i, dist=0, damage_amount;
	bool visible;
	map_tile* target_tile;

	assert(_creature != NULL);
	_map = _creature->get_map();
	assert(_map != NULL);

	// find the target tile and determine visibility
	visible = (_creature->is_visible()) || (_creature->is_hero());
	int max_distance=4;
	while(1)
	{
		dist++;
		x = _creature->get_x() + (dist * dx);
		y = _creature->get_y() + (dist * dy);
		tile = map::get_tile(_map, x, y);
		if ((tile == NULL) || (tile->is_solid))
		{
			// stop before walls
			dist--;
			break;
		}
		if (tile->vis == V_VISIBLE)
		{
			visible = true;
		}
		if (tile->my_creature != NULL)
		{
			// stop at creatures
			damage_amount = _creature->calculate_damage(tile->my_creature,
           _item->get_min_damage(),_item->get_max_damage(),
            0, (tile->vis == V_VISIBLE) || (tile->my_creature->is_hero()) );

			if (damage_amount) //if missed, continue
				break;
		}
		if (dist==max_distance)
			break;
	}

	// post: wait for message, clear FX
	{
		clear_fx _action = clear_fx(*_map);
		stack::push_action(_action);
	}
	if (visible)
	{
		wait_if_message _action = wait_if_message(true);
		stack::push_action(_action);
	}

	// prep fx
	for (y = 0; y < MAP_HEIGHT; y++)
	{
		for (x = 0; x < MAP_WIDTH; x++)
		{
			my_fx[x][y].is_fx = false;
		}
	}

	// do 
	for (i = (dist == 0) ? (0) : (1); i <= dist; i++)
	{
		x = _creature->get_x() + (i * dx);
		y = _creature->get_y() + (i * dy);
		tile = map::get_tile(_map, x, y);
		if (tile != NULL)
		{
			// prep fx
			my_fx[x][y].is_fx = true;
			if (i == dist)
			{
				my_fx[x][y].ch = '*';
				target_tile = map::get_tile(_map, x, y);
			} else {
				my_fx[x][y].ch = ray_chars[dx + 1][dy + 1];
			}
			my_fx[x][y].style = throw_colour(i);
		}
	}

	// show / describe fx
	if (visible)
	{
		{
			show_fx _action = show_fx(*_map, &(my_fx[0][0]));
			stack::push_action(_action);
		}
		{
			if (_creature->is_hero()) 
			{
				display_message _action = display_message("You throw a " + _item->short_name + ".");
				stack::push_action(_action);
			}
			else 
			{
				display_message _action = display_message(_creature->get_name()+ " throws a " + _item->short_name + ".");
				stack::push_action(_action);
			}
		}
	}

	//remove item from creature
	drop_item_to_tile _action2 = drop_item_to_tile(_creature, target_tile, _item);
	stack::push_action(_action2);
}

// --- zap_spines ---

void zap_spines(creature *_creature, int dx, int dy)
{
	fx_tile my_fx[MAP_WIDTH][MAP_HEIGHT];
	map_tile *tile;
	map *_map;
	int x, y, i, dist, damage_amount;
	bool visible;

	assert(_creature != NULL);
	_map = _creature->get_map();
	assert(_map != NULL);

	// find the target tile and determine visibility
	dist = 0;
	visible = (_creature->is_visible()) || (_creature->is_hero());
	while (true)
	{
		dist++;

		x = _creature->get_x() + (dist * dx);
		y = _creature->get_y() + (dist * dy);
		tile = map::get_tile(_map, x, y);
		if ((tile == NULL) || (tile->is_solid))
		{
			// stop before walls
			dist--;
			break;
		}
		if (tile->vis == V_VISIBLE)
		{
			visible = true;
		}
		if (tile->my_creature != NULL)
		{
			// stop at creatures
			break;
		}
	}

	// post zap: wait for message, clear FX
	{
		clear_fx _action = clear_fx(*_map);
		stack::push_action(_action);
	}
	if (visible)
	{
		wait_if_message _action = wait_if_message(true);
		stack::push_action(_action);
	}

	// prep fx
	for (y = 0; y < MAP_HEIGHT; y++)
	{
		for (x = 0; x < MAP_WIDTH; x++)
		{
			my_fx[x][y].is_fx = false;
		}
	}

	// do spikes
	for (i = (dist == 0) ? (0) : (1); i <= dist; i++)
	{
		x = _creature->get_x() + (i * dx);
		y = _creature->get_y() + (i * dy);
		tile = map::get_tile(_map, x, y);
		if (tile != NULL)
		{
			if (tile->my_creature != NULL)
			{
				// check resistances
				damage_amount = random(4, 10);
 
				// damage creature
				{
					creature_damage _action = creature_damage(_creature, tile->my_creature, DT_NORMAL, damage_amount);
					stack::push_action(_action);
				}

				// report
				if ((tile->vis == V_VISIBLE) || (tile->my_creature->is_hero()))
				{
					display_message _action = display_message(describe_spine(_creature, tile->my_creature, damage_amount));
					stack::push_action(_action);
				}
			}

			// prep fx
			if (i % 2 == 1)
			{
				my_fx[x][y].is_fx = true;
				my_fx[x][y].ch = ray_chars[dx + 1][dy + 1];
				my_fx[x][y].style = mk_char_style(192, 192, 192, 0, 0, 0);
			}
		}
	}

	// show / describe fx
	if (visible)
	{
		{
			show_fx _action = show_fx(*_map, &(my_fx[0][0]));
			stack::push_action(_action);
		}
		{
			display_message _action = display_message(describe_spines(_creature));
			stack::push_action(_action);
		}
	}
}

// --- do_summons ---

void do_summons(game &_game, creature *_creature, int number)
{
	map_tile *tile;
	map *_map;
	int x, y, summoned, possible, radius;
	bool visible;

	assert(_creature != NULL);
	_map = _creature->get_map();
	assert(_map != NULL);

	visible = (_creature->is_visible()) || (_creature->is_hero());

	// find a radius at which we can summon the monsters
	radius = 0;
	possible = 0;
	while ((possible < number) && (radius < 4))
	{
		radius++;
		possible = 0;
		for (y = _creature->get_y() - radius; y <= _creature->get_y() + radius; y++)
		{
			for (x = _creature->get_x() - radius; x <= _creature->get_x() + radius; x++)
			{
				tile = map::get_tile(_map, x, y);
				if ((tile != NULL) && (!tile->is_solid) && (tile->my_creature == NULL))
				{
					possible++;
				}
			}
		}
	}

	// try to summon the monsters at the determined radius
	summoned = 0;
	for (y = _creature->get_y() - radius; y <= _creature->get_y() + radius; y++)
	{
		for (x = _creature->get_x() - radius; x <= _creature->get_x() + radius; x++)
		{
			tile = map::get_tile(_map, x, y);
			if ((tile != NULL) && (!tile->is_solid) && (tile->my_creature == NULL))
			{
				if (random(possible) < number - summoned)
				{
					// summon a monster
					summon_creature _action = summon_creature(_game, _map, x, y, CT_SPIDER, false);
					stack::push_action(_action);
					summoned++;
					visible |= (tile->vis == V_VISIBLE);
				}
				possible--;
			}
		}
	}

	// report
	if ((visible) && (summoned > 0))
	{
		if (summoned == 1)
		{
			display_message _action = display_message("There's a puff of smoke.  A spider appears!");
			stack::push_action(_action);
		} else {
			display_message _action = display_message("There's a puff of smoke.  Spiders appear!");
			stack::push_action(_action);
		}
	}
}

// --- summon_creature ---

void summon_creature :: carry_out(const char_key &key)
{
	assert(map::get_tile(my_map, x, y) != NULL);
	assert(map::get_tile(my_map, x, y)->my_creature == NULL);

	new creature(my_game, my_map, x, y, type, can_be_equipped);
}
