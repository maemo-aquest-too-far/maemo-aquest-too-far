// stack.h - 'A Quest Too Far'
//
// Copyright 2010 Geoffrey White
//
// see readme.txt for more details.

#ifndef INCLUDED_STACK
#define INCLUDED_STACK

	#include "charlib.h"
	#include <vector>

	// --- action ---

	class action
	{
	public:
		action() : needs_key(false) {}
		virtual ~action() {}
		virtual action *clone() = 0;

		bool get_needs_key() {return needs_key;}
		virtual void carry_out(const char_key &key) = 0;
		virtual void draw() {}

	protected:
		bool needs_key;
	};

	// --- stack ---

	class stack
	{
	public:
		static void push_action(action &_action);
		static int get_num_actions();

		static bool get_needs_key();
		static void carry_out(const char_key &key);
		static void draw();

	private:
		static std::vector<action *> actions;
	};

#endif // INCLUDED_STACK