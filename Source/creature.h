// creature.h - 'A Quest Too Far'
//
// Copyright 2010 Geoffrey White
//
// see readme.txt for more details.

#ifndef INCLUDED_CREATURE
#define INCLUDED_CREATURE

	#include "brain.h"
	#include "charlib.h"
	#include "stack.h"
	#include <string>
	#include <list>

	// detection parameters
	#define DETECTION_RANGE (10)
	#define DETECTION_STYLE mk_char_style(192, 128, 128, 0, 0, 0)

	// territory
	#define TERRITORY_SIZE (6)

	// attack flags
	#define AF_POISON		(0x01)
	#define AF_FAST			(0x02)
	#define AF_RFIRE		(0x04)
	#define AF_RSPINE		(0x08)
	#define AF_SUMMONS		(0x10)

	// defense flags
	#define DF_PRESIST		(0x01)
	#define DF_FRESIST		(0x02)
	#define DF_INVISIBLE	(0x04)

	// --- equipment slots ---

	enum equip_slot
	{
		ES_WEAPON,
		ES_LEFTRING,	// any item equippable to left right is always equippable to right ring, and vice-verasa
		ES_RIGHTRING,
		ES_NUM
	};
	struct equip_slot_desc
	{
		const char *slot_name;
		const char *equip_plural_text, *equip_text;
		const char *unequip_plural_text, *unequip_text;
	};
	extern equip_slot_desc equip_slot_descs[ES_NUM];

	// --- creature types ---

	enum creature_type
	{
		// pests
		CT_CENTIPEDE,
		CT_SNAKE,
		// brutes
		CT_ANT,
		CT_BEETLE,
		CT_SPIDER,
		CT_BEAR,
		CT_MANTIS,
		CT_GRIFFON,
		CT_TROLL,
		CT_DRAGON,
		// special
		CT_MANTICORE,
		CT_WRAITH,
		// humanoids
		CT_MAN,
		CT_GOBLIN,
		CT_HOBGOBLIN,
		CT_SKELETON,
		CT_MUMMY,
		CT_WITCH,
		//
		CT_NUM
	};

	// --- creature_update ---

	class creature_update : public action
	{
	public:
		creature_update(creature *_creature) : my_creature(_creature) {}
		virtual ~creature_update() {}
		virtual action *clone() {return new creature_update(my_creature);}

		virtual void carry_out(const char_key &key);

	private:
		creature *my_creature;
	};

	// --- creature_damage ---

	enum damage_type
	{
		DT_NORMAL,
		DT_FIRE,
		DT_DISINTEGRATE,
	};
	class creature_damage : public action
	{
	public:
		creature_damage(creature *_attacker, creature *_target, damage_type _type, int _amount) :
		  attacker(_attacker), target(_target), type(_type), amount(_amount) {}
			// 'attacker' may be NULL
		virtual ~creature_damage() {}
		virtual action *clone() {return new creature_damage(attacker, target, type, amount);}

		virtual void carry_out(const char_key &key);

	private:
		creature *attacker, *target;
		damage_type type;
		int amount;
	};

	// --- creature_poison ---

	class creature_poison : public action
	{
	public:
		creature_poison(creature *_attacker, creature *_target, int _turns) :
		  attacker(_attacker), target(_target), turns(_turns) {}
			// 'attacker' may be NULL
		virtual ~creature_poison() {}
		virtual action *clone() {return new creature_poison(attacker, target, turns);}

		virtual void carry_out(const char_key &key);

	private:
		creature *attacker, *target;
		int turns;
	};

	// --- creature ---

	class creature
	{
	public:
		creature(game &_game, class map *_map, int _x, int _y, creature_type _type, bool can_be_equipped);
		~creature();
		void cleanup();

		// description
		// (note: may vary, e.g. based on visibility)
		std::string get_name();				// you / goblin
		std::string get_name_the();			// you / the goblin
		std::string get_name_the_object();	// your / the goblin's
		bool get_use_plural_verb();
		void introduce();

		// position
		map *get_map() {return my_map;}
		int get_x() {return x;}
		int get_y() {return y;}
		struct map_tile *get_map_tile();

		bool is_visible();
		bool is_hero();
		bool is_friends(creature *with);

		u32 get_attack_flags();
		int get_carry_max();

		bool can_move(map_tile *tile);
		void move_to(map *_map, int _x, int _y);
		void attack(creature *target);
		int calculate_damage(creature *target, int min, int max, u32 attack_flags, bool visible);

		void move();
		void update();
		void draw();

		// state
		int hp, hp_max, heal_ctr;
		int poisoned_turns, decline;
		int carrying;
		int damage_min, damage_max;
		bool damage_unarmed;
		bool resists_fire, resists_poison, has_detection, has_invisibility;
		int special_counter, special_recharge; // 0 = ready

		bool had_turn; // has the creature had it's turn yet?

		// brain
		brain my_brain;

		// inventory
		std::list<class item *> my_items;
		item *equipped[ES_NUM]; // must all be in my_items

	private:
		// position
		//  - can be assumed to be on map during normal function (after initialization)
		game &my_game;
		map *my_map;
		int x, y;

		// type
		creature_type type;
	};

#endif // INCLUDED_CREATURE
