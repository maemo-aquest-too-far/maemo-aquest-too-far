// charlib.h
//
// by Geoffrey White

#ifndef INCLUDED_CHARLIB
#define INCLUDED_CHARLIB

	// --- compiler settings ---

	#ifdef WIN32
		#pragma warning(disable:4786)
			// sometimes STL related symbols end up rather long and the 'browse information' can't cope.  I don't care.
	#endif // WIN32

	// --- libs ---

	#include <SDL.h>
	#include <SDL/SDL_image.h>
	#include <string>

	// --- types ---

	typedef Uint32 u32;
	typedef Uint16 u16;
	typedef Uint8 u8;

	// --- debugging / error handling ---

	void char_debug_out(const std::string &message);
		// displays a message on the debug channel (if _DEBUG is defined)
	std::string char_int_str(int i);

	void char_display_error(const std::string &message);
		// displays an error message to the user (e.g. in a messagebox)
		// TODO: make this work better in Linux

	// --- init / update / shutdown ---

	struct char_surface_decl
	{
		// this structure describes a bitmap of tiles that will be loaded
		std::string file_name;				// bitmap file to be loaded
		int grid_width, grid_height;		// tile layout in the bitmap
	};

	struct char_config
	{
		// this structure contains everything CharLib needs to know about your program

		// app details
		std::string window_title;			// displayed window title
		std::string icon_file_name;			// bitmap file to use as the program icon

		// source tiles
		char_surface_decl ascii_decl;		// tiles to load (ascii characters 32 .. 127)

		// grid display
		// (CharLib will display the grid of tiles as large as possible in the window)
		int grid_width, grid_height;		// layout of tiles in the window
		int aspect_width, aspect_height;	// shape of each tile (1, 1 if you want square tiles)
	};

	void char_init(const char_config &config);
		// call this at startup to create the window
	bool char_update();
		// call this frequently to process the window; returns false if the user has chosen to quit
	void char_shutdown();
		// call this when you're done

	// --- tile drawing ---
	//
	// note that changes won't be rendered until 'char_gfx_update' is called

	struct char_style
	{
		// this structure describes the colour to display a tile

		int fore_r, fore_g, fore_b;			// foreground colour (white on the tile becomes this)
		int back_r, back_b, back_g;			// background colour (black on the tile becomes this)
	};
	extern char_style char_default_style; // pre-defined default, white on black

	inline char_style mk_char_style(int fore_r, int fore_g, int fore_b,
									int back_r, int back_g, int back_b)
	{
		char_style style;

		style.fore_r = fore_r;		style.fore_g = fore_g;		style.fore_b = fore_b;
		style.back_r = back_r;		style.back_g = back_g;		style.back_b = back_b;

		return style;
	}

	void char_clear();
		// clears the entire tile grid
	void char_draw(int x, int y, char ch, const char_style &style);
		// draws a single tile
	void char_draw(int x, int y, const std::string &str, const char_style &style);
		// draws a string of tiles one after another (does not wrap)

	// --- keyboard input ---

	struct char_key
	{
		// this structure describes a keypress; non-ascii keys, such as the arrow keys, have ascii
		// code 0 but you can use the sdl code to differentiate them.

		char ascii;
		SDLKey sdl;
	};

	bool char_get_key(char_key &key);
		// returns true if a key was pressed since the last query, and if so, puts it's details
		// into the 'key' parameter.

#endif // INCLUDED_CHARLIB