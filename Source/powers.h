// powers.h - 'A Quest Too Far'
//
// Copyright 2010 Geoffrey White
//
// see readme.txt for more details.

#ifndef INCLUDED_POWERS
#define INCLUDED_POWERS

	#include "creature.h"
	#include "stack.h"

	void do_teleport(creature *_creature);
	void do_teleport_to(creature *_creature, class map *_map, int x, int y);

	void zap_disintegration(creature *_creature, int dx, int dy);
	void zap_displacement(creature *_creature, int dx, int dy);
	void zap_fire(creature *_creature, int dx, int dy);
	void powers_throw_item(creature *_creature, int dx, int dy, item* _item);
	void zap_spines(creature *_creature, int dx, int dy);

	void do_summons(class game &_game, creature *_creature, int number);

	// --- summon_creature ---

	class summon_creature : public action
	{
	public:
		summon_creature(game &_game, map *_map, int _x, int _y, creature_type _type, bool _can_be_equipped) :
		  my_game(_game), my_map(_map), x(_x), y(_y), type(_type), can_be_equipped(_can_be_equipped) {}
		virtual ~summon_creature() {}
		virtual action *clone() {return new summon_creature(my_game, my_map, x, y, type, can_be_equipped);}

		virtual void carry_out(const char_key &key);

	private:
		game &my_game;
		map *my_map;
		int x, y;
		creature_type type;
		bool can_be_equipped;
	};

#endif // INCLUDED_POWERS