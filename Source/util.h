// util.h - 'A Quest Too Far'
//
// Copyright 2010 Geoffrey White
//
// see readme.txt for more details.

#ifndef INCLUDED_UTIL
#define INCLUDED_UTIL

	#include "charlib.h"

	// --- directional stuff ---

	extern int dir_xs[8], dir_ys[8];
	int dir_from(int dx, int dy);

	// --- directional keys ---

	bool translate_direction_key(const char_key &key, int &out_dx, int &out_dy);

#endif // INCLUDED_UTIL