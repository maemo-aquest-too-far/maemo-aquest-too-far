// vis.cpp - 'A Quest Too Far'
//
// Copyright 2010 Geoffrey White
//
// see readme.txt for more details.

#include "vis.h"
#include "map.h"
#include "creature.h"
#include "game.h"
#include "message.h"
#include "util.h"
#include "debug.h"
#include <assert.h>

// --- ray_trace_cb ---

static bool ray_trace_cb(map &_map, int x1, int y1, int x2, int y2, bool (*cb_continue)(map &map, int x, int y))
{
	// ray traces to determine if you can see between the centres of two points; Bresenham algorithm.
	int x, y, frac, increment, denom, dirx, diry;
	int dx = abs(x2 - x1);
	int dy = abs(y2 - y1);

	assert(cb_continue != NULL);

	dirx = (x2 >= x1) ? (1) : (-1);
	diry = (y2 >= y1) ? (1) : (-1);

	if (dx > dy)
	{
		// major axis is horizontal
		x = x1;
		y = y1;
		frac = dx; // 0.5, with denom 2 * dx
		increment = 2 * dy; // dy / dx, with denom 2 * dx
		denom = 2 * dx;
		if (!(*cb_continue)(_map, x, y))
		{
			return false;
		}
		while (x != x2)
		{
			x += dirx;
			frac += increment;
			if (frac >= denom)
			{
				frac -= denom;
				y += diry;
			}
			
			if (!(*cb_continue)(_map, x, y))
			{
				return false;
			}
		}
	} else {
		// major axis is vertical
		x = x1;
		y = y1;
		frac = dy; // 0.5, with denom 2 * dy
		increment = 2 * dx; // dx / dy, with denom 2 * dy
		denom = 2 * dy;
		if (!(*cb_continue)(_map, x, y))
		{
			return false;
		}
		while (y != y2)
		{
			y += diry;
			frac += increment;
			if (frac >= denom)
			{
				frac -= denom;
				x += dirx;
			}
			
			if (!(*cb_continue)(_map, x, y))
			{
				return false;
			}
		}
	}

	return true;
}

// --- ray_trace_vis ---
//
// during the vis_update, we proceed in a spiral pattern and only accept rays passing through *visible*
// room tiles, which prevents far away 'fragment' artifacts.

bool check_vis(map &_map, int x, int y)
{
	map_tile *tile = map::get_tile(&_map, x, y);

	if ((tile == NULL) || (tile->is_solid) || (tile->vis != V_VISIBLE))
	{
		return false;
	} else {
		return true;
	}
}

bool ray_trace_fair(map &_map, int x1, int y1, int x2, int y2)
{
	// always check visibility a fixed way around, for symmetry
	if ((x1 < x2) || ((x1 == x2) && (y1 < y2)))
	{
		return ray_trace_cb(_map, x1, y1, x2, y2, &check_vis);
	} else {
		return ray_trace_cb(_map, x2, y2, x1, y1, &check_vis);
	}
}

bool ray_trace_vis(map &_map, int x1, int y1, int x2, int y2)
{
	int dx, dy;

	if ((x1 < 0) || (x1 >= MAP_WIDTH) ||
		(y1 < 0) || (y1 >= MAP_HEIGHT) ||
		(x2 < 0) || (x2 >= MAP_WIDTH) ||
		(y2 < 0) || (y2 >= MAP_HEIGHT))
	{
		return false;
	}

	dx = 0;
	if (x2 > x1) {dx = 1;}
	if (x2 < x1) {dx = -1;}
	dy = 0;
	if (y2 > y1) {dy = 1;}
	if (y2 < y1) {dy = -1;}

	// accept rays from several closer cells (2x2 block)
	//  - this is to reveal walls around rooms that you can see
	if ((ray_trace_fair(_map, x1, y1, x2, y2)) ||
		(ray_trace_fair(_map, x1, y1, x2 - dx, y2)) ||
		(ray_trace_fair(_map, x1, y1, x2, y2 - dy)) ||
		(ray_trace_fair(_map, x1, y1, x2 - dx, y2 - dy)))
	{
		return true;
	}

	return false;
}

// --- vis_update ---

void vis_make_visible(game &_game, map *_map, int x, int y)
{
	_map->get_tile(x, y)->vis = V_VISIBLE;

	// do we need to introduce a creature?
	if ((_map->get_tile(x, y)->my_creature != NULL) &&
		(_map->get_tile(x, y)->my_creature->is_visible()))
	{
		_map->get_tile(x, y)->my_creature->introduce();
	}

	// do we need to introduce the stairs?
	if ((_map->get_tile(x, y)->is_stairs) &&
		(!_game.stairs_introduced))
	{
		display_message _action = display_message(
			"You see stairs going down.  Presumably your grandchildren are deeper in the dungeon.");
		stack::push_action(_action);
		_game.stairs_introduced = true;
	}
}

void vis_update :: carry_out(const char_key &key)
{
	map *_map = NULL;
	int x, y, r, i;
	bool cont;
#ifdef DEBUG_PROFILE
	int t1, t2;
	t1 = SDL_GetTicks();
#endif // DEBUG_PROFILE

	if (my_creature != NULL)
	{
		_map = my_creature->get_map();
		if (_map != NULL)
		{
			// downgrade all 'visible' tiles to 'explored'
			for (y = 0; y < MAP_HEIGHT; y++)
			{
				for (x = 0; x < MAP_WIDTH; x++)
				{
					if (_map->get_tile(x, y)->vis == V_VISIBLE)
					{
						_map->get_tile(x, y)->vis = V_EXPLORED;
					}
				}
			}

			// calculate new visibility
			x = my_creature->get_x();
			y = my_creature->get_y();
			vis_make_visible(my_game, _map, x, y);
			r = 1;
			cont = true;
			while (cont)
			{
				cont = false;
				for (i = -r; i < r; i++)
				{
					if (ray_trace_vis(*_map, x, y, x + i, y - r))
					{
						vis_make_visible(my_game, _map, x + i, y - r);
						cont = true;
					}
					if (ray_trace_vis(*_map, x, y, x + r, y + i))
					{
						vis_make_visible(my_game, _map, x + r, y + i);
						cont = true;
					}
					if (ray_trace_vis(*_map, x, y, x - i, y + r))
					{
						vis_make_visible(my_game, _map, x - i, y + r);
						cont = true;
					}
					if (ray_trace_vis(*_map, x, y, x - r, y - i))
					{
						vis_make_visible(my_game, _map, x - r, y - i);
						cont = true;
					}
				}
				r++;
			}

			// post-process
			for (y = 0; y < MAP_HEIGHT - 1; y++)
			{
				for (x = 0; x < MAP_WIDTH - 1; x++)
				{
					i = 0;
					i += (_map->get_tile(x, y)->vis == V_VISIBLE) ? (1) : (0);
					i += (_map->get_tile(x + 1, y)->vis == V_VISIBLE) ? (1) : (0);
					i += (_map->get_tile(x, y + 1)->vis == V_VISIBLE) ? (1) : (0);
					i += (_map->get_tile(x + 1, y + 1)->vis == V_VISIBLE) ? (1) : (0);
					if (i == 3)
					{
						// diagonal tunnels rule - you can see a diagonal wall, reveal the 'corner point';
						// this is somewhat generous, but allows side tunnelss off diagonal tunnels to be
						// seen, and improves the visibility of room corners.
						//	X#
						//	#.
						if ((!_map->get_tile(x + 1, y + 1)->is_solid) &&
							(_map->get_tile(x + 1, y)->is_solid) && (_map->get_tile(x, y + 1)->is_solid))
						{
							_map->get_tile(x, y)->vis = V_VISIBLE;
						}
						if ((!_map->get_tile(x, y + 1)->is_solid) &&
							(_map->get_tile(x, y)->is_solid) && (_map->get_tile(x + 1, y + 1)->is_solid))
						{
							_map->get_tile(x + 1, y)->vis = V_VISIBLE;
						}
						if ((!_map->get_tile(x + 1, y)->is_solid) &&
							(_map->get_tile(x + 1, y + 1)->is_solid) && (_map->get_tile(x, y)->is_solid))
						{
							_map->get_tile(x, y + 1)->vis = V_VISIBLE;
						}
						if ((!_map->get_tile(x, y)->is_solid) &&
							(_map->get_tile(x, y + 1)->is_solid) && (_map->get_tile(x + 1, y)->is_solid))
						{
							_map->get_tile(x + 1, y + 1)->vis = V_VISIBLE;
						}
					}
				}
			}
		}
	}

#ifdef DEBUG_PROFILE
	t2 = SDL_GetTicks();
	char_debug_out("vis_update took " + char_int_str(t2 - t1) + "ms");
#endif // DEBUG_PROFILE
}