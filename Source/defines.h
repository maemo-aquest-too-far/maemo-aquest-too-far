// defines.h - 'A Quest Too Far'
//
// Copyright 2010 Geoffrey White
//
// see readme.txt for more details.

#ifndef INCLUDED_DEFINES
#define INCLUDED_DEFINES

	#define MIN(a, b) ((a) < (b) ? (a) : (b))
	#define MAX(a, b) ((a) > (b) ? (a) : (b))

	// display grid size (1.4 : 1)
	#define GRID_WIDTH (56)
	#define GRID_HEIGHT (40)

#endif