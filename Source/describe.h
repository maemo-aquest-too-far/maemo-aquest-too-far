// describe.h - 'A Quest Too Far'
//
// Copyright 2010 Geoffrey White
//
// see readme.txt for more details.

#ifndef INCLUDED_DESCRIBE
#define INCLUDED_DESCRIBE

	#include "creature.h"
	#include <string>

	// attacks
	std::string describe_attack(creature *attacker, creature *target, bool attack_hit, int damage_amount);
	std::string describe_poison(creature *attacker, creature *target, int turns); // 'attacker' may be NULL
	std::string describe_burn(creature *attacker, creature *target, int damage_amount); // 'attacker' may be NULL
	std::string describe_spine(creature *attacker, creature *target, int damage_amount); // 'attacker' may be NULL
	std::string describe_hurt(creature *attacker, creature *target, damage_type type); // 'attacker' may be NULL
	std::string describe_killed(creature *attacker, creature *target, damage_type type); // 'attacker' may be NULL

	// behaviour
	std::string describe_warning(creature *who, creature *target, warning_type type);

	// items
	std::string describe_things_here(struct map_tile *tile);
	std::string describe_pickup(creature *who, class item *what);
	std::string describe_drop(creature *who, item *what);
	std::string describe_equip(creature *who, item *what, equip_slot slot, bool unequip);
	enum candle_action
	{
		CA_LIGHT,
		CA_UNLIGHT,
		CA_EXPIRE
	};
	std::string describe_candle(creature *who, item *what, candle_action _action);

	// special
	std::string describe_healed(creature *who);
	std::string describe_poison_off(creature *who);
	std::string describe_decline(creature *who);
	std::string describe_spines(creature *who);

#endif // INCLUDED_DESCRIBE