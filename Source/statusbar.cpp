// statusbar.cpp - 'A Quest Too Far'
//
// Copyright 2010 Geoffrey White
//
// see readme.txt for more details.

#include "defines.h"
#include "statusbar.h"
#include "game.h"
#include "creature.h"
#include <assert.h>

#define POISON_STYLE mk_char_style(0, 224, 0, 0, 0, 0)

struct status_values
{
	int hp, hp_max;
	int damage_min, damage_max;
	int item, item_max;
	int decline;
	bool has_detection;
	bool resists_fire;
	bool resists_poison;
	bool has_invisibility;
	bool is_poisoned;
	int dlevel;
};
status_values my_status;
bool my_status_inited = false;

void draw_status(int y, game *_game, creature *_creature)
{
	char_style style;
	std::string str;
	int x;

	// draw basic status bar
	str = "hp:      dmg:     dec:          item:      dlvl:        ";
	   // "hp:99/99 dmg:9-99 dec:9999 POIS item:99/99 dlvl:9 DEFRIV"
	   //  ----------*---------*---------*---------*---------*-----
	assert(str.size() == GRID_WIDTH);
	char_draw(0, y, str, mk_char_style(192, 192, 192, 0, 0, 0));

	// get values
	if (_creature != NULL)
	{
		my_status.hp = _creature->hp;
		my_status.hp_max = _creature->hp_max;
		my_status.damage_min = _creature->damage_min;
		my_status.damage_max = _creature->damage_max;
		my_status.item = _creature->carrying;
		my_status.item_max = _creature->get_carry_max();
		my_status.decline = _creature->decline;
		my_status.has_detection = _creature->has_detection;
		my_status.resists_fire = _creature->resists_fire;
		my_status.resists_poison = _creature->resists_poison;
		my_status.has_invisibility = _creature->has_invisibility;
		my_status.is_poisoned = (_creature->poisoned_turns > 0);
		my_status.dlevel = _game->level + 1;
		my_status_inited = true;
	}

	// fill in values
	if (my_status_inited)
	{
		// hit points
		style = mk_char_style(255, 0, 0, 0, 0, 0);
		if (my_status.hp > my_status.hp_max / 3)
		{
			style = mk_char_style(255, 255, 0, 0, 0, 0);
		}
		if (my_status.hp > (my_status.hp_max * 2) / 3)
		{
			style = mk_char_style(0, 255, 0, 0, 0, 0);
		}
		char_draw(3, y, char_int_str(my_status.hp) + "/" + char_int_str(my_status.hp_max), style);

		// damage
		char_draw(13, y, char_int_str(my_status.damage_min) + "-" +
						 char_int_str(my_status.damage_max), char_default_style);

		// decline
		style = char_default_style;
		if (my_status.is_poisoned)
		{
			style = mk_char_style(255, 0, 0, 0, 0, 0);
		}
		char_draw(22, y, char_int_str(my_status.decline), style);

		// poisoned?
		if (my_status.is_poisoned)
		{
			char_draw(27, y, "POIS", POISON_STYLE);
		}

		// items
		style = char_default_style;
		if (my_status.item > my_status.item_max)
		{
			style = mk_char_style(255, 0, 0, 0, 0, 0);
		}
		char_draw(37, y, char_int_str(my_status.item) + "/" + char_int_str(my_status.item_max), style);

		
		// dungeon level
		char_draw(48, y, char_int_str(my_status.dlevel), char_default_style);

		// flags
		x = 50;
		if (my_status.has_detection)
		{
			char_draw(x, y, "DE", DETECTION_STYLE);
			x += 2;
		}
		if (my_status.resists_fire)
		{
			char_draw(x, y, "FR", mk_char_style(224, 0, 0, 0, 0, 0));
			x += 2;
		}
		if (my_status.resists_poison)
		{
			char_draw(x, y, "PR", POISON_STYLE);
			x += 2;
		}
		if (my_status.has_invisibility)
		{
			char_draw(x, y, "IV", mk_char_style(192, 192, 192, 0, 0, 0));
			x += 2;
		}
		assert(x <= 56); // (there's only room for 3 status flags; having more should be impossible)
	}
}