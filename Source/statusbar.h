// statusbar.h - 'A Quest Too Far'
//
// Copyright 2010 Geoffrey White
//
// see readme.txt for more details.

#ifndef INCLUDED_STATUSBAR
#define INCLUDED_STATUSBAR

	void draw_status(int y, class game *_game, class creature *_creature);

#endif // INCLUDED_STATUSBAR