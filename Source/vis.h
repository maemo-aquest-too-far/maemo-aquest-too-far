// vis.h - 'A Quest Too Far'
//
// Copyright 2010 Geoffrey White
//
// see readme.txt for more details.

#ifndef INCLUDED_VIS
#define INCLUDED_VIS

	#include "stack.h"

	class vis_update : public action
	{
	public:
		vis_update(class game &_game, class creature *_creature) :
			my_game(_game), my_creature(_creature) {}
		virtual ~vis_update() {}
		virtual action *clone() {return new vis_update(my_game, my_creature);}

		virtual void carry_out(const char_key &key);

	private:
		game &my_game;
		creature *my_creature;
	};

#endif // INCLUDED_VIS