// inventory.h - 'A Quest Too Far'
//
// Copyright 2010 Geoffrey White
//
// see readme.txt for more details.

#ifndef INCLUDED_INVENTORY
#define INCLUDED_INVENTORY

	#include "creature.h"
	#include "stack.h"
	#include "item.h"

	// --- display_inventory ---

	enum inventory_mode
	{
		IM_INVENTORY,
		IM_WIELD,
		IM_PICKUP,
		IM_DROP
	};

	class display_inventory : public action
	{
	public:
		display_inventory(creature *_creature, inventory_mode _mode, action &_cancel_action);
		virtual ~display_inventory();
		virtual action *clone() {return new display_inventory(my_creature, mode, *my_cancel_action);}

		virtual void carry_out(const char_key &key);
		virtual void draw();

	private:
		bool can_pick_item(class item *_item);

		creature *my_creature;
		inventory_mode mode;
		std::list<item *> *items;
		int offset;
		action *my_cancel_action;
	};

	// --- pickup_item ---

	class pickup_item : public action
	{
	public:
		pickup_item(creature *_creature, class item *_item);
		virtual ~pickup_item() {}
		virtual action *clone() {return new pickup_item(my_creature, my_item);}

		virtual void carry_out(const char_key &key);

	private:
		creature *my_creature;
		item *my_item;
	};

	// --- drop_item ---

	class drop_item : public action
	{
	public:
		drop_item(creature *_creature, class item *_item);
		virtual ~drop_item() {}
		virtual action *clone() {return new drop_item(my_creature, my_item);}

		virtual void carry_out(const char_key &key);

	private:
		creature *my_creature;
		item *my_item;
	};

	// --- drop_item_to_tile ---

	class drop_item_to_tile : public action
	{
	public:
		drop_item_to_tile(creature *_creature, map_tile *_tile, class item *_item);
		virtual ~drop_item_to_tile() {}
		virtual action *clone() {return new drop_item_to_tile(my_creature, my_tile, my_item);}

		virtual void carry_out(const char_key &key);

	private:
		creature *my_creature;
		map_tile *my_tile;
		item *my_item;
	};

	
	// --- equip_item ---

	class equip_item : public action
	{
	public:
		equip_item(creature *_creature, class item *_item, equip_slot _slot);
		virtual ~equip_item() {}
		virtual action *clone() {return new equip_item(my_creature, my_item, slot);}

		virtual void carry_out(const char_key &key);

	private:
		creature *my_creature;
		item *my_item;
		equip_slot slot;
	};

	// --- activate_item ---

	class activate_item : public action
	{
	public:
		activate_item(creature *_creature, class item *_item, action &_cancel_action);
		virtual ~activate_item();
		virtual action *clone() {return new activate_item(my_creature, my_item, *my_cancel_action);}

		virtual void carry_out(const char_key &key);
		void draw();

	private:
		creature *my_creature;
		item *my_item;
		action *my_cancel_action;
	};

	// --- consume_item ---

	class consume_item : public action
	{
	public:
		consume_item(creature *_creature, item *_item);
		virtual ~consume_item() {}
		virtual action *clone() {return new consume_item(my_creature, my_item);}

		virtual void carry_out(const char_key &key);

	private:
		creature *my_creature;
		item *my_item;
	};

#endif // INCLUDED_INVENTORY