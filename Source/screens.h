// screens.h - 'A Quest Too Far'
//
// Copyright 2010 Geoffrey White
//
// see readme.txt for more details.

#ifndef INCLUDED_SCREENS
#define INCLUDED_SCREENS
	#include "creature.h"
	#include "stack.h"

	// --- intro_screen ---

	class intro_screen : public action
	{
	public:
		intro_screen();
		virtual ~intro_screen() {}
		virtual action *clone() {return new intro_screen();}

		virtual void carry_out(const char_key &key);
		virtual void draw();

	private:
	};

	// --- help_screen ---

	class help_screen : public action
	{
	public:
		help_screen();
		virtual ~help_screen() {}
		virtual action *clone() {return new help_screen();}

		virtual void carry_out(const char_key &key);
		virtual void draw();

	private:
	};

	// --- history_screen ---

	class history_screen : public action
	{
	public:
		history_screen();
		virtual ~history_screen() {}
		virtual action *clone() {return new history_screen();}

		virtual void carry_out(const char_key &key);
		virtual void draw();

	private:
	};

		// --- suicide_dialog ---

	class suicide_dialog : public action
	{
	public:
		suicide_dialog(creature *_hero);
		virtual ~suicide_dialog() {}
		virtual action *clone() {return new suicide_dialog(my_hero);}

		virtual void carry_out(const char_key &key);
		virtual void draw();

	private:
		creature* my_hero;
	};
	
#endif // INCLUDED_SCREENS
