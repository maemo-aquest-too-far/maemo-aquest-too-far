// map.cpp - 'A Quest Too Far'
//
// Copyright 2010 Geoffrey White
//
// see readme.txt for more details.

#include "map.h"
#include "defines.h"
#include "game.h"
#include "creature.h"
#include "item.h"
#include "rng.h"
#include "debug.h"
#include <assert.h>

// --- directional stuff ---

static int dir_xs[8] = {0, 1, 1, 1, 0, -1, -1, -1};
static int dir_ys[8] = {-1, -1, 0, 1, 1, 1, 0, -1};

// --- level types ---

struct level_info
{
	const char *introduction;
	char_style floor_style, wall_style, alt_style;
	int rooms_gen, room_min, room_max;
	int tunnels_gen, min_area;
	int monsters_min, monsters_max;
};
level_info level_infos[LT_NUM] =
//		 ########################################################
{	// LT_ENTRY
	{	"You enter the cave.  It's cold and dark, and the floor is slimy.  Brings back memories...",
		{224, 255, 224, 0, 0, 0},		{224, 255, 224, 0, 0, 0},		{135, 180, 135, 0, 0, 0},	// alt = 60:40 mix with 0, 64, 0
		500, 3, 8,		// largeish rooms (rounded)
		300,
		700,
		10, 15			// fewer monsters
	},
	// LT_TUNNELS
	{	"There's a foul smell to these narrow, earthy tunnels.",
		{255, 224, 192, 0, 0, 0},		{192, 160, 128, 0, 0, 0},		{192, 192, 192, 0, 0, 0},
		50, 2, 4,		// very few, very small rooms
		750,			// lots of tunnels
		250,
		15, 20
	},
	// LT_CRYPT
	{	"There's a chill to the air in this dusty level.",
		{224, 224, 255, 0, 0, 0},		{224, 224, 255, 0, 0, 0},		{224, 255, 255, 0, 0, 0},
		3000, 2, 7,		// smallish rooms (including crypts)
		300,			// lots of tunnels
		600,
		15, 20
	},
	// LT_GOBLINHALLS
	{	"The sound of distant chatter fills these halls.",
		{255, 255, 224, 0, 0, 0},		{255, 255, 224, 0, 0, 0},		{155, 180, 135, 0, 0, 0},	// alt = 60:40 mix with 0, 64, 0
		2000, 4, 14,	// huge rooms
		200,
		900,
		30, 40			// double number of monsters
	},
	// LT_LAIR
	{	"There's a wicked cackling sound in the distance.",
		{192, 192, 192, 0, 0, 0},		{192, 192, 192, 0, 0, 0},		{128, 128, 128, 0, 0, 0},
		300, 2, 8,		// range of rooms (some overlapping)
		100,			// fewer tunnels
		700,
		15, 20
	},
};

// --- perlin noise ---

int perlin_grid[16][16];

void init_perlin()
{
	int i, j;

	for (i = 0; i < 16; i++)
	{
		for (j = 0; j < 16; j++)
		{
			perlin_grid[i][j] = random(256);
		}
	}
}

int sample_perlin(int x, int y)
{
	int i, divider;
	int bx, by, rx, ry, offsx, offsy, val, weight, sum;

	sum = 0;
	divider = 1;
	for (i = 0; i < 5; i++)
	{
		// linear interpolated sample
		bx = x / divider;
		rx = x % divider;
		by = y / divider;
		ry = y % divider;
		val = 0;
		for (offsx = 0; offsx <= 1; offsx++)
		{
			for (offsy = 0; offsy <= 1; offsy++)
			{
				weight = ((offsx == 0) ? (divider - rx) : (rx)) *
						 ((offsy == 0) ? (divider - ry) : (ry));
				val += perlin_grid[(bx + offsx) & 0xF][(by + offsy) & 0xF] * weight;
			}
		}
		sum += val / (divider * divider);

		divider *= 2;
	}

	return (sum / 5);
}

// --- map ---

map :: map()
{
	int x, y;

	for (y = 0; y < MAP_HEIGHT; y++)
	{
		for (x = 0; x < MAP_WIDTH; x++)
		{
			tiles[x][y].display_ch = ' ';
			tiles[x][y].display_style = char_default_style;
			tiles[x][y].is_fx = false;
			tiles[x][y].vis = V_HIDDEN;
			tiles[x][y].my_creature = NULL;
		}
	}
	start_x = -1;
	start_y = -1;
}

map :: ~map()
{
	clear_creatures();
	clear_items();
}

void map :: clear_items()
{
	item *_item;
	int x, y;

	// delete all items on the map
	for (y = 0; y < MAP_HEIGHT; y++)
	{
		for (x = 0; x < MAP_WIDTH; x++)
		{
			while (!tiles[x][y].my_items.empty())
			{
				_item = tiles[x][y].my_items.front();
				delete _item;
				tiles[x][y].my_items.pop_front();
			}
		}
	}
}

void map :: clear_creatures()
{
	int x, y;

	// delete all creatures on the map
	for (y = 0; y < MAP_HEIGHT; y++)
	{
		for (x = 0; x < MAP_WIDTH; x++)
		{
			if (tiles[x][y].my_creature != NULL)
			{
				delete tiles[x][y].my_creature;
				tiles[x][y].my_creature = NULL;
			}
		}
	}
}

bool map :: add_room(level_type level)
{
	int x, y, w, h, x2, y2;
	bool rounded_room, ring_room/*, coffin_room*/;

	// place the room
	x = random(1, MAP_WIDTH - 3);
	y = random(1, MAP_HEIGHT - 3);
	w = random(level_infos[level].room_min, level_infos[level].room_max);
	h = random(level_infos[level].room_min, level_infos[level].room_max);
	if ((x + w - 1 >= MAP_WIDTH - 1) ||
		(y + h - 1 >= MAP_HEIGHT - 1))
	{
		return false;
	}

	if ((level == LT_LAIR) && (random(15) == 0))
	{
		// occasional overlapping rooms
	} else {
		// check the room
		for (y2 = y - 1; y2 < y + h + 1; y2++)
		{
			for (x2 = x - 1; x2 < x + w + 1; x2++)
			{
				if (!tiles[x2][y2].is_solid)
				{
					return false;
				}
			}
		}
	}

	rounded_room = false;
	ring_room = false;
//	coffin_room = false;
	if (level == LT_ENTRY)
	{
		// round room
		rounded_room = true;
	}
	if ((level == LT_GOBLINHALLS) && (random(3) == 0))
	{
		// ring room (with wall in the middle)
		ring_room = true;
	}
/*	if ((level == LT_CRYPT) && ((w % 2) == 1) && ((h % 2) == 1) &&
		((w >= 5) || (h >= 5)) && (random(2) == 0))
	{
		// coffin room (must be an appropriate size)
		coffin_room = true;
	}*/

	// add the room
	for (y2 = y; y2 < y + h; y2++)
	{
		for (x2 = x; x2 < x + w; x2++)
		{
			tiles[x2][y2].display_ch = '.';
			tiles[x2][y2].is_solid = false;

			if (ring_room)
			{
				// room with a wall in the middle
				if ((x2 >= x + 2) && (x2 < x + w - 2) &&
					(y2 >= y + 2) && (y2 < y + h - 2))
				{
					tiles[x2][y2].display_ch = '#';
					tiles[x2][y2].is_solid = true;
				}
			}
			if (rounded_room)
			{
				// room with rounded corners
				if (((x2 == x) || (x2 == x + w - 1)) &&
					((y2 == y) || (y2 == y + h - 1)) &&
					(random(3) != 0))
				{
					tiles[x2][y2].display_ch = '#';
					tiles[x2][y2].is_solid = true;
				}
			}
/*			if (coffin_room)
			{
				// room with coffins in it
				if ((x2 >= x + 1) && (x2 < x + w - 1) &&
					(y2 >= y + 1) && (y2 < y + h - 1))
				{
					if ((((x2 - x) % 2) == 1) && (((y2 - y) % 2) == 1))
					{
						tiles[x2][y2].display_ch = '=';
						tiles[x2][y2].is_solid = false;
					}
				}
			}*/
		}
	}

	return true;
}

struct point
{
	int x, y;
};

bool map :: add_tunnel(level_type level)
{
	std::vector<point> points;
	std::vector<point>::iterator it;
	point p;
	int dir;

	// start the tunnel
	p.x = random(1, MAP_WIDTH - 2);
	p.y = random(1, MAP_HEIGHT - 2);
	dir = random(4) * 2;
	if (level == LT_TUNNELS)
	{
		// allow diagonal tunnels
		dir = random(8);
	}

	// tunnels must start in a room
	if (tiles[p.x][p.y].is_solid)
	{
		return false;
	}

	// go to the edge of the room
	while (!tiles[p.x][p.y].is_solid)
	{
		p.x += dir_xs[dir];
		p.y += dir_ys[dir];
		if ((p.x <= 0) || (p.x >= MAP_WIDTH - 1) ||
			(p.y <= 0) || (p.y >= MAP_HEIGHT - 1))
		{
			// going off map
			return false;
		}
	}

	// trace the tunnel
	while (tiles[p.x][p.y].is_solid)
	{
		points.push_back(p);

		// chance of turning mid-tunnel
		if (level == LT_TUNNELS)
		{
			if (random(10) == 0)
			{
				dir = (dir + 1) % 8;
			}
			if (random(10) == 0)
			{
				dir = (dir + 7) % 8;
			}
		} else {
			if (random(40) == 0)
			{
				dir = (dir + 2) % 8;
			}
			if (random(40) == 0)
			{
				dir = (dir + 6) % 8;
			}
		}

		if ((!tiles[p.x + dir_xs[(dir + 2) % 8]][p.y + dir_ys[(dir + 2) % 8]].is_solid) || // 90 degrees right
			(!tiles[p.x + dir_xs[(dir + 6) % 8]][p.y + dir_ys[(dir + 6) % 8]].is_solid)) // 90 degrees left
		{
			// clipping another tunnel
			return false;
		}

		p.x += dir_xs[dir];
		p.y += dir_ys[dir];
		if ((p.x <= 0) || (p.x >= MAP_WIDTH - 1) ||
			(p.y <= 0) || (p.y >= MAP_HEIGHT - 1))
		{
			// going off map
			return false;
		}
	}

	// add the tunnel
	for (it = points.begin(); it != points.end(); it++)
	{
		p = *it;
		tiles[p.x][p.y].display_ch = '.';
		tiles[p.x][p.y].is_solid = false;
	}

	return true;
}

bool map :: add_player_start(game &the_game)
{
	int x, y, i;

	for (i = 0; i < 100; i++)
	{
		x = random(1, MAP_WIDTH - 2);
		y = random(1, MAP_HEIGHT - 2);
		if ((!tiles[x][y].is_solid) &&
			(tiles[x][y].my_creature == NULL))
		{
			start_x = x;
			start_y = y;
			return true;
		}
	}

	return false;
}

bool map :: add_stairs(game &the_game, level_type level)
{
	int x, y, sx, sy, sspots;

	assert(start_x != -1);

	sspots = 0;
	for (y = 0; y < MAP_HEIGHT; y++)
	{
		for (x = 0; x < MAP_WIDTH; x++)
		{
			if ((!tiles[x][y].is_solid) &&
				(abs(x - start_x) > 10) && (abs(y - start_y) > 10))
			{
				sspots++;
				if (random(sspots) == 0)
				{
					sx = x;
					sy = y;
				}
			}
		}
	}

	if (sspots > 0)
	{
		// place the stairs
		tiles[sx][sy].display_ch = '>';
		tiles[sx][sy].is_stairs = true;
		tiles[sx][sy].display_style = mk_char_style(255, 255, 255, 64, 64, 64);

		if (level == LT_LAIR)
		{
			// place the witch above the stairs
			if ((tiles[sx][sy].my_creature != NULL) ||
				((sx == start_x) && (sy == start_y)))
			{
				return false;
			}
			new creature(the_game, this, sx, sy, CT_WITCH, true);
		}

		return true;
	}
	
	return false;
}

bool map :: add_monster(game &the_game, level_type level)
{
	creature_type ct;
	int x, y, i;

	// choose a location
	x = random(1, MAP_WIDTH - 2);
	y = random(1, MAP_HEIGHT - 2);
	if ((tiles[x][y].is_solid) ||
		(tiles[x][y].my_creature != NULL) ||
		((x == start_x) && (y == start_y)))
	{
		return false;
	}

	// choose a creature type
	i = random(18);
	// basic monsters
	ct = CT_GOBLIN;												// common (4)
	if ((i == 4) || (i == 5))			{ct = CT_CENTIPEDE;}	// uncommon (2)
	if ((i == 6) || (i == 7))			{ct = CT_ANT;}			// uncommon (2)
	if ((i == 8) || (i == 9))			{ct = CT_SPIDER;}		// uncommon (2)
	if ((i == 10) || (i == 11))			{ct = CT_BEETLE;}		// uncommon (2)
	if (i == 12)						{ct = CT_MANTIS;}		// rare (1)
	// rougher monsters
	if (i == 13)						{ct = CT_HOBGOBLIN;}	// rare (1)
	if (i == 14)						{ct = CT_MANTICORE;}	// rare (1)
	if (i == 15)						{ct = CT_GRIFFON;}		// rare (1)
	if (i == 16)						{ct = CT_TROLL;}		// rare (1)
	if (i == 17)						{ct = CT_DRAGON;}		// rare (1)

	switch (level)
	{
	case LT_ENTRY:
		{
			// replace rougher monsters with entry cave ones
			if ((i >= 13) && (i <= 15))	{ct = CT_SNAKE;}		// common (3)
			if ((i == 16) || (i == 17))	{ct = CT_BEAR;}			// uncommon (2)
		} break;

	case LT_TUNNELS:
		{
			// lots of trolls
			if (random(8) == 0)
			{
				ct = CT_TROLL;
			}
		} break;

	case LT_CRYPT:
		{
			// replace goblins with skeletons, hobgoblins with mummy's
			if ((i >= 0) && (i <= 3))	{ct = CT_SKELETON;}		// common (4)
			if (i == 13)				{ct = CT_MUMMY;}		// rare (1)

			// lots of wraiths
			if (random(8) == 0)
			{
				ct = CT_WRAITH;
			}
		} break;

	case LT_GOBLINHALLS:
		{
			// lots of goblins and hobgoblins
			// (there are twice as many monsters on this level)
			if (random(2) == 0)
			{
				ct = (random(5) == 0) ? (CT_HOBGOBLIN) : (CT_GOBLIN);
			}
		} break;
		
	case LT_LAIR:
		{
		} break;

	default:
		{
			assert(false);
		} break;
	}

	// create the creature
	new creature(the_game, this, x, y, ct, true);
	return true;
}

int map :: make_connected(game &the_game)
{
	bool is_connected[MAP_WIDTH][MAP_HEIGHT];
	int x, y, area, prev_area;

	// start with only the hero's start point connected
	for (y = 0; y < MAP_HEIGHT; y++)
	{
		for (x = 0; x < MAP_WIDTH; x++)
		{
			is_connected[x][y] = false;
		}
	}
	assert(start_x != -1);
	is_connected[start_x][start_y] = true;

	area = 0;
	prev_area = -1;
	while (area != prev_area)
	{
		// flood fill right and down
		for (y = 1; y < MAP_HEIGHT; y++)
		{
			for (x = 1; x < MAP_WIDTH; x++)
			{
				if ((!tiles[x][y].is_solid) &&
					(!is_connected[x][y]))
				{
					if ((is_connected[x - 1][y]) ||
						(is_connected[x][y - 1]) ||
						(is_connected[x - 1][y - 1]))
					{
						is_connected[x][y] = true;
					}
				}
			}
		}

		// flood fill left and down
		for (y = 1; y < MAP_HEIGHT; y++)
		{
			for (x = MAP_WIDTH - 2; x >= 0; x--)
			{
				if ((!tiles[x][y].is_solid) &&
					(!is_connected[x][y]))
				{
					if ((is_connected[x + 1][y]) ||
						(is_connected[x][y - 1]) ||
						(is_connected[x + 1][y - 1]))
					{
						is_connected[x][y] = true;
					}
				}
			}
		}

		// flood fill right and up
		for (y = MAP_HEIGHT - 2; y >= 0; y--)
		{
			for (x = 1; x < MAP_WIDTH; x++)
			{
				if ((!tiles[x][y].is_solid) &&
					(!is_connected[x][y]))
				{
					if ((is_connected[x - 1][y]) ||
						(is_connected[x][y + 1]) ||
						(is_connected[x - 1][y + 1]))
					{
						is_connected[x][y] = true;
					}
				}
			}
		}


		// flood fill left and up
		for (y = MAP_HEIGHT - 2; y >= 0; y--)
		{
			for (x = MAP_WIDTH - 2; x >= 0; x--)
			{
				if ((!tiles[x][y].is_solid) &&
					(!is_connected[x][y]))
				{
					if ((is_connected[x + 1][y]) ||
						(is_connected[x][y + 1]) ||
						(is_connected[x + 1][y + 1]))
					{
						is_connected[x][y] = true;
					}
				}
			}
		}

		// measure area
		prev_area = area;
		area = 0;
		for (y = 0; y < MAP_HEIGHT; y++)
		{
			for (x = 0; x < MAP_WIDTH; x++)
			{
				if (is_connected[x][y])
				{
					area++;
				}
			}
		}
	}

	// fill in any disconnected areas
	for (y = 0; y < MAP_HEIGHT; y++)
	{
		for (x = 0; x < MAP_WIDTH; x++)
		{
			if (!is_connected[x][y])
			{
				tiles[x][y].display_ch = '#';
				tiles[x][y].is_solid = true;
			}
		}
	}

	return area;
}

bool map :: post_process(game &the_game, level_type level)
{
	int x, y, c1, c2, c3;

	// post-process - variable colour walls
	init_perlin();
	for (y = 0; y < MAP_HEIGHT; y++)
	{
		for (x = 0; x < MAP_WIDTH; x++)
		{
			if (tiles[x][y].is_solid)
			{
				// mix main and alt. wall colours
				c2 = sample_perlin(x, y);
				c2 = (c2 - 128) * 6;
				if (c2 < 0) {c2 = 0;}
				if (c2 > 256) {c2 = 256;}
				c1 = 256 - c2;

				// scale down
				c3 = sample_perlin(x + 100, y) + 128;
				if (c3 > 256) {c3 = 256;}
				c1 = (c1 * c3) >> 8;
				c2 = (c2 * c3) >> 8;

				tiles[x][y].display_style.fore_r =
					((level_infos[level].wall_style.fore_r * c1) + (level_infos[level].alt_style.fore_r * c2)) >> 8;
				tiles[x][y].display_style.fore_g =
					((level_infos[level].wall_style.fore_g * c1) + (level_infos[level].alt_style.fore_g * c2)) >> 8;
				tiles[x][y].display_style.fore_b =
					((level_infos[level].wall_style.fore_b * c1) + (level_infos[level].alt_style.fore_b * c2)) >> 8;
				tiles[x][y].display_style.fore_r = MIN(tiles[x][y].display_style.fore_r, 255);
				tiles[x][y].display_style.fore_g = MIN(tiles[x][y].display_style.fore_g, 255);
				tiles[x][y].display_style.fore_b = MIN(tiles[x][y].display_style.fore_b, 255);
			}
		}
	}

	return true;
}

bool map :: try_gen_level(game &the_game, level_type level, creature *the_hero)
{
	int x, y, i, area, monsters;

	assert(level >= 0);
	assert(level < LT_NUM);
	assert(the_hero != NULL);

	// remove the hero from his current location
	the_hero->move_to(NULL, 0, 0);

	// start with solid rock, nothing else
	clear_creatures();
	clear_items();
	for (y = 0; y < MAP_HEIGHT; y++)
	{
		for (x = 0; x < MAP_WIDTH; x++)
		{
			tiles[x][y].display_ch = '#';
			tiles[x][y].display_style = level_infos[level].floor_style; // (walls are restyled later)
			tiles[x][y].is_fx = false;
			tiles[x][y].is_solid = true;
			tiles[x][y].is_stairs = false;
			tiles[x][y].vis = V_HIDDEN;
			tiles[x][y].my_creature = NULL;
		}
	}
	start_x = -1;
	start_y = -1;

	// add rooms
	for (i = 0; i < level_infos[level].rooms_gen; i++)
	{
		add_room(level);
	}

	// add tunnels
	for (i = 0; i < level_infos[level].tunnels_gen; i++)
	{
		add_tunnel(level);
	}

	// add player start position
	if (!add_player_start(the_game))
	{
		return false;
	}

	// connectedness processing
	area = make_connected(the_game);
#ifdef DEBUG_LEVELGEN
	char_debug_out("level area = " + char_int_str(area));
#endif // DEBUG_LEVELGEN
	if (area < level_infos[level].min_area)
	{
		return false;
	}

	// add stairs
	if (!add_stairs(the_game, level))
	{
		return false;
	}

	// add monsters
	monsters = random(level_infos[level].monsters_min, level_infos[level].monsters_max);
	i = 0;
	while (monsters > 0)
	{
		if (add_monster(the_game, level))
		{
			monsters--;
		} else if (++i > 1000) {
			// give up
			return false;
		}
	}

	// post process
	if (!post_process(the_game, level))
	{
		return false;
	}

	// put the player at the start point
	the_hero->move_to(this, start_x, start_y);

	return true;
}

void map :: gen_level(game &the_game, level_type _level, creature *the_hero)
{
	int attempts = 0;

	level = _level;

#ifdef DEBUG_LEVELGEN
	char_debug_out("generating level (" + char_int_str(_level) + ")");
#endif // DEBUG_LEVELGEN

	// keep trying to generate a level
	while (true)
	{
		attempts++;
		if (try_gen_level(the_game, level, the_hero))
		{
#ifdef DEBUG_LEVELGEN
			char_debug_out("success in " + char_int_str(attempts) + " attempts");
#endif // DEBUG_LEVELGEN
			return;
		}
	}
}

void map :: draw()
{
	int x, y;

	for (y = 0; y < MAP_HEIGHT; y++)
	{
		for (x = 0; x < MAP_WIDTH; x++)
		{
			if (tiles[x][y].vis == V_VISIBLE)
			{
				if ((tiles[x][y].my_items.empty()) ||
					(tiles[x][y].is_stairs))
				{
					// no items (or stairs, which takes priority)
					char_draw(x, y, tiles[x][y].display_ch, tiles[x][y].display_style);
				} else if (tiles[x][y].my_items.size() == 1) {
					// an item
					tiles[x][y].my_items.front()->draw(x, y);
				} else {
					// multiple items
					char_draw(x, y, '*', ITEM_DEFAULT_STYLE);
				}
#ifdef DEBUG_SEE_EVERYTHING
			} else if (true) {
#else
			} else if (tiles[x][y].vis == V_EXPLORED) {
#endif // DEBUG_SEE_EVERYTHING
				char_style fogged_style = tiles[x][y].display_style;

				// explored tiles are darker, slightly blue tinted
				// (the back colour is tinted but not darkened much, since it's usually dark anyway)
				fogged_style.fore_r = (fogged_style.fore_r * 3) / 8;
				fogged_style.fore_g = (fogged_style.fore_g * 3) / 8;
				fogged_style.fore_b = (fogged_style.fore_b * 4) / 8;
				fogged_style.back_r = (fogged_style.back_r * 6) / 8;
				fogged_style.back_g = (fogged_style.back_g * 6) / 8;
				fogged_style.back_b = (fogged_style.back_b * 8) / 8;

				char_draw(x, y, tiles[x][y].display_ch, fogged_style);
			} else {
				assert(tiles[x][y].vis == V_HIDDEN);
				char_draw(x, y, ' ', char_default_style);
			}
		}
	}
}

void map :: draw_fx()
{
	int x, y;

	for (y = 0; y < MAP_HEIGHT; y++)
	{
		for (x = 0; x < MAP_WIDTH; x++)
		{
#ifdef DEBUG_SEE_EVERYTHING
			if (tiles[x][y].is_fx)
#else
			if ((tiles[x][y].is_fx) && (tiles[x][y].vis == V_VISIBLE))
#endif // DEBUG_SEE_EVERYTHING
			{
				char_draw(x, y, tiles[x][y].fx_ch, tiles[x][y].fx_style);
			}
		}
	}
}

map_tile *map :: get_tile(int x, int y)
{
	assert((x >= 0) && (y >= 0) && (x < MAP_WIDTH) && (y < MAP_HEIGHT));

	return &(tiles[x][y]);
}

map_tile *map :: get_tile(map *_map, int x, int y)
{
	if ((_map != NULL) &&
		(x >= 0) && (y >= 0) && (x < MAP_WIDTH) && (y < MAP_HEIGHT))
	{
		return _map->get_tile(x, y);
	} else {
		return NULL;
	}
}

std::string map :: get_level_introduction()
{
	assert(level >= 0);
	assert(level < LT_NUM);

	return level_infos[level].introduction;
}