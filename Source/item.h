// item.h - 'A Quest Too Far'
//
// Copyright 2010 Geoffrey White
//
// see readme.txt for more details.

#ifndef INCLUDED_ITEM
#define INCLUDED_ITEM

	#include "creature.h"
	#include <string>

	#define ITEM_DEFAULT_STYLE	(mk_char_style(255, 255, 128, 0, 0, 0))

	// --- item ---

	class item
	{
	public:
		item(class game &_game);
		virtual ~item();

		// basic item
		std::string short_name, full_name, inventory_ext;
		char display_ch;
		int weight;

		// draw
		void draw(int x, int y);

		// function
		virtual bool can_equip(equip_slot slot);
		virtual void carry_update(creature *_creature);
		virtual bool can_activate(bool &out_is_exhausted);
			// out_is_exhausted is set to true if the only reason we can't activate the item is a lack of charges
		virtual bool is_directional();
		virtual std::string get_direction_query();
		virtual void activate(creature *_creature, int dx, int dy);
		virtual void dropped();
		virtual int get_min_damage();
		virtual int get_max_damage();

	protected:
		game &my_game;
	};

	// --- weapon ---

	enum weapon_type
	{
		WT_DAGGER,
		WT_SPEAR,
		WT_MACE,
		WT_HAMMER,
		WT_SWORD,
		WT_AXE,
		WT_NUM
	};

	class weapon : public item
	{
	public:
		weapon(game &_game, weapon_type _type, int _plus);
		virtual ~weapon();

		virtual bool can_equip(equip_slot slot);
		virtual bool can_activate(bool &out_is_exhausted);
		virtual void carry_update(creature *_creature);
		virtual int get_min_damage();
		virtual int get_max_damage();
		virtual bool is_directional();
		virtual void activate(creature *_creature, int dx, int dy);
		virtual std::string get_direction_query();
	private:
		weapon_type type;
		int plus;
	};

	// --- stick ---

	enum stick_type
	{
		ST_DISINTEGRATION,
		ST_DISPLACEMENT,
		ST_FIRE,
		ST_NUM
	};

	class stick : public item
	{
	public:
		stick(game &_game, stick_type _type, int _plus, int _charges);
		virtual ~stick();

		virtual bool can_equip(equip_slot slot);
		virtual void carry_update(creature *_creature);
		virtual bool can_activate(bool &out_is_exhausted);
		virtual bool is_directional();
		virtual std::string get_direction_query();
		virtual void activate(creature *_creature, int dx, int dy);

	private:
		void update_name();

		stick_type type;
		int plus, charges;
	};

	// --- ring ---

	enum ring_type
	{
		RT_MIGHT,
		RT_DETECTION,
		RT_FRESIST,
		RT_PRESIST,
		RT_NUM
	};

	class ring : public item
	{
	public:
		ring(game &_game, ring_type _type);
		virtual ~ring();

		virtual bool can_equip(equip_slot slot);
		virtual void carry_update(creature *_creature);

	private:
		ring_type type;
	};

	// --- candle ---

	class candle : public item
	{
	public:
		candle(game &_game, int _turns_left);
		virtual ~candle();

		virtual void carry_update(creature *_creature);
		virtual bool can_activate(bool &out_is_exhausted);
		virtual void activate(creature *_creature, int dx, int dy);
		virtual void dropped();

	private:
		void update_name();

		bool is_lit;
		int turns_left;
	};

	// --- consumable (scroll or potion) ---

	enum consumable_type
	{
		SPT_EXTRAHEALING,
		SPT_TELEPORT,
		SPT_MAPPING,
		SPT_NUM
	};
	class consumable : public item
	{
	public:
		consumable(game &_game, consumable_type _type);
		virtual ~consumable();

		virtual bool can_activate(bool &out_is_exhausted);
		virtual void activate(creature *_creature, int dx, int dy);

	private:
		consumable_type type;
	};

#endif // INCLUDED_ITEM