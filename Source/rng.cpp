// rng.cpp - 'A Quest Too Far'
//
// Copyright 2010 Geoffrey White
//
// see readme.txt for more details.

#include "rng.h"
#include "charlib.h"
#include <assert.h>
#include <time.h>

// --- random number generator ---
//
// Mersenne Twister 'MT19937' algorithm
//  - fast algorithm with much better properties than many implementations of rand()
//  - various newer algorithms claim to be marginally better, but I found little consensus
//  - as far as I can tell the algorithm itself can be used 'for any purpose, including
//    commercial use'
//  - this is my own implementation based on Wikipedia pseudocode and other sources

static u32 mt_state[624];
static int mt_index;
static bool mt_inited = false;

void mt_init_generator(u32 seed)
{
	int i;

	mt_state[0] = seed;
	for (i = 1; i < 624; i++)
	{
		mt_state[i] = 0x6c078965 * (mt_state[i - 1] ^ (mt_state[i - 1] >> 30)) + i;
	}
	mt_index = 624;
	mt_inited = true;
}

u32 mt_get_number()
{
	const u32 alternate[2] = {0x00000000, 0x9908b0df};
	int i;
	u32 x;

	assert(mt_inited);

	if (mt_index == 624)
	{
		// generate numbers
		for (i = 0; i < 624; i++)
		{
			x = (mt_state[i] & 0x80000000) + (mt_state[(i + 1) % 624] & 0x7FFFFFFF);
			mt_state[i] = mt_state[(i + 397) % 624] ^ (x >> 1) ^ alternate[x & 0x1];
		}
		mt_index = 0;
	}

	x = mt_state[mt_index++];
	x ^= (x >> 11);
	x ^= (x << 7) & 0x9d2c5680;
	x ^= (x << 15) & 0xefc60000;
	x ^= (x >> 18);

	return x;
}

int random(int less_than)
{
	if (!mt_inited)
	{
		mt_init_generator(time(NULL));
			// (not really precise enough)
	}

	assert(less_than <= 63356); // '% x' method is biased for large x
	return (mt_get_number() % less_than);
}