// brain.h - 'A Quest Too Far'
//
// Copyright 2010 Geoffrey White
//
// see readme.txt for more details.

#ifndef INCLUDED_BRAIN
#define INCLUDED_BRAIN

	#include "charlib.h"
	#include <list>

	// --- brain ---

	enum brain_type
	{
		BT_WANDERING,				// wanders, searches
		BT_TERRITORIAL,				// defends territory cautiously
		BT_TERRITORIALAGGRO,		// defends territory aggressively
		BT_BOSS,					// guards home spot, harasses from a distance
	};

	enum brain_mode
	{
		BM_NOTINITIALIZED,
		BM_WAITING,
		BM_WARNING,
		BM_PATHING,
		BM_ATTACKING,
		BM_ANGRY,				// attacking because we were attacked
	};

	enum warning_type
	{
		WT_NONE,
		WT_GROWL,
		WT_ROAR,
	};

	class brain
	{
	public:
		brain(class game &_game);
		~brain();
		void init(class creature *_creature, brain_type _type, warning_type _warning);

		void move(bool is_final_move);
			// (is_final_move is false for a hasty monster's first move)

		void behave_attacked(creature *attacker);
			// adjusts mode for being attacked
	private:
		bool try_to_step(int dx, int dy, bool &out_progressed);
			// moves to position, or perhaps attacks something there, or returns false if blocked
		bool follow_path();
			// follows the existing planned path, returns true if progressing
		bool ready_to_range();
		bool check_range(int from_x, int from_y, int target_x, int target_y, int *out_dx, int *out_dy);
		bool try_to_range(int target_x, int target_y);
			// tries to do a range attack, returns true if it did

		void behave_normal();
			// sets mode for normal behaviour
		void behave_attacking(bool use_range);
			// path to the player
		void behave_spot(bool is_final_move);
			// adjusts mode for player spotted

		game &my_game;
		creature *my_creature;
		brain_type type;
		int home_x, home_y;

		warning_type warning;
		bool has_warned;

		brain_mode mode;
		std::list<int> planned_path;
		int counter;

		int spot_x, spot_y;
		bool has_spotted;
	};

#endif // INCLUDED_BRAIN