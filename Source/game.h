// game.h - 'A Quest Too Far'
//
// Copyright 2010 Geoffrey White
//
// see readme.txt for more details.

#ifndef INCLUDED_GAME
#define INCLUDED_GAME

	#include "map.h"
	#include "stack.h"
	#include "creature.h"
	#include "charlib.h"
	#include <list>

	// --- game ---

	class game
	{
	public:
		game();
		~game();
		void cleanup();

		bool update(bool key_ready, const char_key &key);

		map the_map;
		creature *the_hero; // can be NULL

		int level;
		level_type level_order[LT_NUM];
		bool creatures_introduced[CT_NUM];
		bool stairs_introduced;

	private:
		std::list<creature *> all_creatures;
		std::list<class item *> all_items;

		void draw();

		void add_creature(creature *_creature);
		void remove_creature(creature *_creature);
			// called by creatures in constructor / destructor

		void add_item(item *_item);
		void remove_item(item *_item);
			// called by items in constructor / destructor

		friend class creature;
		friend class item;
		friend class map;
	};

	// --- go_new_level ---

	class go_new_level : public action
	{
	public:
		go_new_level(game &_game) : my_game(_game) {}
		virtual ~go_new_level() {}
		virtual action *clone() {return new go_new_level(my_game);}

		virtual void carry_out(const char_key &key);

	private:
		game &my_game;
	};

	// --- game_won ---

	class game_won : public action
	{
	public:
		game_won(game &_game);
		virtual ~game_won() {}
		virtual action *clone() {return new game_won(my_game);}

		virtual void carry_out(const char_key &key);
		virtual void draw();

	private:
		game &my_game;
	};

#endif // INCLUDED_GAME