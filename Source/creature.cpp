// creature.cpp - 'A Quest Too Far'
//
// Copyright 2010 Geoffrey White
//
// see readme.txt for more details.

#include "creature.h"
#include "defines.h"
#include "game.h"
#include "map.h"
#include "item.h"
#include "describe.h"
#include "message.h"
#include "rng.h"
#include "debug.h"
#include <assert.h>

#define POISON_MIN_TURNS (10)
#define POISON_MAX_TURNS (20)

// --- equip slots ---

equip_slot_desc equip_slot_descs[ES_NUM] =
{
	{"held",			"wield a",		"wields a",		"put away a",	"puts away a"},	// ES_WEAPON
	{"left hand",		"put on a",		"puts on a",	"take off a",	"takes off a"},	// ES_LEFTRING
	{"right hand",		"put on a",		"puts on a",	"take off a",	"takes off a"}	// ES_RIGHTRING
};

// --- creature_types ---

enum equip_class
{
	EC_NONE,
	EC_BASIC,
	EC_IMPROVED,
	EC_ADVENTURER,
	EC_WITCH,
};

struct creature_stats
{
	// basic / appearance
	const char *name, *name_the, *name_the_object;
	bool use_plural_verb; // true if they talk, false if he talks
	const char *introduction;
	char display_ch;
	char_style display_style;

	// attack
	int hit_percent;
	int unarmed_min, unarmed_max;
	u32 attack_flags;
	equip_class equipment_class;

	// defense
	int hp_min, hp_max;
	u32 defense_flags;

	// brain
	brain_type brain;
	warning_type warning;
};
creature_stats creature_types[CT_NUM] =
{
	//  ########################################################
	// --- PESTS ---
	// CT_CENTIPEDE
	{	"giant centipede",			"the giant centipede",		"the giant centipede's",	false,
		"A poisonous giant centipede crawls into view.",
		'c',						{192,	64,		64,			0,		0,		0},
		80,							1,		2,					AF_POISON,					EC_NONE,
		1,		5,					DF_PRESIST,
		BT_WANDERING,				WT_NONE
	},
	// CT_SNAKE
	{	"snake",					"the snake",				"the snake's",				false,
		"A venemous snake slithers into view.",
		's',						{64,	192,	64,			0,		0,		0},
		80,							1,		4,					AF_POISON,					EC_NONE,
		5,		10,					DF_PRESIST,
		BT_WANDERING,				WT_NONE
	},
	// --- BRUTES ---
	// CT_ANT						(power 0.6 * 4.5 * 12.5 = 26.25)
	{	"giant ant",				"the giant ant",			"the giant ant's",			false,
		"You see a giant ant.  You've slain many of these critters in your time!",
		'a',						{192,	64,		64,			0,		0,		0},
		60,							1,		6,					0,							EC_NONE,
		10,		15,					0,
		BT_TERRITORIALAGGRO,		WT_NONE
	},
	// CT_BEETLE					(power 0.6 * 4.5 * 15 = 40.5)
	{	"giant beetle",				"the giant beetle",			"the giant beetle's",		false,
		"You see an armour plated giant beetle.",
		'b',						{128,	64,		64,			0,		0,		0},
		60,							1,		8,					0,							EC_NONE,
		10,		20,					0,
		BT_TERRITORIALAGGRO,		WT_NONE
	},
	// CT_SPIDER					(power 0.6 * 7 * 15 = 63)
	{	"giant spider",				"the giant spider",			"the giant spider's",		false,
		"You see an enormous hairy spider.",
		's',						{128,	128,	128,			0,		0,		0},
		60,							4,		10,					0,							EC_NONE,
		10,		20,					0,
		BT_TERRITORIALAGGRO,		WT_NONE
	},
	// CT_BEAR						(power 0.6 * 7 * 25 = 105)
	{	"bear",						"the bear",					"the bear's",				false,
		"You come across a bear.  Grumpy and territorial, sometimes it's best to leave these beasts alone.",
		'B',						{128,	96,		0,			0,		0,		0},
		60,							4,		10,					0,							EC_NONE,
		20,		30,					0,
		BT_TERRITORIAL,				WT_GROWL
	},
	// CT_MANTIS					(power 0.6 * 12.5 * 15 = 112.5)
	{	"giant mantis",				"the giant mantis",			"the giant mantis's",		false,
		"You instantly recognize the vicious biting mandibles of a giant mantis.",
		'M',						{96,	128,	96,		0,		0,		0},
		60,							10,		15,					0,							EC_NONE,
		10,		20,					0,
		BT_TERRITORIALAGGRO,		WT_NONE
	},
	// CT_GRIFFON					(power 0.8 * 7 * 2 * 35 = 392)
	{	"griffon",					"the griffon",				"the griffon's",			false,
		"You see a griffon.  Fast and deadly!",
		'G',						{192,	128,	64,			0,		0,		0},
		80,							4,		10,					AF_FAST,					EC_NONE,
		30,		40,					0,
		BT_TERRITORIALAGGRO,		WT_NONE
	},
	// CT_TROLL						(power 0.7 * 15 * 45 = 472.5)
	{	"troll",					"the troll",				"the troll's",				false,
		"You see a very large and very ugly troll.",
		'T',						{0,		128,	0,			0,		0,		0},
		70,							10,		20,					0,							EC_NONE,
		40,		50,					0,
		BT_WANDERING,				WT_NONE
	},
	// CT_DRAGON					(power 0.8 * 7 * 2 * 45 = 504 + specials)
	{	"dragon",					"the dragon",				"the dragon's",				false,
		"You're filled with terror at the sight of a huge, scaly, fire breathing dragon!",
		'D',						{64,	192,	0,			0,		0,		0},
		80,							4,		10,					AF_FAST | AF_RFIRE,			EC_NONE,
		40,		50,					DF_FRESIST,
		BT_TERRITORIAL,				WT_ROAR
	},
	// --- SPECIAL ---
	// CT_MANTICORE					(power 0.7 * 7 * 25 = 122.5 + special)
	{	"manticore",				"the manticore",			"the manticore's",			false,
		"You see a manticore, with fearsome tail spikes!",
		'M',						{192,	192,	128,		0,		0,		0},
		70,							4,		10,					AF_RSPINE,					EC_NONE,
		20,		30,					0,
		BT_TERRITORIALAGGRO,		WT_NONE
	},
	// CT_WRAITH					(power 0.7 * 15 * 25 = 262.5 + special)
	{	"wraith",					"the wraith",				"the wraith's",				false,
		"You are filled with dread at the sight of a wraith!",
		'W',						{255,	255,	255,		0,		0,		0},
		70,							10,		20,					0,							EC_NONE,
		20,		30,					DF_INVISIBLE,
		BT_WANDERING,				WT_NONE
	},
	// --- HUMANOIDS ---
	// CT_MAN						(power 0.8 * 14 (weapon) * 50 = 560 + specials)
	{	"man",						"the man",					"the man's",				false,
		"",
		'@',						{255,	255,	128,		0,		0,		0},
		80,							1,		4,					0,							EC_ADVENTURER,
		50,		50,					0,
		BT_WANDERING,				WT_NONE
	},
	// CT_GOBLIN
	{	"goblin",					"the goblin",				"the goblin's",				false,
		"You see a sneaky goblin.",
		'g',						{128,	192,	0,			0,		0,		0},
		60,							1,		4,					0,							EC_BASIC,
		5,		10,					0,
		BT_WANDERING,				WT_NONE
	},
	// CT_HOBGOBLIN
	{	"hobgoblin",				"the hobgoblin",			"the hobgoblin's",			false,
		"You spot a fierce hobgoblin.",
		'h',						{128,	192,	0,			0,		0,		0},
		70,							1,		4,					0,							EC_IMPROVED,
		10,		15,					0,
		BT_WANDERING,				WT_NONE
	},
	// CT_SKELETON
	{	"skeleton",					"the skeleton",				"the skeleton's",			false,
		"A decaying skeleton stumbles into view.",
		's',						{192,	192,	192,		0,		0,		0},
		60,							1,		4,					0,							EC_BASIC,
		5,		10,					0,
		BT_WANDERING,				WT_NONE
	},
	// CT_MUMMY
	{	"mummy",					"the mummy",				"the mummy's",				false,
		"You see a mummy wrapped in ancient bandages.",
		'm',						{192,	192,	128,		0,		0,		0},
		60,							1,		8,					0,							EC_NONE,
		15,		20,					0,
		BT_TERRITORIALAGGRO,		WT_NONE
	},
	// CT_WITCH
	{	"witch",					"the witch",				"the witch's",				false,
		"You recognize the witch immediately - and remember striking her down all those years ago.  How "\
		"come she's still alive?",
		'w',						{128,	128,	128,			0,		0,		0},
		80,							1,		4,					AF_SUMMONS,					EC_WITCH,
		40,		40,					0,
		BT_BOSS,					WT_NONE
	},
};

// --- creature_update ---

void creature_update :: carry_out(const char_key &key)
{
	assert(my_creature != NULL);
	my_creature->update();
	my_creature->had_turn = true;
}

// --- creature_damage ---

void creature_damage :: carry_out(const char_key &key)
{
	item *_item;
	int i, original_hp;

	// 'attacker' may be NULL
	assert(target != NULL);

	original_hp = target->hp;
	target->hp -= amount;
	if ((target->hp <= 0) && (original_hp > 0))
	{
		// killed (actually deleted in game loop)
		if ((target->is_visible()) || (target->is_hero()))
		{
			if (target->is_hero())
			{
				// when the hero is killed, ensure that the last message is displayed properly
				wait_if_message _action = wait_if_message(true);
				stack::push_action(_action);
			}
			{
				display_message _action = display_message(describe_killed(attacker, target, type));
				stack::push_action(_action);
			}
		}

		if (type == DT_DISINTEGRATE)
		{
			// destroy inventory (rather than dropping it)
			while (!target->my_items.empty())
			{
				_item = target->my_items.front();
				delete _item;
				target->my_items.pop_front();
			}
			for (i = 0; i < ES_NUM; i++)
			{
				target->equipped[i] = NULL;
			}
		}
	} else if ((target->hp <= target->hp_max / 3) && (original_hp > target->hp_max / 3)) {
		// badly hurt
		if ((target->is_hero()) || (random(2) == 0)) // don't always announce this for monsters
		{
			if ((target->is_visible()) || (target->is_hero()))
			{
				display_message _action = display_message(describe_hurt(attacker, target, type));
				stack::push_action(_action);
			}
		}
	}

	// affect AI
	if (attacker != NULL)
	{
		target->my_brain.behave_attacked(attacker);
	}
}

// --- creature_poison ---

void creature_poison :: carry_out(const char_key &key)
{
	// 'attacker' may be NULL
	assert(target != NULL);

	target->poisoned_turns = MAX(target->poisoned_turns, turns);

	// affect AI
	if (attacker != NULL)
	{
		target->my_brain.behave_attacked(attacker);
	}
}

// --- creature ---

creature :: creature(game &_game, map *_map, int _x, int _y, creature_type _type, bool can_be_equipped) :
	my_brain(_game), my_game(_game), my_map(NULL)
{
	item *new_item = NULL;
	int i;

	try
	{
		// add to game and map
		my_game.add_creature(this);
		move_to(_map, _x, _y);

		// set some stats
		type = _type;
		assert(type >= 0);
		assert(type < CT_NUM);
		hp = hp_max = random(creature_types[type].hp_min, creature_types[type].hp_max);
		heal_ctr = 0;
		poisoned_turns = 0;
		decline = 0;
		special_recharge = 0;
		special_counter = 0;
		had_turn = true;

		// equipped items
		for (i = 0; i < ES_NUM; i++)
		{
			equipped[i] = NULL;
		}
		if (can_be_equipped)
		{
			switch (creature_types[type].equipment_class)
			{
			case EC_NONE:
				{
				} break;

			case EC_BASIC:
				{
					// basic - 50% get a weapon
					if (random(2) == 0)
					{
						// random weapon
						new_item = new weapon(my_game, (weapon_type)random(WT_NUM), 0);
							my_items.push_back(new_item);
							equipped[ES_WEAPON] = new_item;
						new_item = NULL;
					}
				} break;

			case EC_IMPROVED:
				{
					// improved - always get a weapon, occasionally it's +1

					// random weapon
					new_item = new weapon(my_game, (weapon_type)random(WT_NUM), (random(3) == 0) ? (1) : (0));
						my_items.push_back(new_item);
						equipped[ES_WEAPON] = new_item;
					new_item = NULL;
				} break;

			case EC_ADVENTURER:
				{
					// +5 sword (avg 10.5 damage)
					new_item = new weapon(my_game, WT_SWORD, 5);
						my_items.push_back(new_item);
						equipped[ES_WEAPON] = new_item;
					new_item = NULL;

					// +5 dagger (avg 8.5 damage)
					new_item = new weapon(my_game, WT_DAGGER, 5);
						my_items.push_back(new_item);
					new_item = NULL;

					// four rings
					new_item = new ring(my_game, RT_MIGHT);
						my_items.push_back(new_item);
						equipped[ES_RIGHTRING] = new_item;
					new_item = NULL;
					new_item = new ring(my_game, RT_DETECTION);
						my_items.push_back(new_item);
					new_item = NULL;
					new_item = new ring(my_game, RT_FRESIST);
						my_items.push_back(new_item);
					new_item = NULL;
					new_item = new ring(my_game, RT_PRESIST);
						my_items.push_back(new_item);
					new_item = NULL;

					// +4 staff of disintegration [3 charges] (avg 7.5 damage)
					new_item = new stick(my_game, ST_DISINTEGRATION, 4, 3);
						my_items.push_back(new_item);
					new_item = NULL;

					// wand of displacement [7 charges]
					new_item = new stick(my_game, ST_DISPLACEMENT, 0, 7);
						my_items.push_back(new_item);
					new_item = NULL;

					// wand of fire [6 charges]
					new_item = new stick(my_game, ST_FIRE, 0, 6);
						my_items.push_back(new_item);
					new_item = NULL;

					// candle of invisibility [100 turns]
					new_item = new candle(my_game, 100);
						my_items.push_back(new_item);
					new_item = NULL;

					// 2 potions of extra healing
					for (i = 0; i < 2; i++)
					{
						new_item = new consumable(my_game, SPT_EXTRAHEALING);
							my_items.push_back(new_item);
						new_item = NULL;
					}

					// two scrolls
					new_item = new consumable(my_game, SPT_TELEPORT);
						my_items.push_back(new_item);
					new_item = NULL;
					new_item = new consumable(my_game, SPT_MAPPING);
						my_items.push_back(new_item);
					new_item = NULL;
				} break;

			case EC_WITCH:
				{
					// +4 dagger (avg 7.5 damage)
					new_item = new weapon(my_game, WT_DAGGER, 4);
						my_items.push_back(new_item);
						equipped[ES_WEAPON] = new_item;
					new_item = NULL;
				} break;

			default:
				{
					assert(false);
				} break;
			}
		}

		// init brain
		my_brain.init(this, creature_types[type].brain, creature_types[type].warning);

		// update to ensure values get through
		update();
		decline = 0; // (but keep decline at zero)
	} catch (...) {
		if (new_item != NULL)
		{
			delete new_item;
			new_item = NULL;
		}
		cleanup();
		throw;
	}
}

creature :: ~creature()
{
	cleanup();
}

void creature :: cleanup()
{
	map_tile *tile = map::get_tile(my_map, x, y);
	item *_item;

	// drop (or delete) inventory
	while (!my_items.empty())
	{
		_item = my_items.front();
		my_items.pop_front();
		if (tile == NULL)
		{
			delete _item;
		} else {
			tile->my_items.push_back(_item);
			_item->dropped();
		}
	}

	// remove from map
	move_to(NULL, 0, 0);

	// remove from game
	if (is_hero())
	{
		my_game.the_hero = NULL;
	}
	my_game.remove_creature(this);
}

std::string creature :: get_name()
{
	if (is_hero())
	{
		// you
		return "you";
	} else if (is_visible()) {
		// visible monster
		return creature_types[type].name;
	} else {
		// unseen monster
		return "something";
	}
}

std::string creature :: get_name_the()
{
	if (is_hero())
	{
		// you
		return "you";
	} else if (is_visible()) {
		// visible monster
		return creature_types[type].name_the;
	} else {
		// unseen monster
		return "something";
	}
}

std::string creature :: get_name_the_object()
{
	if (is_hero())
	{
		// you
		return "your";
	} else if (is_visible()) {
		// visible monster
		return creature_types[type].name_the_object;
	} else {
		// unseen monster
		return "something's";
	}
}

bool creature :: get_use_plural_verb()
{
	if (is_hero())
	{
		// you
		return true;
	} else if (is_visible()) {
		// visible monster
		return creature_types[type].use_plural_verb;
	} else {
		// unseen monster
		return false;
	}
}

void creature :: introduce()
{
	if ((hp > 0) && (!my_game.creatures_introduced[type]))
	{
		if (creature_types[type].introduction != std::string(""))
		{
			display_message _action = display_message(creature_types[type].introduction);
			stack::push_action(_action);
		}
		my_game.creatures_introduced[type] = true;
	}
}

map_tile *creature :: get_map_tile()
{
	map_tile *tile = map::get_tile(my_map, x, y);
	assert(tile != NULL);

	return tile;
}

bool creature :: is_visible()
{
	if (has_invisibility)
	{
		if ((my_game.the_hero != NULL) &&
			(my_game.the_hero->has_detection) && (!is_hero()) &&
			(get_map_tile()->vis == V_VISIBLE))
		{
			// detection lets you see invisible
			return true;
		} else {
			// invisible
			return false;
		}
	}

	return (get_map_tile()->vis == V_VISIBLE);
}

bool creature :: is_hero()
{
	return my_game.the_hero == this;
}

bool creature :: is_friends(creature *with)
{
	assert(with != NULL);
	return (is_hero() == with->is_hero());
}

u32 creature :: get_attack_flags()
{
	return creature_types[type].attack_flags;
}

int creature :: get_carry_max()
{
	int dec_steps;;

	if (is_hero())
	{
		// calculate inventory limit from decline;
		dec_steps = (decline / 50); // decline every 50 points
		if (dec_steps > 10)
		{
			dec_steps = ((dec_steps - 10) / 2) + 10; // half speed after 10 spaces lost (6 left, 500 decline)
		}

		return MAX(16 - dec_steps, 2); // minimum 2 slots
	} else {
		// monsters aren't affected by decline
		return 16;
	}
}

bool creature :: can_move(map_tile *tile)
{
	if (tile == NULL)
	{
		return false;
	}

	if (tile->is_solid)
	{
		return false;
	}

	if ((tile->my_creature != NULL) &&
		(tile->my_creature != this))
	{
		return false;
	}

	return true;
}

void creature :: move_to(map *_map, int _x, int _y)
{
	map_tile *tile;

	// leave current position
	if (get_map() != NULL)
	{
		tile = get_map_tile();
		assert(tile->my_creature == this);
		tile->my_creature = NULL;
	}

	// move
	my_map = _map;
	x = _x;
	y = _y;
	assert(x >= 0);
	assert(x < MAP_WIDTH);
	assert(y >= 0);
	assert(y < MAP_HEIGHT);

	// enter new position
	if (my_map != NULL)
	{
		tile = get_map_tile();
		assert(tile->my_creature == NULL);
		tile->my_creature = this;

		// hero - describe what's here
		if (is_hero())
		{
			std::string _message = describe_things_here(tile);
			if (_message.size() > 0)
			{
				display_message _action = display_message(_message);
				stack::push_action(_action);
			}
		}
	}
}

int creature :: calculate_damage(creature *target, int min, int max, u32 attack_flags, bool visible) {
	int roll, damage_amount=0, i;

	roll = random(100);
	if (roll < creature_types[type].hit_percent)
	{
		// hit
		damage_amount = random(min,max);
		if (((attack_flags & AF_POISON) != 0) && (!target->resists_poison))
		{
			i = random(POISON_MIN_TURNS, POISON_MAX_TURNS);
			{
				creature_poison _action = creature_poison(this, target, i);
				stack::push_action(_action);
			}
			{
				display_message _action = display_message(describe_poison(this, target, i));
				stack::push_action(_action);
			}
		}
		{
			creature_damage _action = creature_damage(this, target, DT_NORMAL, damage_amount);
			stack::push_action(_action);
		}
		
	} 

		if (visible)
		{
			if (damage_amount) 
				{
				display_message _action = display_message(describe_attack(this, target, true, damage_amount));
				stack::push_action(_action);
				}
			else
				{
				display_message _action = display_message(describe_attack(this, target, false, 0));
				stack::push_action(_action);
				}
		}
		
	return damage_amount;
}

void creature :: attack(creature *target)
{
	bool visible;
	assert(target != NULL);

	visible = (is_visible()) || (target->is_visible()) || (is_hero()) || (target->is_hero());
	calculate_damage(target,damage_min, damage_max, 
       creature_types[type].attack_flags,visible);

	// explicitly affect AI, even though we didn't do damage (where this is normally processed)
	target->my_brain.behave_attacked(this);
}

void creature :: move()
{
	if (((creature_types[type].attack_flags & AF_FAST) != 0) && (hp > 0))
	{
		// move twice!
		my_brain.move(false);
	}

	my_brain.move(true);

	// we may actually need to introduce the creature now if it's just moved into view.  This doesn't
	// really need to be done now, except that if the monster was hasty it may have performed another
	// action and stacked messages about itself - in which case we need this now!
	if (is_visible())
	{
		introduce();
	}
}

void creature :: update()
{
	std::list<item *>::iterator it, it_here;
	int old_carry_max;

	// clear stats
	carrying = 0;
	damage_min = 0;
	damage_max = 0;
	damage_unarmed = true;
	resists_fire = (creature_types[type].defense_flags & DF_FRESIST) != 0;
	resists_poison = (creature_types[type].defense_flags & DF_PRESIST) != 0;
	has_detection = false;
	has_invisibility = (creature_types[type].defense_flags & DF_INVISIBLE) != 0;

	// update inventory
	it = my_items.begin();
	while (it != my_items.end())
	{
		it_here = it;
		it++;

		carrying += (*it_here)->weight;
		(*it_here)->carry_update(this);
	}

	if (damage_unarmed)
	{
		// add unarmed damage
		damage_min += creature_types[type].unarmed_min;
		damage_max += creature_types[type].unarmed_max;
	}

	// healing (recover full hit points in 125 turns)
	heal_ctr += hp_max;
	while (heal_ctr >= 125)
	{
		hp += 1;
		heal_ctr -= 125;
	}
	if (hp >= hp_max)
	{
		hp = hp_max;
		heal_ctr = 0;
	}

	old_carry_max = get_carry_max();

	// poison
	if (poisoned_turns > 0)
	{
		decline += 5;
		poisoned_turns--;
		if ((poisoned_turns == 0) && (is_hero()))
		{
			display_message _action = display_message(describe_poison_off(this));
			stack::push_action(_action);
		}
	} else if (is_hero()) {
		// normal decline
		decline++;
	}
	if (decline > 9999)
	{
		decline = 9999; // maximum
	}

	if ((is_hero()) && (get_carry_max() < old_carry_max))
	{
		display_message _action = display_message(describe_decline(this));
		stack::push_action(_action);
	}

	// recharge special powers
	if (special_recharge > 0)
	{
		special_recharge--;
	}
}

void creature :: draw()
{
#ifdef DEBUG_SEE_EVERYTHING
	if (true)
#else
	if (is_visible())
#endif // DEBUG_SEE_EVERYTHING
	{
		// creature is visible
		char_draw(x, y, creature_types[type].display_ch, creature_types[type].display_style);
	} else if (is_hero()) {
		// display the hero even when he's invisible
		char_style greyed_style;

		greyed_style = creature_types[type].display_style;
		greyed_style.fore_r = (greyed_style.fore_r >> 2) + 96;
		greyed_style.fore_g = (greyed_style.fore_g >> 2) + 96;
		greyed_style.fore_b = (greyed_style.fore_b >> 2) + 96;
		char_draw(x, y, creature_types[type].display_ch, greyed_style);
	} else {
		if ((my_game.the_hero != NULL) && (my_game.the_hero->has_detection) &&
			(abs(my_game.the_hero->get_x() - x) <= DETECTION_RANGE) &&
			(abs(my_game.the_hero->get_y() - y) <= DETECTION_RANGE))
		{
			// creature is detected
			char_draw(x, y, '?', DETECTION_STYLE);
		}
	}
}
